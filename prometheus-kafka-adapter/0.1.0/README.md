This helm chart is from the
[Telefonica/prometheus-kafka-adapter](https://github.com/Telefonica/prometheus-kafka-adapter/tree/master/helm/prometheus-kafka-adapter)
git repository but is not direclty available via any helm repository we can
find.

For this reason, we have included the helm chart direclty in our own
repository.

# Helm values

This chart is configurable using values.
<table>
 <thead>
  <tr>
   <th>Value name</th>
   <th>Default</th>
   <th>Description</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td>nameOverride</td>
   <td>.Chart.Name</td>
   <td>
    specifies a partial override of name generated using the fullname template
    <p>
     The generated full name will be
     <code>{{ .Release.Name }}-{{ nameOverride }}</code>
    </p>
   </td>
  </tr>
  <tr>
   <td>fullnameOverride</td>
   <td>""</td>
   <td>
    specifies a full override of the name generated using the fullname template
    <p>
     The generated full name will be
     <code>{{ fullnameOverride }}</code>
    </p>
   </td>
  </tr>
  <tr>
   <td>pod.annotations</td>
   <td>
    <pre>
     prometheus.io/scrape: "true"
     prometheus.io/port:   "8080"
    </pre>
   </td>
   <td>
    defines the set of annotations that may be assigned to pods to enable
    automatic prometheus metrics scraping.
   </td>
  </tr>

  <tr>
   <td>serviceAccount.name</td>
   <td>generated using the fullname template</td>
   <td>the name of the service account to use</td>
  </tr>

 </tbody>
</table>


Most critical settings are contained in the <code>environment</code> dictionary:
<table>
 <thead>
  <tr>
   <th>Value name (<code>environment.</code>)</th>
   <th>Default</th>
   <th>Description</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td>KAFKA_BROKER_LIST</td>
   <td>kafka:9092</td>
   <td>defines kafka endpoint and port</td>
  </tr>
  <tr>
   <td>KAFKA_TOPIC</td>
   <td>metrics</td>
   <td>
    defines kafka topic to be used.
    <p>
     Could use go template, labels are passed (as a map) to the template, for
     example:<br>
     <code>metrics.{{index . "__name__" }}</code> to use per-metric topic.<br>
     Two template functions are available:<br>
     &nbsp;replace (<code>{{ index . "__name__" | replace "message" "msg" }}</code>)<br>
     &nbsp;substring (<code>{{ index . "__name__" | substring 0 5 }}</code>)
    </p>
   </td>
  </tr>
  <tr>
   <td>KAFKA_COMPRESSION</td>
   <td>none</td>
   <td>defines the compression type to be used.</td>
  </tr>
  <tr>
   <td>KAFKA_BATCH_NUM_MESSAGES</td>
   <td>10000</td>
   <td>defines the number of messages to batch write.</td>
  </tr>
  <tr>
   <td>SERIALIZATION_FORMAT</td>
   <td>json</td>
   <td>
    defines the serialization format.
    <p>
     can be <code>json</code>, <code>avro-json</code>.
    </p>
    <p>
     For <code>json</code>, the serialized data will have the form:
     <pre>
{
  "timestamp": "1970-01-01T00:00:00Z",
  "value": "9876543210",
  "name": "up",

  "labels": {
    "__name__": "up",
    "label1": "value1",
    "label2": "value2"
  }
}
     </pre>

     <b>NOTE:</b> The timestamp in the incoming metric sample is expected to be
     milliseconds since the epoch.
    </p>
   </td>
  </tr>
  <tr>
   <td>PORT</td>
   <td>8080</td>
   <td>defines http port to listen, used directly by
   <a href="https://github.com/gin-gonic/gin">gin</a>.
   </td>
  </tr>
  <tr>
   <td>BASIC_AUTH_USERNAME</td>
   <td>no basic auth</td>
   <td>
    basic auth username to be used for receive endpoint
    <p>
     If provided, this will be included in a generated secret (named using to
     the fullname template) with the key <code>BASIC_AUTH_USERNAME</code>.
    </p>
   </td>
  </tr>
  <tr>
   <td>BASIC_AUTH_PASSWORD</td>
   <td>no basic auth</td>
   <td>
    basic auth password to be used for receive endpoint
    <p>
     If provided, this will be included in a generated secret (named using to
     the fullname template) with the key <code>BASIC_AUTH_PASSWORD</code>.
    </p>
   </td>
  </tr>
  <tr>
   <td>LOG_LEVEL</td>
   <td>info</td>
   <td>
    defines log level for <a href="https://github.com/sirupsen/logrus">logrus</a>.
    <p>
     can be <code>debug</code>, <code>info</code>, <code>warn</code>,
     <code>error</code>, <code>fatal</code> or <code>panic</code>.
    </p>
   </td>
  </tr>
  <tr>
   <td>GIN_MODE</td>
   <td></td>
   <td>
    manage <a href="https://github.com/gin-gonic/gin">gin</a> debug logging.
    <p>
     can be <code>debug</code> or <code>release</code>.
    </p>
   </td>
  </tr>
 </tbody>
</table>


To connect to Kafka over SSL define the following additonal
<code>environment</code> entries:
<table>
 <thead>
  <tr>
   <th>Value name (<code>environment.</code>)</th>
   <th>Default</th>
   <th>Description</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td>KAFKA_SSL_CLIENT_CERT_FILE</td>
   <td>
    if <code>KAFKA_SSL_CLIENT_CERT</code> is set,
    "/client_cert/ssl_client_cert.pem", otherwise ""
   </td>
   <td>
    Kafka SSL client certificate file.
    <p>
     If provided and <code>KAFKA_SSL_CLIENT_CERT</code> is set, this will be
     used to generate a secret containing the certificate data named using to
     the fullname template with a prefix of <code>-ssl-client-cert</code>.
    </p>
   </td>
  </tr>

  <tr>
   <td>KAFKA_SSL_CLIENT_KEY_FILE</td>
   <td>
    if <code>KAFKA_SSL_CLIENT_KEY</code> is set,
    "/client_cert/ssl_client_key.pem", otherwise ""
   </td>
   <td>
    Kafka SSL client certificate key file.
    <p>
     If provided and <code>KAFKA_SSL_CLIENT_KEY</code> is set, this will be
     used to generate a secret containing the key data named using to
     the fullname template with a prefix of <code>-ssl-client-key</code>.
    </p>
   </td>
  </tr>
  <tr>
   <td>KAFKA_SSL_CLIENT_KEY_PASS</td>
   <td>""</td>
   <td>
    The password for the SSL client key identified via
    <code>KAFKA_SSL_CLIENT_KEY_FILE</code>.
    <p>
     If provided, this will be included in a generated secret (named using to
     the fullname template) with the key
     <code>KAFKA_SSL_CLIENT_KEY_PASS</code>.
    </p>
   </td>
  </tr>

  <tr>
   <td>KAFKA_SSL_CA_CERT_FILE</td>
   <td>
    if <code>KAFKA_SSL_CA_CERT</code> is set,
    "/ca_cert/ssl_ca_cert.pem", otherwise ""
   </td>
   <td>
    Kafka SSL broker CA certificate file.
    <p>
     If provided and <code>KAFKA_SSL_CA_CERT</code> is set, this will be
     used to generate a secret containing the certificate data named using to
     the fullname template with a prefix of <code>-ssl-ca-cert</code>.
    </p>
   </td>
  </tr>
 </tbody>
</table>
