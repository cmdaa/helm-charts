{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
*/}}
{{- define "fullname" -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/* :XXX: CMDAA addition {
Create a generate endpoint from server and port.
   Usage:
     endpoint: \{\{- include "argo.s3.endpoint" . \}\}
*/}}
{{- define "argo.s3.endpoint" -}}
{{-  if .Values.artifactRepository.s3.host -}}
{{-   $s3Port := (.Values.artifactRepository.s3.port | toString | default "9000") -}}
{{-   printf "%s:%s" .Values.artifactRepository.s3.host $s3Port -}}
{{-  else if .Values.artifactRepository.s3.endpoint -}}
{{-   .Values.artifactRepository.s3.endpoint -}}
{{-  else -}}
{{-   printf "%s-%s" .Release.Name "minio:9000" -}}
{{-  end -}}
{{- end -}}
{{- /* :XXX: CMDAA addition } */ -}}
