#!/bin/bash
#
# Cache helm charts from a variety of external sources.
#
#     curl -Lkfs https://charts.timescale.com/index.yaml
#
# In this case, the files are likely still in the repo, just no longer indexed.
#     curl -Lkfs https://charts.timescale.com/tobs-0.0.1.tgz -o tobs-0.0.1.tgz
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")

. "$DIR/ini_parse.sh"
. "$DIR/helm.sh"

cd "$ROOT"

echo ">>> Reading ini file '$DIR/cache.ini' ..."
eval "$(ini_parse "$DIR/cache.ini")"

echo "${#ini_sections[@]} sections[ ${ini_sections[@]} ]"
for sect in ${ini_sections[@]}; do
  repo=$(ini_get "$sect.repository")
  vers=( $(ini_get "$sect.versions") )

  nVers=${#vers[@]}
  if [[ $nVers -gt 0 ]]; then
    # One or more helm versions
    if [[ -z "$repo" ]]; then
      echo "*** Missing helm repository for '$sect'"
      continue
    fi

    name=$(ini_get "$sect.name")
    [[ -z "$name" ]] && name="$sect"

    #echo "  $sect[ $name ]: repository[ $repo ]: $nVers versions:"
    printf "\n  %s [ %s ]: %d versions:\n" \
            "$name" "$repo" "$nVers"

    # Do we need to do a `helm_refresh`?
    CACHE_DIR=$( helm_cache_dir "$repo" )
    if [ ! -f "$CACHE_DIR/tarballs.txt" ]; then
      msg=$( helm_refresh_index "$repo" )
       rc=$?
      if [[ $rc -ne 0 ]]; then
        echo "*** Cannot refresh [$rc]: $msg"
        continue
      fi
    fi

    for vers in ${vers[@]}; do

      msg=$( helm_fetch_version "$repo" "$name" "$vers" )
      rc=$?
      if [[ $rc -ne 0 ]]; then
        echo "    *** Failed to fetch $name : $vers :$rc: $msg"
        continue
      fi

      tarball_full="$msg"
      tarball_base="$(basename "$tarball_full")"

      echo -n "    $tarball_base"

      if [[ -f "$tarball_base" ]]; then
        MD5_CUR=$( md5sum "$tarball_base" | awk '{print $1}' )
        MD5_NEW=$( md5sum "$tarball_full" | awk '{print $1}' )

        if [[ "$MD5_CUR" == "$MD5_NEW" ]]; then
          # No change
          echo " (no change)"
          continue
        fi
        echo " (update)"
      else
        echo
      fi

      # Copy the (new) tarball to the top
      cp "$tarball_full" "$tarball_base"
    done
  fi
done

exit

########################################
BASE_URL="https://charts.timescale.com"

#REPO=$(basename "$(pwd)") #"tobs"
REPO="timescaledb-single"

echo -n ">>> Refresh $REPO index.yaml ..."
curl -Lkfs "$BASE_URL/index.yaml" -o index-new.yaml
rc=$?

if [[ $rc -eq 0 ]]; then
  # Is this new index.yaml different from any we already have?
  echo
  if [[ -f index.yaml ]]; then
    MD5_OLD=$( md5sum index.yaml      | awk '{print $1}')
    MD5_NEW=$( md5sum index-new.yaml  | awk '{print $1}')

    if [[ "$MD5_OLD" != "$MD5_NEW" ]]; then
      # index.yaml change
      mv -f index-new.yaml index.yaml
      rm -f versions.txt

      echo "--- Updated index.yaml"
    else
      rm -f index-new.yaml

      echo "--- No change to index.yaml"
    fi

  else
    echo "--- Fetched index.yaml"
    mv -f index-new.yaml index.yaml
    rm -f versions.txt
  fi

  ###
  # Show all available projects in the given repository index
  #echo "--- Available projects: "
  #awk 'BEGIN{ printf("     "); out=0; outlen=0}
  #/^  [a-z]/{
  #  name    = substr($1,1,length($1)-1);
  #  outlen += length(name) + 2;
  #
  #  if (out++  >  0){ printf(", ") }
  #  if (outlen > 75){ printf("\n     "); outlen=0 }
  #
  #  printf("%s", name);
  #} END{ printf("\n") }' index.yaml


else
  echo "*** Update of index.yaml FAILED [ $rc ]"
  exit $rc
fi

if [[ ! -f versions.txt ]]; then
  echo "--- Generate version info for $REPO ..."

  awk '
    /^  [a-z-]+:/ { if ( $1 == "'$REPO':" ) { inRepo = 1; inUrls = 0 }
                    else                    { inRepo = 0; inUrls = 0 }
                    next;
                  }
    / urls:/      { if (inRepo)             { inUrls = 1 } next }
    / - /         { if (inRepo && inUrls)   { print $2 }   next }
    {
      #printf("%s%s: %s\n",
      #        (inRepo ? "Repo" : "IGNR"),
      #        (inUrl  ? "-urls": "     "),
      #        $0);
      inUrls = 0;
    }' index.yaml > versions.txt

fi

if [[ ! -s versions.txt ]]; then
  echo "*** No $REPO versions found, populate with known versions ..."
  cat <<EOF > versions.txt
$REPO-0.4.0.tgz
$REPO-0.5.0.tgz
$REPO-0.5.1.tgz
$REPO-0.5.2.tgz
$REPO-0.5.3.tgz
$REPO-0.5.4.tgz
$REPO-0.5.5.tgz
$REPO-0.5.6.tgz
$REPO-0.5.7.tgz
$REPO-0.5.8.tgz
$REPO-0.6.0.tgz
$REPO-0.6.1.tgz
$REPO-0.6.2.tgz
$REPO-0.7.0.tgz
$REPO-0.7.1.tgz
$REPO-0.8.0.tgz
$REPO-0.8.1.tgz
$REPO-0.8.2.tgz
EOF
#else
#  echo "--- Use existing versions.txt"
fi

VERS=()
if [[ -f versions.txt ]]; then
  VERS=( $(cat versions.txt) )
fi

#echo "${#VERS[@]} $REPO versions [ ${VERS[@]} ]"
#exit

for tarball in ${VERS[@]}; do
  version=$(echo "$tarball" | sed 's/'$REPO'-//;s/.tgz$//')

  if [[ ! -d "$version" ]]; then
    URL="$BASE_URL/$tarball"

    echo -n "--- Pull version $version [ $URL ] ..."
    curl -Lkfs "$URL" | tar xz
    rc_curl=${PIPESTATUS[0]} rc_tar=${PIPESTATUS[1]} rc=$?

    if [[ $rc_curl -ne 0 ]]; then
      echo "* Fetch FAILED [ $rc_curl ]"

    elif [[ $rc_tar -ne 0 ]]; then
      echo "* untar FAILED [ $rc_tar ]"

    else
      echo -n ". "
      mv "$REPO" "$version"
      [[ $? -eq 0 ]] && echo "done"

    fi

  else
    echo "--- Already have version $version : skip"
  fi
done
