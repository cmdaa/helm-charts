#!/bin/bash
#
# Retrieve the chart and app versions (if available) from the given chart
# tarball(s)
#
#   get-version.sh %chart-tarball% ...
#
printf "%-25s : %-10s : %s\n" "Name" "Chart-vers" "App-vers"
for tarball in $@; do
  name="$( basename "$tarball" | sed -E 's/-[0-9.-]+.tgz//' )"
  tar -C /tmp -xzf "$tarball" --wildcards '*/Chart.yaml'

  for file in $(find "/tmp/$name" -name 'Chart.yaml'); do
    chart="$( dirname "$file" | sed -E "s#/tmp/$name/?##;s#charts/##g")"
    if [ -z "$chart" ]; then
      chart=$name
    elif [ "$chart" != "$name" ]; then
      chart=" $chart"
    fi

    verChart=$(awk '/^version:/{print $2}' "$file")
    verApp=$(  awk '/^appVersion:/{print $2}' "$file")

    printf "%-25s : %-10s : %s\n" "$chart" "$verChart" "$verApp"
  done

  rm  -rf "/tmp/$name"
  echo
done
