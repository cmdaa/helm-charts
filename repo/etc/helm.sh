#!/bin/bash
#
# Helpers to manage cached helm indices and tarballs.
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")

##
# Log to stderr
# @method helm_log
# @param  [log data]
#
# @return void
#
function helm_log() {
  cat <<< "$@" 1>&2;
}

##
# Generate a URL without a '/' suffix
# @method helm_url
# @param  url   The url to the target helm repository;
#
# @return The url without any '/'' suffix
#
function helm_url() {
  echo "$@" | sed 's#/$##'
}

##
# Ensure there is a cache directory for the target repository
# @method helm_cache_dir
# @param  url   The url to the target helm repository;
#
# The return code indicates status:
#   0       success
#   250     Missing url parameter
#   other   mkdir errors
#
# @return The path to the cache directory (or an error message);
#
function helm_cache_dir() {
  url="$(helm_url "$@")"
  if [[ -s "$url" ]]; then
    echo "<Missing url>"
    return 250
  fi

  # Remove 'https?://' prefix
  # Convert all non-fs characters to '_'
  # Remove any '.+' prefix
  # Remove any '/'  prefix
  repo="$(echo "$url" | \
    sed -E 's#^https?://([^?\#]+).*$#\1#;s#[^a-zA-Z0-9/.-]#_#g;s#^[./]+##')"

  CACHE_DIR="$ROOT/.cache/$repo"
  if [[ ! -d "$CACHE_DIR" ]]; then
    err=$(mkdir -p "$CACHE_DIR" 2>&1)
    rc=$?

    if [[ $rc -ne 0 ]]; then
      echo "<Cannot create cache directory [ $CACHE_DIR ]: $err>"
      return $rc
    fi
  fi

  echo "$CACHE_DIR"
  return 0
}

##
# Refresh the cached index.yaml for the given repository
# @method helm_refresh_index
# @param  url   The url to the target helm repository;
#
# The return code indicates status:
#   0       success
#   250     Missing url parameter
#   other   curl errors
#
# @return A string representation of the state;
#
function helm_refresh_index() {
  url="$(helm_url "$@")"
  if [[ -s "$url" ]]; then
    echo "<Missing url>"
    return 250
  fi

  CACHE_DIR=$( helm_cache_dir "$url" )

  INDEX_CUR="$CACHE_DIR/index.yaml"
  INDEX_NEW="$CACHE_DIR/new-index.yaml"

  TARBALLS="$CACHE_DIR/tarballs.txt"

  curl -Lkfs "$url/index.yaml" -o "$INDEX_NEW"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echo "<Fetch of '$url/index.yaml' failed [ $rc ]>"
    return $rc
  fi

  if [[ -f "$INDEX_CUR" ]]; then
    # Compare the new index with the current
    MD5_CUR=$( md5sum "$INDEX_CUR" | awk '{print $1}' )
    MD5_NEW=$( md5sum "$INDEX_NEW" | awk '{print $1}' )

    if [[ "$MD5_CUR" != "$MD5_NEW" ]]; then
      # index.yaml changed
      mv -f "$INDEX_NEW" "$INDEX_CUR"
      rm -f "$TARBALLS"
    else
      # No change
      rm -f "$INDEX_NEW"
    fi
  else
    # There was no index previously
    mv -f "$INDEX_NEW" "$INDEX_CUR"
    rm -f "$TARBALLS"
  fi

  if [[ ! -f "$TARBALLS" ]]; then
    # (Re)generate a simplified file with just the available tarballs
    awk '
      /^  [a-z-]+:/ {
        name   = substr( $1, 1, length($1)-1 );
        inRepo = 1;
        inUrls = 0;
        next;
      }
      / urls:/ {
        if (inRepo) { inUrls = 1 }
        next;
      }
      / - / {
        if (inRepo && inUrls) { print $2 }
        next;
      }
      { inUrls = 0 }' "$INDEX_CUR" | \
        sed 's#^'$url'/##' > "$TARBALLS"
  fi

  echo "done"
  return 0
}

##
# Generate the full URL to the tarball matching the given path/version within
# the repository associatd with the given url.
# @method helm_full_url
# @param  url     The url to the target repository;
# @param  path    The target package within the repository;
# @param  version The desired version;
#
# The return code indicates status:
#   0       success
#   250     Missing /empty url     parameter
#   251     Missing /empty path    parameter
#   252     Missing /empty version parameter
#   253     Missing tarballs : have you run helm_refresh_index?
#   254     No match
#   255     Multiple matches [ %list% ]
#
# @return The matching tarball (or an error messsage);
#
function helm_full_url() {
     url="$(helm_url "$1")"
     path="$(echo "$2" | sed 's#^/##;s#/$##')"
  version="$3"
  if [[ -s "$url" ]]; then
    echo "<Missing / empty url [ $1 => $url ]>"
    return 250
  fi
  if [[ -s "$url" ]]; then
    echo "<Missing / empty path [ $2 => $path ]>"
    return 251
  fi
  if [[ -s "$version" ]]; then
    echo "<Missing / empty version [ $3 => $version ]>"
    return 252
  fi

  #helm_log ">>> helm_full_url: url[ $1 ], path[ $2 ], version[ $3 ] ..."

  # Identify the tarball that matches the target path/version
  CACHE_DIR=$( helm_cache_dir "$url" )
      INDEX="$CACHE_DIR/index.yaml"
   TARBALLS="$CACHE_DIR/tarballs.txt"

  if [[ ! -f "$TARBALLS" ]]; then
    # There is no tarballs file.
    #
    # If the requested version looks like a path (i.e. contains a '/'), return
    # a url comprised of the repository URL and version.
    #
    if [[ "$version" =~ / ]]; then
      echo "$url/$version"
      return 0
    fi

    echo "<Missing tarballs [ $TARBALLS ] : have you run helm_refresh_index?>"
    return 253
  fi

  MATCHES=( $(grep -E "$path-$version" "$TARBALLS") )
  nMatches=${#MATCHES[@]}

  #helm_log ">>> helm_full_url: MATCHES: ${MATCHES[@]} ..."

  if [[ $nMatches -lt 1 ]]; then
    echo "<No match>"
    return 254
  fi
  if [[ $nMatches -gt 1 ]]; then
    echo "<Multiple matches [ ${MATCHES[@]} ]>"
    return 255
  fi

  MATCH="${MATCHES[0]}"
  #helm_log ">>> helm_full_url: MATCH: $MATCH ..."

  echo "$MATCH" | grep -E '^https?://' >/dev/null 2>&1
  if [ $? -eq 0 ]; then
    # The match is a full URL, likely different from the chart URL
    #helm_log ">>> helm_full_url: NEW FULL url: $MATCH"
    full_url="$MATCH"
  else
    full_url="$url/$MATCH"

  fi

  echo "$full_url"
  return 0
}

##
# Fetch the tarball
# @method helm_fetch_version
# @param  url     The url to the target helm repository;
# @param  path    The target package within the repository;
# @param  version The desired version;
#
# The return code indicates status:
#   0       success
#   # From helm_full_url() {
#   250     Missing /empty url     parameter
#   251     Missing /empty path    parameter
#   252     Missing /empty version parameter
#   253     Missing tarballs : have you run helm_refresh_index?
#   254     No match
#   255     Multiple matches [ %list% ]
#   # From helm_full_url() }
#   other   curl errors
#
# @return The path to the fetched tarball (or an error message);
#
function helm_fetch_version() {
  #helm_log ">>> helm_fetch_version: url[ $1 ], path[ $2 ], version[ $3 ] ..."

  res=$( helm_full_url $@ )
   rc=$?
  if [[ $rc -ne 0 ]]; then
    echo $res
    return $rc
  fi

  full_url="$res"
  repo_url="$(helm_url "$1")"

   cache_dir=$( helm_cache_dir "$repo_url" )
  cache_path="$cache_dir/$(basename "$full_url")"


  curl -Lkfs "$full_url" -o "$cache_path"
  rc=$?
  if [[ $rc -ne 0 ]]; then
    echo "<Fetch of '$full_url' failed [ $rc ]>"
    return $rc
  fi

  echo "$cache_path"
  return 0
}
