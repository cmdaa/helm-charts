#!/bin/bash
#
# Helpers to parse and access ini files.
#

##
# Parse an ini file into bash3-compatible varibles
# @method ini_parse
# @param  init_file   The path to the ini file;
#
# Example:
#   eval "$(ini_parse cache.ini)"
#
# @return   A string that may be `eval` to generate:
#   ini_sections        An array of section names;
#   ini_%sect%__fields  An array of fields for the named section;
#   ini_%sect%_%field%  An array of values for the named section/field;
#
function ini_parse() {
  ini_file="$1"

  awk '
  /^#/{ next }
  /^\[/{
    sect   = substr($1, 2, length($1)-2);
    gsub(/[^a-z0-9]+/, "_", sect);

    inSect = 1;
    sections[ nsections++ ] = sect;
    next;
  }
  /=/ {
    if (inSect != 1) { next }
    field = $1;

    fields[ sect, nfields[sect]++ ] = field;

    printf("ini_%s_%s=(", sect,field);
    for (idex = 3; idex <= NF; idex++) {
      section[ sect, field, nvals[ sect,field ]++ ] = $idex;

      printf(" \"%s\"", $idex);
    }
    printf(" );\n");
  }
  END {
    printf("ini_sections=(");
    for (idex = 0; idex < nsections; idex++) {
      sect = sections[ idex ];
      printf(" \"%s\"", sect);
    }
    printf(" );\n");

    for (idex = 0; idex < nsections; idex++) {
      sect = sections[ idex ];
      printf("ini_%s__fields=(", sect);
      for (jdex = 0; jdex < nfields[sect]; jdex++) {
        field = fields[ sect, jdex ];
        printf(" \"%s\"", field);
      }
      printf(" );\n");
    }

    #for (idex = 0; idex < nsections; idex++) {
    #  sect = sections[ idex ];
    #
    #  printf("section #%3d: %s, %d fields\n",
    #          idex, sect, nfields[sect]);
    #  for (jdex = 0; jdex < nfields[sect]; jdex++) {
    #    field = fields[ sect, jdex ];
    #    printf("  %s\n", field);
    #
    #    for (kdex = 0; kdex < nvals[sect,field]; kdex++) {
    #      val = section[sect,field,kdex];
    #      printf("    %s\n", val);
    #    }
    #  }
    #
    #}
    #for (key in section) {
    #  split(key, sep, SUBSEP);
    #  sect  = sep[1];
    #  field = sep[2];
    #  idex  = sep[3];
    #
    #  printf("  sect[ %s ], field[ %s ], idex[ %s ]: %s\n",
    #         sect, field, idex, section[sect,field,idex]);
    #}
  }' "$ini_file"
}

##
# Given an environment containing variables generated via ini_parse(), expose
# the array of fields for the given section;
# @method ini_fields
# @param  sect    The target section;
#
# Example:
#   eval "$(ini_parse cache.ini)"
#
#   ini_fields tobs
#   echo ${fields[@]}"
#
# Expose the fields of the target section as `fields`
#
function ini_fields() {
  local sect

  sect="$1"

  eval 'fields=( ${ini_'$sect'__fields[@]} )'
}

##
# Given an environment containing variables generated via ini_parse(), expose
# the array of values for a specific section/field;
# @method ini_values
# @param  sect    The target section;
# @param  field   The target field within `sect`;
#
# Example:
#   eval "$(ini_parse cache.ini)"
#
#   ini_values tobs versions
#   echo ${values[@]}"
#
# Expose the values of the target section as `values`
#
function ini_values() {
  local sect field

   sect="$1"
  field="$2"

  eval 'values=( ${ini_'$sect'_'$field'[@]} )'
}

##
# Given an environment containing variables generated via ini_parse(),
# determine if the given section exists;
# @method ini_hasSection
# @param  sect    The target section;
#
# Example:
#   eval "$(ini_parse cache.ini)"
#
#   if [[ $(ini_hasSection tobs == 1 ]]; then
#     echo ">>> Has 'tobs' section"
#   else
#     echo "*** No  'tobs' section"
#   fi
#
# @return   An indicator of whether the section exists;
#           - exists          1
#           - does NOT exist  0
#
function ini_hasSection() {
  local sect

  sect="$1"

  if [[ " ${ini_sections[@]} " =~ " ${sect} " ]]; then
    # Exists
    echo "1"
    return 1
  fi

  # Does NOT exist
  echo "0"
  return 0
}

##
# Given an environment containing variables generated via ini_parse(),
# determine if the given section has the given field;
# @method ini_hasField
# @param  sect    The target section;
# @param  field   The target field within `sect`;
#
# Example:
#   eval "$(ini_parse cache.ini)"
#
#   if [[ $(ini_hasField tobs versions == 1 ]]; then
#     echo ">>> Has 'tobs.versions' field"
#   else
#     echo "*** No  'tobs.versions' field"
#   fi
#
# @return   An indicator of whether the field exists;
#           - exists          1
#           - does NOT exist  0
#
function ini_hasField() {
  local sect field

   sect="$1"
  field="$2"

  if [[ $(ini_hasSection "$sect") == 1 ]]; then
    ini_fields "$sect"

    if [[ " ${fields[@]} " =~ " ${field} " ]]; then
      # Exists
      echo "1"
      return 1
    fi
  fi

  # Does NOT exist
  echo "0"
  return 0
}

##
# Given an environment containing variables generated via ini_parse(),
# fetch the named variable;
# @method ini_get
# @param  path    A dotted path to the target variable;
#
# The dotted path should have the form:
#   section[.field[.index]]
#
# The return code indicates status:
#   0     success
#   250   Missing path parameter
#   251   Invalid / empty section
#   252   Invalid / empty field
#   253   Invalid index
#
# Example:
#   eval "$(ini_parse cache.ini)"
#
#   res=( $(ini_get tobs.versions) )
#   rc=$?
#
#   case $rc in
#     0)    echo ">>> Found ${#res[@]} values: ${res[@]}";;
#     250)  echo "*** Missing path : $res";;
#     251)  echo "*** Invalid / empty section: $res";;
#     252)  echo "*** Invalid / empty field: $res";;
#     253)  echo "*** Invalid index: $res";;
#     *)    echo "*** Unexpected rc[ $rc ]: $res";;
#   esac
#
# @return   The value of the target;
#
function ini_get() {
  local path sect res field idex

  path=( $(echo "$1" | sed 's/\./ /g') )
  if [[ ${#path[@]} -lt 1 ]]; then # {
    # Invalid usage, no path
    echo "<Missing / empty path>"
    return 250
  fi # }

  #echo "${#path[@]}: path[ ${path[@]} ]"

  sect=${path[0]}

  ini_fields "$sect"
  res=( ${fields[@]} )

  #echo "  sect[ $sect ]: ${#res[@]}: [ ${res[@]} ]"

  if [[ ${#path[@]} -le 1 ]]; then  # {
    echo ${res[@]}
    return 0
  fi # }

  ######################################
  # Process the section field
  #
  if [[ ${#res[@]} -lt 1 ]]; then   # {
    # Invalid/empty section
    echo "<Invalid / empty section [$sect]>"
    return 251
  fi # }

  field=${path[1]}

  ini_values "$sect" "$field"
  res=( ${values[@]} )

  #echo "    field[ $field ]: ${#res[@]}: [ ${res[@]} ]"

  if [[ ${#path[@]} -le 2 ]]; then  # {
    echo ${res[@]}
    return 0
  fi # }

  ######################################
  # Process the section field index
  #
  if [[ ${#res[@]} -lt 1 ]]; then   # {
    # Invalid/empty field
    echo "<Invalid / empty field [$sect.$field]>"
    return 252
  fi # }

  idex=${path[2]}

  #echo "      idex[ $idex ]: ${#res[@]}: [ ${res[$idex]} ]"

  if [[ $idex -ge ${#res[@]} ]]; then # {
    # Invalid index
    echo "<Invalid index [$sect.$field.$idex > ${#res[@]}]>"
    return 253
  fi # }

  #echo "$sect.$field.$idex ${#res[@]}:[ ${res[@]} ] => [ ${res[$idex]} ]..."

  echo ${res[$idex]}
  return 0
}
