This is a contained helm repository.

You may use the following url for any utility that pulls from a helm
repository:
> https://gitlab.com/cmdaa/helm-charts/-/raw/master/repo/


This repository contains packaged helm-charts, both cached and CMDAA-specific.


## Cached charts
Several critical helm-charts are included in this repository to ensure their
continued availablity.

These charts are identified via [etc/cache.ini](etc/cache.ini) and may be
updated via [etc/update-cache.sh](etc/update-cache.sh) or `make cache`.

**NOTE:** One cached chart
([prometheus-kafka-adapter](../prometheus-kafka-adapter/0.1.0)) is only
avaialble via github, so the necessary portions of the git repository have been
cloned and are used to maintain access.


## CMDAA-specific charts

CMDAA-specific charts may be added using `helm package` followed by an update
of the repository index via `helm repo index`.

For example, to include the
[cmdaa/analytics/v0.0.12](../cmdaa/analytics/v0.0.12) helm chart in this
repository:
```bash
$ cd helm-charts

#
# Package up the target chart
#
$ helm package -d ./repo ./cmdaa/analytics/v0.0.12
Successfully packaged chart and saved it to: repo/cmdaa-analytics-0.0.12.tgz

#
# Update the repository index
#
$ helm repo index ./repo

#
# To complete the inclusion, the tarball must be added to git and pushed,
# along with the changes to index.yaml
#
$ git add repo/cmdaa-analytics-0.0.12.tgz
$ git commit -m "Added cmdaa-analytics" repo
$ git push
```

You can also update the [repo/Makefile](repo/Makefile) to include rules for
your new chart.


