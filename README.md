This git repository contains [supporting helm-charts](#supporting-helm-charts),
[a git-based helm repository](repo/README.md), and the
[primary CMDAA charts](cmdaa/README.md).


## Supporting helm-charts
Several supporting helm-charts are cached in this repository.

Some contain minor modifications to improve their functionality when used
within an umbrella chart, which others are included to ensure their continued
availability to CMDAA.


### Charts with minor modifications
The primary changes are to allow parental over-rides of the full name of
primary services. This enables programatically plugging the various helm charts
together into a cohesive system.
- [argo 0.9.8](argo/0.9.8-cmdaa)
- [elasticsearch 12.6.2](elasticsearch/12.6.2-cmdaa)
- [timescaledb 0.5.1](timescaledb/0.5.1-cmdaa)
- [timescaledb 0.6.2](timescaledb/0.6.2-cmdaa)


### Charts cached for availability
In addition, several helm charts are cached to ensure their continued
availablity to CMDAA:
- [minio 5.0.32](minio/5.0.32) 
- [postgresql 6.0.0](postgresql/6.0.0) 
- [prometheus 11.4.0](prometheus/11.4.0) 
- [prometheus-kafka-adapter 0.1.0](prometheus-kafka-adapter/0.1.0) 

- timescaledb
  - [timescaledb 0.8.2](timescaledb/0.8.2)
  - [tobs 0.1.4](timescaledb/tobs/0.1.4) : The Observability Stack for Kubernetes
  - [promscale 0.1.4](timescaledb/promscale/0.1.4) : Prometheus-compilant
    read/write connector for Timescaledb;
