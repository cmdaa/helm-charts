apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  namespace: cmdaa
  generateName: cmdaa-test-fluent-config-
  labels:
    analytic: cmdaa-test-fluent-config
    type: analytic

spec:
  entrypoint: test-fluentd-config
  ttlSecondsAfterFinished: 86400
  arguments:
    parameters:
    - name: logsource-id
      value: "0"
    - name: elastic-id
      value: "0"
    - name: s3input
      value: "myuser/myfile/Hadoop-2k"
    - name: minio-url
      value: "minio:9000"
    - name: bucket
      value: "argo-workflow-bucket"
    - name: minio-server
      value: "minio"
    - name: server-name
      value: elastic-kibana-0-0

  templates:

  - name: test-fluentd-config
    dag:
      tasks:
      - name: start-kibana-elasticsearch
        template: kibana-elastic
      - name: expose-kibana-elasticsearch
        dependencies: [start-kibana-elasticsearch]
        template: kibana-elastic-expose-ports
      - name: create-kibana-ingress
        dependencies: [expose-kibana-elasticsearch]
        template: create-ingress
      - name: pull-log-file
        template: pull-log-file
      - name: pull-log-file-output
        dependencies: [pull-log-file]
        template: print-output
        arguments:
          artifacts:
          - name: print-stage-output
            from: "{{tasks.pull-log-file.outputs.artifacts.cmda-es-pull-data}}"
      - name: generate-fluentd
        dependencies: [expose-kibana-elasticsearch]
        template: generate-fluentd-conf
      - name: print-fluentd-config
        dependencies: [generate-fluentd]
        template: print-output
        arguments:
          artifacts:
          - name: print-stage-output
            from: "{{tasks.generate-fluentd.outputs.artifacts.config-file-output}}"
      - name: elastic-check
        dependencies: [expose-kibana-elasticsearch]
        template: wait-for-elastic
      - name: kibana-check
        dependencies: [expose-kibana-elasticsearch]
        template: wait-for-kibana
      - name: run-fluentd
        dependencies: [generate-fluentd, pull-log-file, elastic-check, kibana-check]
        template: run-fluentd
        arguments:
          artifacts:
          - name: fluentd-config
            from: "{{tasks.generate-fluentd.outputs.artifacts.config-file-output}}"
          - name: log-file
            from: "{{tasks.pull-log-file.outputs.artifacts.cmda-es-pull-data}}"


  #  - name: kibana-elastic
  #    container:
  #      image: cmdaa/matt-ek:latest

  - name: kibana-elastic
    inputs:
      artifacts:
      - name: kubectl
        path: /bin/kubectl
        mode: 0755
        http:
          url: https://storage.googleapis.com/kubernetes-release/release/v1.18.3/bin/linux/amd64/kubectl
    #            url: https://storage.googleapis.com/kubernetes-release/release/v1.8.0/bin/linux/amd64/kubectl
    container:
      image: debian
      command: [sh, -c]
      args: ["kubectl create deployment elastic-kibana-{{workflow.parameters.logsource-id}}-{{workflow.parameters.elastic-id}} --image=cmdaa/elastic-kibana:latest"]
      env:
      - name: "NETWORK_HOST"
        value: "_site_,_lo_"

  - name: kibana-elastic-expose-ports
    inputs:
      artifacts:
      - name: kubectl
        path: /bin/kubectl
        mode: 0755
        http:
          url: https://storage.googleapis.com/kubernetes-release/release/v1.18.3/bin/linux/amd64/kubectl
    container:
      image: debian
      command: [sh, -c]
      args: ["kubectl expose pod --namespace=$(kubectl get pod --all-namespaces -l app=elastic-kibana-{{workflow.parameters.logsource-id}}-{{workflow.parameters.elastic-id}} -o jsonpath=\"{.items[0].metadata.namespace}\") $(kubectl get pod --all-namespaces -l app=elastic-kibana-{{workflow.parameters.logsource-id}}-{{workflow.parameters.elastic-id}} -o jsonpath=\"{.items[0].metadata.name}\") --port=9200,9300,5601,24224 --name=elastic-kibana-{{workflow.parameters.logsource-id}}-{{workflow.parameters.elastic-id}}"]

  - name: generate-fluentd-conf
    container:
      image: 'cmdaa/generate-fluentd:latest'
      command: [sh, -c]
      #      args: ["/usr/local/bin/scala -classpath ${CMDAA_JAR} ${ANALYTIC}", "-a", "{{inputs.parameters.analytic-name}}", "-o", "/tmp", "-l", "{{inputs.parameters.ll-threshold}}", "-c", "{{inputs.parameters.cosine-sim-threshold}}"]
      args: ["/usr/local/bin/scala -classpath ${JAR_FILE} ${ANALYTIC}"]
      env:
      - name: ANALYTIC
        value: io.cmdaa.fluentd.PullGroks
      #      - name: JAVA_OPTS
      #        value: "-Xms1g -Xmx1g"
      - name: JAR_FILE
        value: "/usr/share/cmdaa/generate-fluentd.jar"
      - name: GROK_CONFIG_ID
        value: "0"
      - name: OUTPUT
        value: "/usr/share/cmdaa/output/fluentd.conf"
      - name: DATABASE_URL
        valueFrom:
          configMapKeyRef:
            name: postgresdb-config
            key: cmdaa.postgres.db.url
    outputs:
      artifacts:
      - name: config-file-output
        path: "/usr/share/cmdaa/output/fluentd.conf"

  - name: pull-log-file
    inputs:
      artifacts:
      - name: analytic-input
        path: /tmp/analytic-input
        s3:
          # Use the corresponding endpoint depending on your S3 provider:
          #   AWS: s3.amazonaws.com
          #   GCS: storage.googleapis.com
          #   Minio: my-minio-endpoint.default:9000
          endpoint: '{{workflow.parameters.minio-url}}'
          bucket: '{{workflow.parameters.bucket}}'
          #          bucket: argo-workflow-bucket

          # argument passed in
          key: '{{workflow.parameters.s3input}}'
          insecure: true

          # accessKeySecret and secretKeySecret are secret selectors.
          # It references the k8s secret named 'my-s3-credentials'.
          # This secret is expected to have have the keys 'accessKey'
          # and 'secretKey', containing the base64 encoded credentials
          # to the bucket.
          accessKeySecret:
            name:  '{{workflow.parameters.minio-server}}'
            key: accesskey
          secretKeySecret:
            name:  '{{workflow.parameters.minio-server}}'
            key: secretkey
    container:
      image: alpine:latest
      command: [sh, -c]
      args: ["cat /tmp/analytic-input > /tmp/cmda-es-pull-data.out"]
    outputs:
      artifacts:
      - name: cmda-es-pull-data
        path: /tmp/cmda-es-pull-data.out

  - name: run-fluentd
    inputs:
      artifacts:
      - name: fluentd-config
        path: /root/fluentd.conf
      - name: log-file
        path: /root/input.log
    container:
      image:  cmdaa/fluentd-log-process:latest
      command: [bash, -c]
      args: ["/root/entrypoint"]


  - name: wait-for-kibana
    container:
      image: ubuntu:20.04
      command: [sh, -c]
      args: ["apt update; apt install curl -y; timeout 300 bash -c 'while [[ \"$(curl --insecure -s -o /dev/null -w ''%{http_code}'' http://elastic-kibana-0-0:5601/status)\" != \"200\" ]]; do sleep 10; done'; sleep 11"]

  - name: wait-for-elastic
    container:
      image: ubuntu:20.04
      command: [sh, -c]
      args: ["apt update; apt install curl -y; timeout 300 bash -c 'while [[ \"$(curl --insecure -s -o /dev/null -w ''%{http_code}'' http://elastic-kibana-0-0:5601/status)\" != \"200\" ]]; do sleep 10; done'; sleep 11"]

  #    - name: wait-for-elastic
  #      container:
  #        image: ubuntu:20.04
  #        command: [sh, -c]
  #        args: ["apt update: apt install netstat -y; timeout 300 bash -c while netstat -antu|grep  -q 10.1.1.1; sleep 10; done; sleep 11"]
  ##        args: ["apt update; apt install curl -y; timeout 300 bash -c 'while [[ \"$(curl --insecure -s -o /dev/null -w ''%{http_code}'' http://elastic-kibana-0-0:24224)\" != \"200\" ]]; do sleep 10; done'; sleep 11"]

  ######################################################################################################################
  - name: create-ingress
    resource:
      action: create
      manifest: |               #put your kubernetes spec here
        apiVersion: extensions/v1beta1
        kind: Ingress
        metadata:
          annotations:
            certmanager.k8s.io/issuer: cmdaa-ca-issuer
            kubernetes.io/ingress.class: "nginx"
            ingress.kubernetes.io/proxy-body-size: "50m"
            nginx.org/proxy-connect-timeout: "30s"
            nginx.org/proxy-read-timeout: "20s"
            nginx.org/client-max-body-size: "50m"
          name: elasticsearch-kibana-0-0
        spec:
          rules:
          - host: {{workflow.parameters.server-name}}.test
            http:
              paths:
              - backend:
                  serviceName: {{workflow.parameters.server-name}}
                  servicePort: 5601
                path: /
          tls:
          - hosts:
            - {{workflow.parameters.server-name}}.test
            secretName: workflow-svc-tls
  - name: print-output
    inputs:
      artifacts:
      - name: print-stage-output
        path: /tmp/message
    container:
      image: alpine:3.7
      command: [sh, -c]
      args: ["cat /tmp/message"]