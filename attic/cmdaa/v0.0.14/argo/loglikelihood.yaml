apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: cmdaa-loglikelihood-
  labels:
    analytic: cmdaa-loglikelihood
    type: analytic

#Using a directed acyclical graph for the analytic engine.  You are able to have multiple nodes at each
#layer in the graph.
spec:
  entrypoint: graph
  ttlSecondsAfterFinished: 86400
  arguments:
    parameters:
    - name: analytic-name
      value: "hello"
    - name: ll-threshold
      value: 0.0
    - name: cosine-sim-threshold
      value: 100.0

  templates:
  - name: graph
    dag:
      tasks:
      - name: pull-data
        template: cmda-es-pull-data

      - name: pull-data-output
        dependencies: [pull-data]
        template: print-output
        arguments:
          artifacts:
          - name: print-stage-output
            from: "{{tasks.pull-data.outputs.artifacts.cmda-es-pull-data}}"

      - name: run-loglikelihood
        dependencies: [pull-data]
        template: cmdaa-loglikelihood
        arguments:
          artifacts:
          - name: analytic-input
            from: "{{tasks.pull-data.outputs.artifacts.cmda-es-pull-data}}"

      - name: ll-out
        dependencies: [run-loglikelihood]
        template: print-output
        arguments:
          artifacts:
          - name: print-stage-output
            from: "{{tasks.run-loglikelihood.outputs.artifacts.hadoop-out}}"

      - name: ll-clusters-out
        dependencies: [run-loglikelihood]
        template: print-output
        arguments:
          artifacts:
          - name: print-stage-output
            from: "{{tasks.run-loglikelihood.outputs.artifacts.hadoop-clusters-out}}"

      - name: run-cluster-hashes
        dependencies: [pull-data, run-loglikelihood]
        template: cluster-hashes
        arguments:
          artifacts:
          - name: analytic-input
            from: "{{tasks.pull-data.outputs.artifacts.cmda-es-pull-data}}"
          - name: cluster-hashes
            from: "{{tasks.run-loglikelihood.outputs.artifacts.hadoop-clusters-out}}"

      - name: run-output-db
        dependencies: [pull-data, run-loglikelihood]
        template: output-db
        arguments:
          artifacts:
          - name: analytic-input
            from: "{{tasks.pull-data.outputs.artifacts.cmda-es-pull-data}}"
          - name: cluster-hashes
            from: "{{tasks.run-loglikelihood.outputs.artifacts.hadoop-clusters-out}}"

      - name: cluster-hashes-out
        dependencies: [run-cluster-hashes]
        template: print-output
        arguments:
          artifacts:
          - name: print-stage-output
            from: "{{tasks.run-cluster-hashes.outputs.artifacts.cluster-hashes-out}}"

      - name: run-multi-line
        dependencies: [pull-data, run-loglikelihood]
        template: multi-line-detector
        arguments:
          artifacts:
          - name: analytic-input
            from: "{{tasks.pull-data.outputs.artifacts.cmda-es-pull-data}}"
          - name: cluster-hashes
            from: "{{tasks.run-loglikelihood.outputs.artifacts.hadoop-clusters-out}}"

      - name: multi-line-out
        dependencies: [run-multi-line]
        template: print-output
        arguments:
          artifacts:
          - name: print-stage-output
            from: "{{tasks.run-multi-line.outputs.artifacts.multiline-out}}"


  #  ######################################################################################################################
  #  # cdma-es-pull-data:
  #  #    This stage of the analytic will pull the logs from the index that is specified.  Things that can be specified
  #  #    for this analytic:
  #  #    ES_HOST:
  #  #             This is server and port that elasticsearch is currently running.
  #  #             eg... localhost:9200
  #  #    INDEX_TO_PULL:
  #  #             This is the index where your log messages reside.
  #  #             Best to use a wildcard.
  #  #             eg... yum*, messages*, hdfs*
  #  #    LOG_FIELD:
  #  #             This is the field that contains the raw log messages
  #  #             The default is "field": "message"
  #  #    RECS_TO_PULL:
  #  #             You can analyze between 1 and 1,000,000 log messages.  It's all up to you.
  #  #             Best to have appropriate resources dedicated fir processing a large number of
  #  #             log messages.
  #  ######################################################################################################################
  #  - name: cmda-es-pull-data
  #    container:
  #      resources:
  #        requests:
  #          memory: 5Gi
  #          cpu: 1200m
  #      image: cmdaa/cmdaa-workflow-pull-es:latest
  #      env:
  #      - name: ES_HOST
  #        valueFrom:
  #          configMapKeyRef:
  #            name: es-config
  #            key: cmda.es.url
  #      - name: INDEX_TO_PULL
  #        valueFrom:
  #          configMapKeyRef:
  #            name: es-config
  #            key: cmda.es.index
  #      - name: LOG_FIELD
  #        valueFrom:
  #          configMapKeyRef:
  #            name: es-config
  #            key: cmda.es.field
  #      - name: RECS_TO_PULL
  #        valueFrom:
  #          configMapKeyRef:
  #            name: es-config
  #            key: cmda.es.query.size
  #      - name: SORT_FIELD
  #        valueFrom:
  #          configMapKeyRef:
  #            name: es-config
  #            key: cmda.es.sortfield
  #    outputs:
  #      artifacts:
  #      - name: cmda-es-pull-data
  #        path: /tmp/cmda-es-pull-data.out

  - name: cmda-es-pull-data
    inputs:
      artifacts:
      - name: analytic-input
        path: /tmp/analytic-input
        s3:
          # Use the corresponding endpoint depending on your S3 provider:
          #   AWS: s3.amazonaws.com
          #   GCS: storage.googleapis.com
          #   Minio: my-minio-endpoint.default:9000
          endpoint: '{{workflow.parameters.minio-url}}'
          bucket: '{{workflow.parameters.bucket}}'
#          bucket: argo-workflow-bucket

          # argument passed in
          key: '{{workflow.parameters.s3input}}'
          insecure: true

          # accessKeySecret and secretKeySecret are secret selectors.
          # It references the k8s secret named 'my-s3-credentials'.
          # This secret is expected to have have the keys 'accessKey'
          # and 'secretKey', containing the base64 encoded credentials
          # to the bucket.
          accessKeySecret:
            name:  '{{workflow.parameters.minio-server}}'
            key: accesskey
          secretKeySecret:
            name:  '{{workflow.parameters.minio-server}}'
            key: secretkey
    container:
      image: alpine:latest
      command: [sh, -c]
      args: ["cat /tmp/analytic-input > /tmp/cmda-es-pull-data.out"]
    outputs:
      artifacts:
      - name: cmda-es-pull-data
        path: /tmp/cmda-es-pull-data.out

  ######################################################################################################################
  # cmdaa-loglikelihood:
  #    This stage of the analytic will run a log likelihood analytic against all log lines pulled in the
  #    prior stage.   Once the log likelihood analytic is run against the log line, templates are created.
  #    These templates are the basis for generating log message types.  We run the log lines through a
  #    cosinesimilarity algorithm that will generate a unique id for each message type.  A map reduce routine
  #    will get rid of identical messages and then cluster messages based on their cosine similarity.
  #
  #    The input for this analytic is strictly based on the output of the first stage.
  ######################################################################################################################
  - name: cmdaa-loglikelihood
    inputs:
      artifacts:
      - name: analytic-input
        path: /analytic-input
    container:
      resources:
        requests:
          memory: 7Gi
          cpu: 1500m
      image: 'cmdaa/log-likelihood:{{workflow.parameters.build-version}}'
      command: [sh, -c]
      #      args: ["/usr/local/bin/scala -classpath ${CMDAA_JAR} ${ANALYTIC}", "-a", "{{inputs.parameters.analytic-name}}", "-o", "/tmp", "-l", "{{inputs.parameters.ll-threshold}}", "-c", "{{inputs.parameters.cosine-sim-threshold}}"]
      args: ["/usr/local/bin/scala -classpath ${CMDAA_JAR} ${ANALYTIC} ${PARAMETERS}"]
      env:
      - name: ANALYTIC
        value: org.cmdaa.scala.analytics.LogLikelihoodConnectComponents
      - name: PARAMETERS
        #          value: "-a hadoop -i /analytic-input -o /tmp -l 0.05 -c 0.90"
        value: '-a {{workflow.parameters.analytic-name}} -i /analytic-input -o /tmp -l {{workflow.parameters.ll-threshold}} -c {{workflow.parameters.cosine-sim-threshold}}'
      - name: JAVA_OPTS
        value: "-Xms16g -Xmx16g"
    outputs:
      artifacts:
      - name: hadoop-out
        path: /tmp/hadoop.out
      - name: hadoop-clusters-out
        path: /tmp/hadoop-clusters.out
  ######################################################################################################################
  # cluster-hashes:
  #    Cluster hashes will take all of the connected component id's generated in the prior stage and assign every
  #    log message that was ingested it's cluster ID.
  #
  #    This is useful when checking the output of the clustering to see if it acceptable.
  ######################################################################################################################
  - name: cluster-hashes
    inputs:
      artifacts:
      - name: analytic-input
        path: /analytic-input
      - name: cluster-hashes
        path: /cluster-hashes
    outputs:
      artifacts:
      - name: cluster-hashes-out
        path: /tmp/hadoop.out
    container:
      image: 'cmdaa/log-likelihood:{{workflow.parameters.build-version}}'
      command: [sh, -c]
      args: ["/usr/local/bin/scala -classpath ${CMDAA_JAR} ${ANALYTIC} ${PARAMETERS}"]
      env:
      - name: ANALYTIC
        value: org.cmdaa.scala.analytics.OutputClusterId
      - name: PARAMETERS
#        value: "-a hadoop -i /analytic-input -o /tmp -l 0.05 -h /cluster-hashes"
        value: '-a {{workflow.parameters.analytic-name}} -i /analytic-input -o /tmp -l {{workflow.parameters.ll-threshold}} -h /cluster-hashes'
      - name: JAVA_OPTS
        value: "-Xms16g -Xmx16g"
  ######################################################################################################################
  # output-db:
  #    Output database  will take all of the connected component id's generated in the prior stage and store suggested
  #    groks and the related log messages.
  #
  #    This will be used to store all the output of the analytics.
  ######################################################################################################################
  - name: output-db
    inputs:
      artifacts:
      - name: analytic-input
        path: /analytic-input
      - name: cluster-hashes
        path: /cluster-hashes
#    outputs:
#      artifacts:
#      - name: cluster-hashes-out
#        path: /tmp/hadoop.out
    container:
      image: 'cmdaa/log-likelihood:{{workflow.parameters.build-version}}'
      command: [sh, -c]
      args: ["/usr/local/bin/scala -classpath ${CMDAA_JAR} ${ANALYTIC} ${PARAMETERS}"]
      env:
      - name: ANALYTIC
        value: org.cmdaa.scala.analytics.SaveGroksToDb
      - name: PARAMETERS
        #        value: "-a hadoop -i /analytic-input -o /tmp -l 0.05 -h /cluster-hashes"
        value: '-a {{workflow.parameters.analytic-name}} -i /analytic-input -o /tmp -l {{workflow.parameters.ll-threshold}} -h /cluster-hashes -f {{workflow.parameters.log-file-id}}'
      - name: JAVA_OPTS
        value: "-Xms16g -Xmx16g"
      - name: DATABASE_URL
        valueFrom:
          configMapKeyRef:
            name: postgresdb-config
            key: cmdaa.postgres.db.url
#      - name: DATABASE_URL
#        valueFrom:
#          secretKeyRef:
#            name: postgresdb-secret
#            key: cmdaa.postgres.db.url
  ######################################################################################################################
  # multi-line-detector:
  #    This stage of the analytic will
  ######################################################################################################################
  - name: multi-line-detector
    inputs:
      artifacts:
      - name: analytic-input
        path: /analytic-input
      - name: cluster-hashes
        path: /cluster-hashes
    outputs:
      artifacts:
      - name: multiline-out
        path: /tmp/multi-line.out
    container:
      image: 'cmdaa/log-likelihood:{{workflow.parameters.build-version}}'
      command: [sh, -c]
      args: ["/usr/local/bin/scala -classpath ${CMDAA_JAR} ${ANALYTIC} ${PARAMETERS}"]
      env:
      - name: ANALYTIC
        value: org.cmdaa.scala.analytics.DetectMultilineMessages2
      - name: PARAMETERS
#        value: "-a hadoop -i /analytic-input -o /tmp -l 0.05 -h /cluster-hashes -s 2 -d 1"
        value: '-a {{workflow.parameters.analytic-name}} -i /analytic-input -o /tmp -l {{workflow.parameters.ll-threshold}} -h /cluster-hashes -s 2 -d 1'
      - name: JAVA_OPTS
        value: "-Xms16g -Xmx16g"
  ######################################################################################################################
  - name: print-output
    inputs:
      artifacts:
      - name: print-stage-output
        path: /tmp/message
    container:
      image: alpine:3.7
      command: [sh, -c]
      args: ["cat /tmp/message"]