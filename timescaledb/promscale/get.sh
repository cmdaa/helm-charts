#!/bin/bash
#
# If the timescale helm repo has an incorrect index.yaml then helm won't be
# able to pull charts.
#     curl -Lkfs https://charts.timescale.com/index.yaml
#
# In this case, the files are likely still in the repo, just no longer indexed.
#     curl -Lkfs https://charts.timescale.com/tobs-0.0.1.tgz -o tobs-0.0.1.tgz
#
BASE_URL="https://charts.timescale.com"

REPO=$(basename "$(pwd)") #"tobs"
#REPO="timescaledb-single"

echo -n ">>> Refresh $REPO index.yaml ..."
curl -Lkfs "$BASE_URL/index.yaml" -o index-new.yaml
rc=$?

if [[ $rc -eq 0 ]]; then
  # Is this new index.yaml different from any we already have?
  echo
  if [[ -f index.yaml ]]; then
    MD5_OLD=$( md5sum index.yaml      | awk '{print $1}')
    MD5_NEW=$( md5sum index-new.yaml  | awk '{print $1}')

    if [[ "$MD5_OLD" != "$MD5_NEW" ]]; then
      # index.yaml change
      mv -f index-new.yaml index.yaml
      rm -f versions.txt

      echo "--- Updated index.yaml"
    else
      rm -f index-new.yaml

      echo "--- No change to index.yaml"
    fi

  else
    echo "--- Fetched index.yaml"
    mv -f index-new.yaml index.yaml
    rm -f versions.txt
  fi

else
  echo "*** Update of index.yaml FAILED [ $rc ]"
  exit $rc
fi

if [[ ! -f versions.txt ]]; then
  echo "--- Generate version info for $REPO ..."

  awk '
    /^  [a-z-]+:/ { if ( $1 == "'$REPO':" ) { inRepo = 1; inUrls = 0 }
                    else                    { inRepo = 0; inUrls = 0 }
                    next;
                  }
    / urls:/      { if (inRepo)             { inUrls = 1 } next }
    / - /         { if (inRepo && inUrls)   { print $2 }   next }
    {
      #printf("%s%s: %s\n",
      #        (inRepo ? "Repo" : "IGNR"),
      #        (inUrl  ? "-urls": "     "),
      #        $0);
      inUrls = 0;
    }' index.yaml > versions.txt

fi

if [[ ! -s versions.txt ]]; then
  echo "*** No $REPO versions found, populate with known versions ..."
  cat <<EOF > versions.txt
$REPO-0.1.0.tgz
$REPO-0.1.1.tgz
$REPO-0.1.2.tgz
$REPO-0.1.3.tgz
$REPO-0.1.4.tgz
EOF
#else
#  echo "--- Use existing versions.txt"
fi

VERS=()
if [[ -f versions.txt ]]; then
  VERS=( $(cat versions.txt) )
fi

#echo "${#VERS[@]} $REPO versions [ ${VERS[@]} ]"
#exit

for tarball in ${VERS[@]}; do
  version=$(echo "$tarball" | sed 's/'$REPO'-//;s/.tgz$//')

  if [[ ! -d "$version" ]]; then
    URL="$BASE_URL/$tarball"

    echo -n "--- Pull version $version [ $URL ] ..."
    curl -Lkfs "$URL" | tar xz
    rc_curl=${PIPESTATUS[0]} rc_tar=${PIPESTATUS[1]} rc=$?

    if [[ $rc_curl -ne 0 ]]; then
      echo "* Fetch FAILED [ $rc_curl ]"

    elif [[ $rc_tar -ne 0 ]]; then
      echo "* untar FAILED [ $rc_tar ]"

    else
      echo -n ". "
      mv "$REPO" "$version"
      [[ $? -eq 0 ]] && echo "done"

    fi

  else
    echo "--- Already have version $version : skip"
  fi
done
