#!/bin/bash
#
# Show the projects in the index.yaml within this directory.
#
if [[ -f index.yaml ]]; then
  #awk '/^  [a-z]/{
  #  name = substr($1,1,length($1)-1);
  #
  #  printf("  %s\n", name);
  #}' index.yaml
  awk '
    /^  [a-z-]+:/ {
      repo    = substr($1,1,length($1)-1);
      repoLen = length(repo);

      if (outLen > 0) { printf("\n"); outLen = 0 }

      printf("\n%s\n", repo);

      inRepo = 1;
      inUrls = 0;
      next;
    }
    / urls:/ {
      if (inRepo) { inUrls = 1 }
      next;
    }
    / - / {
      if (inRepo && inUrls) {
        # Stip off the repository prefix and .tgz suffix
        startChar = repoLen + 2;
        numChars  = length($2) - startChar - 3;
        vers      = substr($2, startChar, numChars);

        outStr  = sprintf("%-22s ", vers);
        versLen = length( outStr );

        #if (outLen > 0) { printf(", "); outLen += 2 }
        if (outLen + versLen > 78) {
          printf("\n");
          outLen = 0;
        }
        if (outLen < 1) {
          printf("  ");
          outLen = 2;
        }
        printf("%s", outStr);
        outLen += versLen;
      }
      next;
    }
    { inUrls = 0 }
    END{ printf("\n\n") }' index.yaml

else
  echo "*** There is no 'index.yaml' in this directory"
  exit -1
fi
