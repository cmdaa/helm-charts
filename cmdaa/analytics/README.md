This helm chart may be used to setup the analytics for CMDAA.

It requires an established, functional infrastructure comprised of:
- [s3-compatible storage service](#s3-storage-service);
- [kafka event streaming platform](#kafka-event-streaming);
- [argo workflow engine](#argo-workflow-engine);
- [mongo-db](#mongo-db);

Helm chart values:
<table>
 <thead>
  <tr><th>value</th><th>default</th><th>purpose</th></tr>
 </thead>
 <tbody>
  <tr>
   <td>image.pullPolicy</td><td>IfNotPresent</td>
   <td>Set the image pull policy for this chart</td>
  </tr>

  <tr>
   <td>dao.image.name</td><td>cmdaa/cmdaa-services</td>
   <td>dao services docker image name</td>
  </tr>
  <tr>
   <td>dao.image.tag</td><td>0.0.12</td>
   <td>dao services docker image tag</td>
  </tr>
  <tr>
   <td>dao.service.name</td><td>cmdaa</td>
   <td>dao services service name</td>
  </tr>
  <tr>
   <td>dao.service.port</td><td>8080</td>
   <td>dao services service port</td>
  </tr>
  <tr>
   <td>dao.ingressHost</td><td>dao.rest</td>
   <td>dao services ingress host name</td>
  </tr>
  <tr>
   <td>dao.helper.init.name</td><td>postgres</td>
   <td>docker image name for the postgres initialization helper (prepares for migration)</td>
  </tr>
  <tr>
   <td>dao.helper.init.tag</td><td>11-alpine</td>
   <td>docker image tag for the postgres initialization helper</td>
  </tr>
  <tr>
   <td>dao.helper.migrate.name</td><td>cmdaa/cmdaa-data</td>
   <td>docker image name for the postgres mirgration helper</td>
  </tr>
  <tr>
   <td>dao.helper.migrate.tag</td><td>0.0.12</td>
   <td>docker image tag for the postgres mirgration helper</td>
  </tr>
  <tr>
   <td>s3.image.name</td><td>cmdaa/cmdaa-service-s3</td>
   <td>s3 services docker image name</td>
  </tr>
  <tr>
   <td>s3.image.tag</td><td>0.0.12</td>
   <td>s3 services docker image tag</td>
  </tr>
  <tr>
   <td>s3.service.name</td><td>s3</td>
   <td>s3 services service name</td>
  </tr>
  <tr>
   <td>s3.service.port</td><td>8080</td>
   <td>s3 services service port</td>
  </tr>
  <tr>
   <td>s3.ingressHost</td><td>s3</td>
   <td>s3 services ingress host name</td>
  </tr>

  <tr>
   <td>fluentdGen.image.name</td><td>cmdaa/cmdaa-fluentd</td>
   <td>fluentdGen services docker image name</td>
  </tr>
  <tr>
   <td>fluentdGen.image.tag</td><td>0.0.7</td>
   <td>fluentdGen services docker image tag</td>
  </tr>
  <tr>
   <td>fluentdGen.service.name</td><td>fluentd-gen</td>
   <td>fluentdGen services service name</td>
  </tr>
  <tr>
   <td>fluentdGen.service.port</td><td>8080</td>
   <td>fluentdGen services service port</td>
  </tr>

  <tr>
   <td>ui.image.name</td><td>cmdaa/cmdaa-ui</td>
   <td>ui services docker image name</td>
  </tr>
  <tr>
   <td>ui.image.tag</td><td>0.0.12</td>
   <td>ui services docker image tag</td>
  </tr>
  <tr>
   <td>ui.service.name</td><td>cmdaa-ui</td>
   <td>ui services service name</td>
  </tr>
  <tr>
   <td>ui.service.port</td><td>80</td>
   <td>ui services service port</td>
  </tr>
  <tr>
   <td>ui.service.bodySize</td><td>10m</td>
   <td>ui services service body-size limit</td>
  </tr>
  <tr>
   <td>ui.ingressHost</td><td>cmdaa-ui</td>
   <td>ui services ingress host name</td>
  </tr>

  <tr>
   <td>ingress.enabled</td><td>false</td>
   <td>should ingress rules be enabled?</td>
  </tr>
  <tr>
   <td>ingress.dn</td><td>test</td>
   <td>
    if ingress rules are enabled, the DNS domain name that should be used with each ingress host
   </td>
  </tr>

  <tr>
   <td>db.version</td><td>all</td>
   <td>the database version to use for migration?</td>
  </tr>
  <tr>
   <td>db.type</td><td>false</td>
   <td>the database type to use for migration</td>
  </tr>

  <tr>
   <td>postgres.host</td><td>postgres-postgresql</td>
   <td>postgres host name</td>
  </tr>
  <tr>
   <td>postgres.port</td><td>5432</td>
   <td>postgres port number</td>
  </tr>
  <tr>
   <td>postgres.db</td><td>cmdaa</td>
   <td>postgres database name</td>
  </tr>
  <tr>
   <td>postgres.user</td><td>cmdaa</td>
   <td>postgres user name</td>
  </tr>
  <tr>
   <td>postgres.secret.name</td><td>postgres-credentials</td>
   <td>the secret holding the postgres password for the named user (<code>postgres.user</code>)</td>
  </tr>
  <tr>
   <td>postgres.secret.key</td><td>cmdaa</td>
   <td>
    the name of the key within <code>postgres.secret.name</code> that holds the
    password for the named user (<code>postgres.user</code>)
   </td>
  </tr>

  <tr>
   <td>minio.server</td><td>cmdaa-artifacts</td>
   <td>minio/s3 host name</td>
  </tr>
  <tr>
   <td>minio.port</td><td>9000</td>
   <td>minio/s3 port number</td>
  </tr>
  <tr>
   <td>minio.bucket.service</td><td>cmdaa</td>
   <td>minio/s3 bucket for the s3 service</td>
  </tr>
  <tr>
   <td>minio.bucket.workflow</td><td>argo-workflow-bucket</td>
   <td>minio/s3 bucket for the workflow service</td>
  </tr>
  <tr>
   <td>minio.defaultDir</td><td>myuser/myfile</td>
   <td>minio/s3 default directory within <code>minio.bucket.service</code></td>
  </tr>
  <tr>
   <td>minio.secret.name</td><td>cmdaa-artifacts</td>
   <td>the name of the secret holding access credentials for minio/s3</td>
  </tr>
  <tr>
   <td>minio.secret.accesskey</td><td>accesskey</td>
   <td>
    the name of the key within <code>minio.secret.name</code> that holds the
    access key for minio/s3.
   </td>
  </tr>
  <tr>
   <td>minio.secret.secretkey</td><td>secretkey</td>
   <td>
    the name of the key within <code>minio.secret.name</code> that holds the
    secret key for minio/s3.
   </td>
  </tr>
 </tbody>
</table>


## S3 storage service
The s3-compatible storage service (e.g. [minio](https://min.io/)) provides
storage for uploaded log data as well as coordination between step within the
CMDAA analytic workflow. It **MUST** also serve as a direct artifact repository
for the [Argo workflow engine](#Argo-workflow-engine) since the CMDAA workflow
services rely on access to the Argo artifact repository.

This service must expose:
- connection information: service host / port ( `.Values.minio.server`, `.Values.minio.port` );
- authentication information:
  - the name of a secret containing two keys (access, secret) that may be used
    to authentiate to the given host/port with permission to create buckets,
    upload new objects, download existing objects ( `.Values.minio.secret.name` );
  - the name of the access key ( `.Values.minio.secret.accesskey` );
  - the name of the secret key ( `.Values.minio.secret.secretkey` );


## Mongo database service
The Mongo-based database service (e.g.
[mongodb](https://www.mongodb.com/) provides storage for user
information, ...

This service must expose:
- connection information: service host / port ( `.Values.mongo.server`, `.Values.mongo.port` );
- authentication information:
  - the name of a secret containing database passwords keyed by user name
    ( `.Values.mongo.secrets.name` );
  - the name of the key within the secret holding the user password
    ( `.Values.mongo.secrets.passwordKey` );
  - the name of an existing, application-level database user to create and use
    new databases ( `.Values.mongo.dbUsername` ) -- this is the user whose
    password is identified by the above secret;

## Argo workflow engine
The [argo](https://argoproj.github.io/) workflow engine provides a flexible
platform that hosts the primary analytic services of CMDAA.

Note that Argo provides a Custom Resource Definition that, along with resource
annotations, allows Argo to monitor and respond to resources either within an
entire k8s cluster or a specific namespace. As a result, no connection or
authentication information must be exposed.


Argo **MUST** be properly setup to use an existing artifact repository, which
is also accessible to CMDAA workflow services.

To use the CMDAA s3-service, argo **MUST** be setup with the following yaml:
```yaml
argo:
  useDefaultArtifactRepo: true  # activate 'artifactRepository'

  artifactRepository:
    s3:
      host:     {{ .Values.minio.server }}
      port:     {{ .Values.minio.port }}
      bucket:   {{ .Values.minio.bucket.workflow }}

      accessKeySecret:
        name:   {{ .Values.minio.secret.name }}
        key:    {{ .Values.minio.secret.accesskey }}

      secretKeySecret:
        name:   {{ .Values.minio.secret.name }}
        key:    {{ .Values.minio.secret.secretkey }}

```

---
**The remaining services are in relation to the optional CMDAA analytic streaming engine**

## Kafka event streaming
The [kafka](https://kafka.apache.org/) event streaming platform provides a
reliable connection between the disparate CMDAA services from endpoint
collection through streaming analytics to database/index storage.

*<i>The CMDAA log collection and analysis engine uses Kafka as it's default endpoint 
for it's log collection tasks, which are dynamically generated to work with Fluentd. 
Fluentd can be deployed to both kubernetes daemonsets or bare metal nodes.</i>

This service must expose:
- connection information: service host / port;
- authentication information ...

## Postgres database service
The postgres-based database service (e.g.
[postgres](https://www.postgresql.org/),
[timescaledb](https://www.timescale.com/)) provides storage for log metric information...

This service must expose:
- connection information: service host / port ( `.Values.postgres.host`, `.Values.postgres.port` );
- authentication information:
  - the name of a secret containing database passwords keyed by user name
    ( `.Values.postgres.secret.name` );
  - the name of the key within the secret holding the user password
    ( `.Values.postgres.secret.key` );
  - the name of an existing, application-level database user to create and use
    new databases ( `.Values.postgres.user` ) -- this is the user whose
    password is identified by the above secret;

