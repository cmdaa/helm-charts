apiVersion: argoproj.io/v1alpha1
kind: Workflow
metadata:
  generateName: stop-fluentd-daemon-
spec: {{- include "cmdaa.analytics.argo.specAdditions" . | nindent 2 }}
  entrypoint: delete-fluentd-daemonset
  podGC:
    strategy: OnWorkflowSuccess
  onExit: exit-handler
  ttlStrategy:
    secondsAfterSuccess: {{ .Values.argo.workflow.secondsAfterSuccess }}
    secondsAfterFailure: {{ .Values.argo.workflow.secondsAfterFailure }}
  arguments:
    parameters: # default parameters overridden via `argo submit`
    - name: mongo-log-id
      value: 5ff31f650e00000e003afe5a
    - name: mongo-run-id
      value: 5ff31f6a0e00000e003afe5b

    - name: mongo-daemon-name           # Sandbox elasticsearch server
      value: cmdaa-fluentd

  # *************************************************************************************************************************************************
  templates:
  - name: exit-handler
    steps:
      - - name: success
          templateRef:
            name: external-services
            template: status-to-grokstore
          when: "{{`{{workflow.status}}`}} == Succeeded"
          arguments:
            parameters: [
              { name: json-string,
                value: '{
                   "status": {
                     "isRunning" : false,
                     "isComplete": true,
                     "isError"   : false,
                     "message"   : "Daemonset delete completed."
                   }
                 }'
              },
              { name: log-id,
                value: "{{`{{workflow.parameters.mongo-log-id}}`}}"
              },
              { name: run-id,
                value: "{{`{{workflow.parameters.mongo-run-id}}`}}"
              }
            ]

        - name: failure
          templateRef:
            name: external-services
            template: status-to-grokstore
          when: "{{`{{workflow.status}}`}} != Succeeded"
          arguments:
            parameters: [
              { name: json-string,
                value: '{
                  "status": {
                    "isRunning" : false,
                    "isComplete": false,
                    "isError"   : true,
                    "message"   : "Failed. See Argo UI for details."
                  }
                }'
              },
              { name: log-id,
                value: "{{`{{workflow.parameters.mongo-log-id}}`}}"
              },
              { name: run-id,
                value: "{{`{{workflow.parameters.mongo-run-id}}`}}"
              }
            ]

  - name: delete-fluentd-daemonset
    dag:
      tasks:
        - name: status-deleting
          templateRef:
            name: external-services
            template: status-to-grokstore
          arguments:
            parameters: [
              { name: json-string,
                value: '{
                  "status": {
                    "isRunning" : true,
                    "isComplete": false,
                    "isError"   : false,
                    "message"   : "Deleting fluentd daemonset."
                  }
                }'
              },
              { name: log-id,
                value: "{{`{{workflow.parameters.mongo-log-id}}`}}"
              },
              { name: run-id,
                value: "{{`{{workflow.parameters.mongo-run-id}}`}}"
              }
            ]

        - name: get-params
          templateRef:
            name: external-services
            template: get-run-params
          arguments:
            parameters:
              - name: mongo-log-id
                value: "{{`{{workflow.parameters.mongo-log-id}}`}}"
              - name: mongo-run-id
                value: "{{`{{workflow.parameters.mongo-run-id}}`}}"

        - name: delete-daemonset
          dependencies: [ get-params ]
          template: delete-daemonset
          arguments:
            parameters:
              - name: daemonset-prefix
                value: "{{`{{tasks.get-params.outputs.parameters.daemonset-prefix}}`}}"

        - name: status-deleted
          dependencies: [ delete-daemonset ]
          templateRef:
            name: external-services
            template: status-to-grokstore
          arguments:
            parameters: [
              { name: json-string,
                value: '{
                  "status": {
                    "isRunning" : true,
                    "isComplete": false,
                    "isError"   : false,
                    "message"   : "Fluentd daemonset deleted."
                  }
                }'
              },
              { name: log-id,
                value: "{{`{{workflow.parameters.mongo-log-id}}`}}"
              },
              { name: run-id,
                value: "{{`{{workflow.parameters.mongo-run-id}}`}}"
              }
            ]

        - name: set-daemonset-removed
          dependencies: [ delete-daemonset ]
          templateRef:
            name: external-services
            template: daemon-status-to-grokstore
          arguments:
            parameters: [
              { name: json-string,
                value: '{"dsDeployed": false}'
              },
              { name: log-id,
                value: "{{`{{workflow.parameters.mongo-log-id}}`}}"
              },
              { name: run-id,
                value: "{{`{{workflow.parameters.mongo-run-id}}`}}"
              }
            ]


  # *************************************************************************************************************************************************
  - name: delete-daemonset
    container:
      image: '{{ .Values.kubectl.image }}:{{ .Values.kubectl.tag }}'
      command: [ sh, -c ]
      args:
        - kubectl delete daemonset "$DS_PREFIX-$LOG_ID-$RUN_ID"
      env:
        - name: NETWORK_HOST
          value: "_site_,_lo_"
        - name: DS_PREFIX
          value: '{{`{{inputs.parameters.daemonset-prefix}}`}}'
        - name: LOG_ID
          value: '{{`{{workflow.parameters.mongo-log-id}}`}}'
        - name: RUN_ID
          value: '{{`{{workflow.parameters.mongo-run-id}}`}}'
    inputs:
      parameters:
        - name: daemonset-prefix
# *************************************************************************************************************************************************
