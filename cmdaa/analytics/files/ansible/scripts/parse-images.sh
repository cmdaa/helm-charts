#!/bin/bash
input="$1"
URL="${MONGO_B}${MONGODB_PASS}${MONGO_E}"
echo "URL"
echo $URL
echo "**************************************************************************************"
echo "DROPPING COLLECTION"
mongo --eval "db.tdDockerImages.drop();" ${URL}
echo "**************************************************************************************"
group="empty"
while read imageTag
do
  #finding group which is contained in [group] bracket in ansible host file
  if [ "$imageTag" ] ; then
    RECORD='{ imageTag:'"\"$imageTag\""' }'
    mongo --eval "db.tdDockerImages.insert(${RECORD});" ${URL}
  fi
done < "$input"
echo "**************************************************************************************"