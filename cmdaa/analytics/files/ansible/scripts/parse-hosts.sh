#!/bin/bash
input="$1"
URL="${MONGO_B}${MONGODB_PASS}${MONGO_E}"
echo "URL"
echo $URL
echo "**************************************************************************************"
echo "DROPPING COLLECTION"
mongo --eval "db.ansibleServers.drop();" ${URL}
echo "**************************************************************************************"
group="empty"
while read hostName
do
  #finding group which is contained in [group] bracket in ansible host file
  if [ "$hostName" ] && [[ $hostName = *"["* ]] && [ $hostName != *":"* ] ; then
    group=$(echo $hostName | awk -F '[][]' '{print $2}')
  fi
  #finding server which is not contained within brackets in ansible host file
  if [ "$hostName" ] && [[ $hostName != *"["* ]] && [ $hostName != *":"* ] && [[ $hostName != *"ansible_user"* ]]; then
    RECORD='{ host:'"\"$hostName\""', serverGroup:'"\"$group\""' }'
    echo $RECORD
    mongo --eval "db.ansibleServers.insert(${RECORD});" ${URL}
  fi
done < "$input"
echo "**************************************************************************************"
