apiVersion: argoproj.io/v1alpha1
kind: WorkflowTemplate
metadata:
  name: external-services
spec: #{
  arguments:
    parameters:

  templates: #{
    ##
    # status-to-grokstore()
    #
    # Store processing status in the grok store for UI access
    #   http://${GROK_URL}/api/v1/logs/${LOG_ID}/runs/${RUN_ID}/status
    #
    # inputs.parameters:
    #   json-string   The JSON-formatted string representing status;
    #   log-id        The id of the target log;
    #   run-id        The id of the target log run;
    #
    #
    # The format of `json-string` should be:
    #     {
    #       "status": {
    #         "isRunning" : Boolean,
    #         "isComplete": Boolean,
    #         "isError"   : Boolean,
    #         "message"   : String
    #       }
    #     }
    #
    - name: status-to-grokstore #{
      inputs:
        parameters:
          - name: json-string
          - name: log-id
          - name: run-id

      container:
        image: curlimages/curl:7.75.0
        command: [ sh, -c ]
        args:
        - |
          echo "GROK_URL[ $(GROK_URL) ]"
          echo "LOG_ID  [ $(LOG_ID) ]"
          echo "RUN_ID  [ $(RUN_ID) ]"
          echo "JSON    : $(JSON)"
          curl -X PUT -H "Content-Type: application/json" \
               -d '$(JSON)' \
               http://$(GROK_URL)/api/v1/logs/$(LOG_ID)/runs/$(RUN_ID)/status

        env:
          - name: GROK_URL
            value: {{ .Values.grokstore.service.name }}

          - name: JSON
            value: '{{`{{inputs.parameters.json-string}}`}}'

          - name: RUN_ID
            value: '{{`{{inputs.parameters.run-id}}`}}'

          - name: LOG_ID
            value: '{{`{{inputs.parameters.log-id}}`}}'
    #} status-to-grokstore

    ##
    # daemon-status-to-grokstore()
    #
    # Store daemon status in the grok store for UI access
    #   http://${GROK_URL}/api/v1/logs/${LOG_ID}/runs/${RUN_ID}/dsDeployed
    #
    # inputs.parameters:
    #   json-string   The JSON-formatted string representing status;
    #   log-id        The id of the target log;
    #   run-id        The id of the target log run;
    #
    #
    # The format of `json-string` should be:
    #     { "dsDeployed"  : Boolean }
    #
    - name: daemon-status-to-grokstore #{
      inputs:
        parameters:
          - name: json-string
          - name: log-id
          - name: run-id

      container:
        image: curlimages/curl:7.75.0
        command: [ sh, -c ]
        args:
        - |
          echo "GROK_URL[ $(GROK_URL) ]"
          echo "LOG_ID  [ $(LOG_ID) ]"
          echo "RUN_ID  [ $(RUN_ID) ]"
          echo "JSON    : $(JSON)"
          curl -X PUT -H "Content-Type: application/json" \
               -d '$(JSON)' \
               http://$(GROK_URL)/api/v1/logs/$(LOG_ID)/runs/$(RUN_ID)/dsDeployed
        env:
          - name: GROK_URL
            value: {{ .Values.grokstore.service.name }}

          - name: JSON
            value: '{{`{{inputs.parameters.json-string}}`}}'

          - name: RUN_ID
            value: '{{`{{inputs.parameters.run-id}}`}}'

          - name: LOG_ID
            value: '{{`{{inputs.parameters.log-id}}`}}'
    #} daemon-status-to-grokstore

    ##
    # get-s3-folder-file()
    #
    # Given a full s3 path, split it into directory and filename
    #
    # inputs.parameters:
    #   s3-path   The target s3 path;
    #
    # outputs.parameters:
    #   s3-dir    The directory of `s3-path`;
    #   s3-file   The filename  of `s3-path`;
    #
    - name: get-s3-folder-file #{
      inputs:
        parameters:
          - name: s3-path

      outputs:
        parameters:
          - name: s3-dir
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /output/s3-dir.txt

          - name: s3-file
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /output/s3-file.txt

      container:
        image: '{{ .Values.generateFluentd.image }}:{{ .Values.generateFluentd.tag }}'
        command: [sh, -c]
        args:
        - |
          echo "IN_S3_PATH [ $IN_S3_PATH ]"
          echo "OUT_S3_FILE[ $OUT_S3_FILE ]"
          echo "OUT_S3_DIR [ $OUT_S3_DIR ]"

          # s3-file
          mkdir -p "$(dirname "$OUT_S3_FILE")"

          base="$(basename -- $IN_S3_PATH)"
          echo "basename: s3-file[ $base ] > OUT_S3_FILE"
          echo "$base" > "$OUT_S3_FILE"

          # s3-dir
          mkdir -p "$(dirname "$OUT_S3_DIR")"

          dir="$(dirname -- $IN_S3_PATH)"
          echo "dirname : s3-dir[ $dir ] > OUT_S3_DIR"
          echo "$dir" > "$OUT_S3_DIR"

        env:
          - name: IN_S3_PATH
            value: '{{`{{inputs.parameters.s3-path}}`}}'
          - name: OUT_S3_FILE
            value: '{{`{{outputs.parameters.s3-file.path}}`}}'
          - name: OUT_S3_DIR
            value: '{{`{{outputs.parameters.s3-dir.path}}`}}'
    #} get-s3-folder-file

    ##
    # get-run-params()
    #
    # Given a log and run id, retrieve the run parameters
    #
    # inputs.parameters:
    #   mongo-log-id    The id of the target log;
    #   mongo-run-id    The id of the target run;
    #
    # outputs.parameters:
    #   sys-file-path     The source path of the log data;
    #   sys-file          The source file of the log data;
    #   node-labels       The labels that identify the source node from which
    #                     the source file was pulled;
    #   daemonset-prefix  The prefix to be used for any daemonset generated
    #                     from this log data;
    #   log-likeli        The log-likelihood threshold used for the target run;
    #   cos-sim           The cosine similarity value used for the target run;
    #   s3-bucket         The s3 bucket containing the argo accessible/s3 copy
    #                     of the source log data;
    #   s3-path           The full path within `s3-bucket` to the source log
    #                     data;
    #   s3-dir            The "directory" of `s3-path`;
    #   s3-file           The "filename"  of `s3-path`;
    #
    #
    # This functionality is provided via:
    #   /usr/share/cmdaa/generate-fluentd.jar
    #     io.cmdaa.fluentd.mongo.params.fluent.DaemonSet
    #
    #   src: cmdaa/workflow/fluentd/generate-fluentd/src/main/java/
    #           io/cmdaa/fluentd/mongo/params/fluent/
    #
    #   Required environment variables:
    #     MONGODB_URL       [mongodb://cmdaa:cmdaa@localhost:6969/cmdaa]
    #     LOG_ID            inputs.parameters.mongo-log-id
    #     S3_BUCKET         < outputs.parameters.s3-bucket
    #     S3_PATH           < outputs.parameters.s3-path
    #     NUM_WORKERS
    #     SYS_FILE          < outputs.parameters.sys-file
    #     SYS_FILE_PATH     < outputs.parameters.sys-file-path
    #     NODE_LBLS         < outputs.parameters.node-labels
    #     DAEMON_PREFIX     < outputs.parameters.daemonset-prefix
    #
    #     GET_RUN_INFO      [false]
    #       RUN_ID          inputs.parameters.mongo-run-id
    #       LOG_LIKELI      < outputs.parameters.log-likeli
    #       COS_SIM         < outputs.parameters.cos-sim
    #
    - name: get-run-params #{
      inputs:
        parameters:
          - name: mongo-log-id
          - name: mongo-run-id

      outputs:
        parameters:
          - name: sys-file-path
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/directory-to-monitor.txt

          - name: sys-file
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/file-to-monitor.txt

          - name: node-labels
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/nodes-to-monitor.txt

          - name: daemonset-prefix
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: '{{.Values.fluentDaemonset.daemonPrefix}}'
              path: /usr/share/cmdaa/output/daemonset-prefix.txt

          - name: log-likeli
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/log-likeli.txt

          - name: cos-sim
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/cos-sim.txt

          - name: s3-path
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/s3-path.txt

          - name: s3-dir
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/s3-dir.txt

          - name: s3-file
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/s3-file.txt

          - name: s3-bucket
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/s3-bucket.txt

          - name: uploaded-file-name
            valueFrom:
              # Default value to use if retrieving valueFrom fails. If not
              # provided workflow will fail instead
              default: "missing"
              path: /usr/share/cmdaa/output/uploaded-file-name.txt

      container:
        image: '{{ .Values.generateFluentd.image }}:{{ .Values.generateFluentd.tag }}'
        command: [sh, -c]
        args:
        - |
          echo LOG_ID = [ $(LOG_ID) ]
          echo RUN_ID = [ $(RUN_ID) ]
          /scala-2.11.12/bin/scala -classpath ${JAR_FILE} ${ANALYTIC}
        env:
          - name: ANALYTIC
            value: io.cmdaa.fluentd.mongo.params.fluent.DaemonSet

          - name: JAR_FILE
            value: "/usr/share/cmdaa/generate-fluentd.jar"

          - name: MONGODB_PASS
            valueFrom:
              secretKeyRef:
                name: '{{ .Values.mongo.secrets.secret }}'
                key:  '{{ .Values.mongo.secrets.passwordKey }}'

          - name: MONGODB_URL
            value: 'mongodb://{{.Values.mongo.dbUsername}}:$(MONGODB_PASS)@{{.Values.mongo.server}}:{{.Values.mongo.port}}/{{.Values.mongo.database}}'

          - name: LOG_ID
            value: '{{`{{inputs.parameters.mongo-log-id}}`}}'

          - name: SYS_FILE
            value: '{{`{{outputs.parameters.sys-file.path}}`}}'
            #value: "/usr/share/cmdaa/output/file-to-monitor.txt"

          - name: SYS_FILE_PATH
            value: '{{`{{outputs.parameters.sys-file-path.path}}`}}'
            #value: "/usr/share/cmdaa/output/directory-to-monitor.txt"

          - name: NODE_LBLS
            value: '{{`{{outputs.parameters.node-labels.path}}`}}'
            #value: "/usr/share/cmdaa/output/nodes-to-monitor.txt"

          - name: DAEMON_PREFIX
            value: '{{`{{outputs.parameters.daemonset-prefix.path}}`}}'
            #value: "/usr/share/cmdaa/output/daemonset-prefix.txt"

          - name: GET_RUN_INFO
            value: "true"

          - name: RUN_ID
            value: '{{`{{inputs.parameters.mongo-run-id}}`}}'

          - name: LOG_LIKELI
            value: '{{`{{outputs.parameters.log-likeli.path}}`}}'
            #value: "/usr/share/cmdaa/output/log-likeli.txt"

          - name: COS_SIM
            value: '{{`{{outputs.parameters.cos-sim.path}}`}}'
            #value: "/usr/share/cmdaa/output/cos-sim.txt"

          - name: S3_PATH
            value: '{{`{{outputs.parameters.s3-path.path}}`}}'
            #value: "/usr/share/cmdaa/output/s3-path.txt"

          - name: S3_DIR
            value: '{{`{{outputs.parameters.s3-dir.path}}`}}'
            #value: "/usr/share/cmdaa/output/s3-dir.txt"

          - name: S3_FILE
            value: '{{`{{outputs.parameters.s3-file.path}}`}}'
            #value: "/usr/share/cmdaa/output/s3-file.txt"

          - name: S3_BUCKET
            value: '{{`{{outputs.parameters.s3-bucket.path}}`}}'
            #value: "/usr/share/cmdaa/output/s3-bucket.txt"

          - name: UPLOADED_FILE_NAME
            value: '{{`{{outputs.parameters.uploaded-file-name.path}}`}}'
            #value: "/usr/share/cmdaa/output/uploaded-file-name.txt"

    #} get-run-params

    ##
    # print-extracted-parameters()
    #
    # Debug: Print all parameters made available via `get-run-params`
    #
    # inputs.parameters:
    #   sys-file-path     The source path of the log data;
    #   sys-file          The source file of the log data;
    #   node-labels       The labels that identify the source node from which
    #                     the source file was pulled;
    #   daemonset-prefix  The prefix to be used for any daemonset generated
    #                     from this log data;
    #   log-likeli        The log-likelihood threshold used for the target run;
    #   cos-sim           The cosine similarity value used for the target run;
    #   s3-bucket         The s3 bucket containing the argo accessible/s3 copy
    #                     of the source log data;
    #   s3-path           The full path within `s3-bucket` to the source log
    #                     data;
    #   s3-dir            The "directory" of `s3-path`;
    #   s3-file           The "filename"  of `s3-path`;
    #
    - name: print-extracted-parameters #{
      container:
        image: alpine:latest
        command: [sh, -c]
        args:
        - |
          echo system-file-path = {{`{{inputs.parameters.sys-file-path}}`}}
          echo sys-file = {{`{{inputs.parameters.sys-file}}`}}
          echo node-labels = {{`{{inputs.parameters.node-labels}}`}}
          echo daemonset-prefix = {{`{{inputs.parameters.daemonset-prefix}}`}}
          echo log-likeli = {{`{{inputs.parameters.log-likeli}}`}}
          echo cos-sim = {{`{{inputs.parameters.cos-sim}}`}}
          echo s3-path = {{`{{inputs.parameters.s3-path}}`}}
          echo s3-dir = {{`{{inputs.parameters.s3-dir}}`}}
          echo s3-file = {{`{{inputs.parameters.s3-file}}`}}
          echo s3-bucket = {{`{{inputs.parameters.s3-bucket}}`}}
          echo uploaded-file-name = {{`{{inputs.parameters.uploaded-file-name}}`}}

      inputs:
        parameters:
          - name: sys-file-path
          - name: sys-file
          - name: node-labels
          - name: daemonset-prefix
          - name: log-likeli
          - name: cos-sim
          - name: s3-path
          - name: s3-dir
          - name: s3-file
          - name: s3-bucket
          - name: uploaded-file-name
    #} print-extracted-parameters

    ##
    # print-parmas()
    #
    # Debug: Print the provided input parameters
    #
    # inputs.parameters:
    #   minio-url   A full minio/s3 URL;
    #   bucket      An s3 bucket;
    #   s3input     An s3 path within `bucket`;
    #   secret      The secret used to access the target minio/s3;
    #
    - name: print-parmas #{
      inputs:
        parameters:
          - name: minio-url
          - name: bucket
          - name: s3input
          - name: secret

      container:
        image: alpine:latest
        command: [ sh, -c ]
        args:
        - |
          echo "{{`{{inputs.parameters.minio-url}}`}}"
          echo "{{`{{inputs.parameters.bucket}}`}}"
          echo "{{`{{inputs.parameters.s3input}}`}}"
          echo "{{`{{inputs.parameters.secret}}`}}"
    #} print-parmas

    ##
    # print-output()
    #
    # Debug: Output the provided artifact.
    #
    # inputs.artifacts:
    #   print-stage-output    The artifact to be output;
    #
    - name: print-output #{
      inputs:
        artifacts:
          - name: print-stage-output
            path: /tmp/message

      container:
        image: alpine:3.7
        command: [sh, -c]
        args:
        - cat '{{`{{inputs.artifacts.print-stage-output.path}}`}}'
    #} print-output

    ##
    # pull-log-file()
    #
    # Pull the data of a logfile from an argo artifact.
    #
    # inputs.parameters:
    #   minio-url           The full URL of the minio/s3 repository;
    #   bucket              The s3 bucket that holds the target log file;
    #   s3input             The s3 path within `bucket` of the log file;
    #   secret              The name of the k8s secret holding the access
    #                       credentials (secret and access keys);
    #
    # inputs.artifacts:
    #   analytic-input      The artifact that will be pulled from the minio/s3
    #                       repository using the input parameters and helm
    #                       values;
    #
    # outputs.artifacts:
    #   cmda-es-pull-data   The output artifact that will hold the contents of
    #                       the target log file [/tmp/cmda-es-pull-data.out];
    #
    - name: pull-log-file #{
      inputs:
        parameters:
          - name: minio-url
          - name: bucket
          - name: s3input
          - name: secret

        artifacts:
        - name: analytic-input
          path: /tmp/analytic-input
          s3:
            #
            # :XXX: This endpoint should really be:
            #       {{ .Values.minio.server }}:{{ .Values.minio.port }}
            #
            # Use the corresponding endpoint depending on your S3 provider:
            #   AWS: s3.amazonaws.com
            #   GCS: storage.googleapis.com
            #   Minio: my-minio-endpoint.default:9000
            endpoint: '{{`{{inputs.parameters.minio-url}}`}}'
            bucket:   '{{`{{inputs.parameters.bucket}}`}}'
            key:      '{{`{{inputs.parameters.s3input}}`}}'
            insecure: true
            #
            # :XXX: The name values below should really be:
            #       {{ .Values.minio.secret.name }}
            #
            # accessKeySecret and secretKeySecret are secret selectors.
            # It references the k8s secret named 'my-s3-credentials'.
            # This secret is expected to have have the keys 'accessKey'
            # and 'secretKey', containing the base64 encoded credentials
            # to the bucket.
            accessKeySecret:
              name: '{{`{{inputs.parameters.secret}}`}}'
              key:  '{{ .Values.minio.secret.accesskey }}'
            secretKeySecret:
              name: '{{`{{inputs.parameters.secret}}`}}'
              key:  '{{ .Values.minio.secret.secretkey }}'

      outputs:
        artifacts:
        - name: cmda-es-pull-data
          path: /tmp/cmda-es-pull-data.out

      container:
        image: alpine:3.13.1
        command: [sh, -c]
        args:
        - |
          echo "s3.endpoint [ {{`{{inputs.parameters.minio-url}}`}} ]"
          echo "  .bucket   [ {{`{{inputs.parameters.bucket}}`}} ]"
          echo "  .key      [ {{`{{inputs.parameters.s3input}}`}} ]"
          echo "    |"
          echo "    +-> INP_PATH     [ $INP_PATH ]"
          echo "          > OUT_PATH [ $OUT_PATH ]"

          cat "$INP_PATH" > "$OUT_PATH"

        env:
          - name:   INP_PATH
            value:  '{{`{{inputs.artifacts.analytic-input.path}}`}}'

          - name:   OUT_PATH
            value:  '{{`{{outputs.artifacts.cmda-es-pull-data.path}}`}}'
    #} pull-log-file
  #} spec.templates
#} spec
