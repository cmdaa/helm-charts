##############################################################################
# Kafka url
#
#   Usage:
#     url: \{\{- include "cmdaa.analytics.kafka.url" . \}\}
#
{{- define "cmdaa.analytics.kafka.url" -}}
{{ printf "%s:%d" .Values.kafka.server (int .Values.kafka.port) }}
{{- end -}}
##############################################################################
# Ingress annotations
#
#   Usage:
#     annotations: \{\{- include "cmdaa.analytics.ingress.annotations" . | nindent 4 \}\}
#
{{- define "cmdaa.analytics.ingress.annotations" -}}
{{-  range $key, $value := .Values.ingress.annotations -}}
{{    $key }}: {{ $value | quote }}
{{   end -}}
{{- end -}}
##############################################################################
# Ingress TLS-Auth Annotations
#
#   Usage:
#     tls_auth_annotations: \{\{- include "cmdaa.analytics.ingress.tls_auth_annotations" . | nindent 4 \}\}
#
{{- define "cmdaa.analytics.ingress.tls_auth_annotations" -}}
{{-  range $key, $value := .Values.ingress.tls_auth_annotations -}}
{{    $key }}: {{ $value | quote }}
{{   end -}}
{{- end -}}
##############################################################################
# Workflow spec settings
#
#   This template applies spec-related settings to an Argo Workflow template
#   and uses an `argo.spec` value of the form:
#     tolerations:
#       toleration:
#         - key:    node-role.kubernetes.io/master
#           effect: NoSchedule
#     nodeSelector:
#       cmdaa.collect/kernelbeat: "true"
#
#   Usage:
#     spec: \{\{- include "cmdaa.analytics.argo.specAdditions" . | nindent 2 \}\}
#
{{- define "cmdaa.analytics.argo.specAdditions" -}}
{{-  if .Values.argo.spec -}}
{{     toYaml .Values.argo.spec }}
{{-  end }}
{{- end -}}
##############################################################################
# Return the apiVersion for RBAC
#
#   Usage:
#     apiVersion: \{\{ include "cmdaa.analytics.rbac.apiVersion" .\}\}
#
{{- define "cmdaa.analytics.rbac.apiVersion" -}}
{{-  if .Capabilities.APIVersions.Has "rbac.authorization.k8s.io/v1" -}}
{{-   print "rbac.authorization.k8s.io/v1" -}}
{{-  else -}}
{{-   print "rbac.authorization.k8s.io/v1beta1" -}}
{{-  end -}}
{{- end -}}
##############################################################################
# Flink properties
#
#   Usage:
#     annotations: \{\{- include "cmdaa.analytics.flink.properties" . | nindent 4 \}\}
#
{{- define "cmdaa.analytics.flink.properties" -}}
{{-  range $key, $value := .Values.flink.job.properties -}}
{{    $key }}: {{ $value  }}
{{   end -}}
{{- end -}}
e
{{/* vim: set filetype=mustache: */}}
