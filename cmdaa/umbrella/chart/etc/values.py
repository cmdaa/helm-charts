#!/usr/bin/env python3
#
# Generate a version of `values.yaml` that has expanded all yaml anchors and
# aliases, optionally filtered for a single sub-chart/top-level key.
#
import sys
import argparse
import yaml

from values_helper import read_values

parser = argparse.ArgumentParser()
parser.add_argument( '-c', '--chart', type=str,
                     help='The sub-chart for which values should be filtered' )
parser.add_argument( '-g', '--globals', action='store_true',
                     help='When filtering by sub-chart, also include any'
                         + ' `global` values' )

args = parser.parse_args()

values = read_values()

if args.chart is not None: # {
  # Filter values to just the named sub-chart
  filtered = values.get( args.chart, None )

  if args.globals: # {
    # Include globals in the filtered values
    globs = values.get( 'global', None )
    if globs is not None: # {
      # Mix in the globals with any filtered values
      globs = { "global": globs }

      if filtered is None:  filtered = globs
      else: # {
        filtered.update( globs )
      # }
    # }
  # }

  if filtered is None: # {
    print("*** There is no section for chart '%s'" % (args.chart))
    sys.exit( -1 )
  # }

  values = filtered
# }

print( yaml.dump( values ) )

#############################################################################
