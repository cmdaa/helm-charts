#!/bin/bash
#
# Remove the node label that allows the timescaledb deployment to run from the
# given set of nodes.
#
#   labels-timescaledb-del [%node1% [ %node2% ... ]]
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")
 REL="$( $DIR/helm-release )"

if [ $# -gt 0 ]; then
  nodes=$@

else
  nodes=( $($DIR/kubectl get nodes --no-headers | awk '{print $1}') )

fi

# Retrieve the label that controls the target daemonset
label="$($DIR/value-get.py infrastructure.timescaledb.nodeSelector | \
          sed "s/{'//;s/':.*//")"
if [ -z "$label" ]; then
  echo "*** No timescaledb.nodeSelector found in values.yaml"
  exit
fi

for node in ${nodes[@]}; do
  echo ">>> Remove '$label' from $node ..."
  $DIR/kubectl label nodes "$node" "${label}-"
done
