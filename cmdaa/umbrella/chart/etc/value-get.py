#!/usr/bin/env python3
#
# Retrieve the value of one or more dot-separated keys from the expanded
# version of `values.yaml`.
#
import argparse

from values_helper import read_values
from values_helper import get_dotted_path

def main(): # {
  parser = argparse.ArgumentParser()
  parser.add_argument( '-v', '--verbose', action='store_true',
                          help='Be verbose' )
  parser.add_argument( '-f', '--first', action='store_true',
                          help='Output only the first non-empty match' )
  parser.add_argument( 'key', nargs='+',
                          help='The target key(s)' )
  args = parser.parse_args()

  values = read_values()

  maxKey = 0
  if args.verbose: # {
    # Determine the maximum key length
    for key in args.key: # {
      keyLen = len( key )
      if keyLen > maxKey: maxKey = keyLen
    # }
  # }

  #print('maxKey[ %d ]' % (maxKey))

  for key in args.key: # {
    val = get_dotted_path( values, key )  #values.get( key, None )
    if val is None: # {
      if args.first:        continue
      if not args.verbose:  val = ''
    # }

    if args.verbose: # {
      print( '%-*s : ' % (maxKey, key), end='' )
    # }

    print( val )

  # }
# }

###########################################################################
if __name__ == '__main__': #{
  main()
#}
