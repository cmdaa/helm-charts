#!/bin/bash
#
# Connect to the primary pod for timescale and execute a query against one of
# the kernel-beat metrics that *SHOULD* be constantly collected.
#
# The query will show the latest collection time, lag/latency, number of
# records, and average cpu sys time for each node.
#
#   timescale-kernelbeat-stats [-m]
#
# Similar PromQL queries:
#   The number of 'os_cpu_sys' metrics over the last 60 minutes
#   (with a beat of 30s, the values should be ~120):
#     count_over_time( os_cpu_sys{node=~"^.*$"}[60m] )
#
#   Lag of 'os_cpu_sys' metrics
#   (with a beat of 30s, the values should be <= ~30):
#     time() - timestamp( os_cpu_sys{node=~"^.*$"} )
#
#   Latest timestamp (s) of 'os_cpu_sys' metrics:
#     timestamp( os_cpu_sys{node=~"^.*$"} ) * 1000
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")
 REL="$( $DIR/helm-release )"

CLUSTER=$( $DIR/value-get.py timescaledb.clusterName )
if [[ ! -z "$CLUSTER" ]]; then
  LABELS="cluster-name=$CLUSTER"
else
  LABELS="app=${REL}-timescale"
fi

# Auto or manual selection?
AUTO_SELECT=1
if [[ "$1" == "-m" ]]; then
  shift
  AUTO_SELECT=0
else
  # Use the master
  LABELS="$LABELS,role=master"
fi


 PODS=( $($DIR/kubectl get pods --no-headers -l "${LABELS}" |
        awk '{print $1}') )
NPODS=${#PODS[@]}

if [[ $NPODS < 1 ]]; then
  echo "*** No POD found for app '$APP'"
  exit
fi

if [[ $AUTO_SELECT == 0 && $NPODS > 1 ]]; then
  echo ">>> There are $NPODS matching Pods:"

  for (( num=1; num <= $NPODS; num++ )); do
    idex=$(( $num - 1 ))
    printf "%2d: %s\n" "$num" "${PODS[$idex]}"
  done

  echo -n ">>> Which would you like [1..$NPODS]? "
  read num
  idex=$(( $num - 1 ))

  POD=${PODS[$idex]}
else
  POD=${PODS[0]}
fi

read -rd '' SQL <<'EOF'
SELECT DISTINCT( val(node_id) ) AS node,
       MAX(time)  OVER (PARTITION BY node_id ORDER BY node_id)  AS last_time,
       (NOW() -
        MAX(time) OVER (PARTITION BY node_id ORDER BY node_id)) AS lag,
       COUNT(*)   OVER (PARTITION BY node_id ORDER BY node_id)  AS num_recs,
       AVG(value) OVER (PARTITION BY node_id ORDER BY node_id)  AS avg_cpu_sys
  FROM os_cpu_sys
  WHERE time > now() - INTERVAL '60 minutes'
  ORDER BY node ASC;
EOF

# :XXX: psql returns data with '\r\n' separated lines
RES="$( $DIR/kubectl exec -it "$POD" -- \
  psql --dbname=collections --command="$SQL" --csv 2>/dev/null )"
rc=$?
if [ $rc -ne 0 ]; then
  echo "*** Cannot fetch kernelbeat stats"
  exit $rc
fi

echo "$RES" | \
    sed 's///g' | \
    awk -F, '{
      if (NR == 1) {
        # Header
        printf("%-42s | %-28s | %-16s | %8s | %14s\n",
               $1, $2, $3, $4, $5);
      } else {
        # Node record
        printf("%-42s | %-28s | %-16s | %8s | %14d\n",
               $1, $2, $3, $4, $5);
      }
    }'
