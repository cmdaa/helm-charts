#!/bin/bash
#
# Uninstall one or more sub-charts
#
#   sub-charts-uninstall [--verbose] [sub-chart ...]
#
if [[ "$1" == '-v' || "$1" == '--verbose' ]]; then
   VERBOSE=1; shift
else
   VERBOSE=0
fi

function log() {
  [[ $VERBOSE -ne 1 ]] && return

  FMT="$1"; shift
  echo "$FMT" $@
}

###
 ETC=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$ETC")

if [ ! -d "$ROOT/sub-charts" ]; then
  echo "*** Missing sub-charts"
  echo "*** Try running etc/sub-charts-generate"
  exit -1
fi

cd "$ROOT/sub-charts"

# Determine which sub-charts to uninstall
CHK_CHARTS=( $@ )
if [ ${#CHK_CHARTS[@]} -lt 1 ]; then
  CHK_CHARTS=( $(ls) )

  log ">>> Uninstall all subcharts: ${CHK_CHARTS[@]}"
fi

# Resolve sub-chart names
if [ ! -d "$ROOT/tmp" ]; then
  mkdir -p "$ROOT/tmp"
fi

SUB_CHARTS_TEMP=$(mktemp "$ROOT/tmp/sub-charts.XXXXXXXXXX")
for CHART in ${CHK_CHARTS[@]}; do
  CHART="$(basename "$CHART")"

  if [ ! -d "$CHART" ]; then
    # See if a non-ordered sub-chart suffix was provided
    # (e.g. 'umbrella' vs '00-umbrella')
    DIRS=( $(ls -d [0-9]*-"$CHART") )

    if [ ${#DIRS[@]} -eq 1 ]; then
      log ">>> Inferred sub-chart ${DIRS[0]} from $CHART ..."

      CHART="${DIRS[0]}"
    else
      echo "*** Missing sub-chart: $CHART"
      exit -1
    fi
  fi

  echo "$CHART" >> "$SUB_CHARTS_TEMP"
done

# Sort the target set of sub-charts in reverse order of installation
SUB_CHARTS=( $(sort -ru "$SUB_CHARTS_TEMP") )
rm -f "$SUB_CHARTS_TEMP"

log ">>> Uninstall subcharts: ${SUB_CHARTS[@]}"

# Uninstall the set of identified sub-charts
for CHART in ${SUB_CHARTS[@]}; do
  CHART="$(basename "$CHART")"
   NAME="$( echo "$CHART" | sed -E 's/^[0-9-]+//') "

  log ">>> $ETC/helm uninstall $NAME"
  $ETC/helm uninstall $NAME
  RC=$?

  if [ $RC -ne 0 ]; then
    echo "*** Helm uninstall of $CHART failed: $RC"
    exit $RC
  fi
done
