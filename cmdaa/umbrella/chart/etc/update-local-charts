#!/bin/bash
#
# Locally update all locally housed charts referenced in Chart.yaml
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")

##
# Attempt to retreive an exising chart bundle.
# @method importTarball
# @param  SRC   The source tarball path {String};
# @param  DST   The destination tarball path {String};
#
# @return An indication of success/failure:
#         0   = success
#         255 = bundle not found in local repo
#         !0  = error
#
function importTarball() {
  local SRC=$1
  local DST=$2
  if [ ! -f "$SRC" ]; then
    echo "*** $BUNDLE not found in local repo"
    return 255
  fi

  if [ -f "$DST" ]; then
    MD5_SRC="$(md5sum "$SRC" | awk '{print $1}')"
    MD5_DST="$(md5sum "$DST" | awk '{print $1}')"

    if [ $MD5_SRC == $MD5_DST ]; then
      echo "=== Skip existing $BUNDLE"
      return 0
    fi

    echo ">>> Replace $BUNDLE (differing md5sum)"
  else
    echo ">>> Import $BUNDLE"
  fi

  cp "$SRC" "$DST"
  return $?
}

##
# Attempt to package a source-based chart
# @method packageFromSource
# @param  SRC   The source tarball path {String};
# @param  DST   The destination tarball path {String};
#
# @return An indication of success/failure:
#         0   = success
#         1   = cannot identify source repo
#         2   = source repo does not exist
#         !0  = error
#
function packageFromSource() {
  local SRC=$1
  local DST=$2

  local NAME="$(basename "$SRC" | sed -E 's/-[0-9.-]+.tgz//')"
  local REPO="$(awk -v NAME=$NAME '
    /^\s*#/{next}
    /- name:/   {
      name = $3;
      if (name == NAME) {
        step = 1;
      }
      next;
    }
    / repository:/{
      if (step != 1) { next }
      step = 0;

      repository = $2;
      sub("file://", "", repository);
      printf("%s\n", repository);
    }
  ' Chart.yaml)"

  if [ -z "$REPO" ]; then
    echo "*** Cannot identify source repository for $NAME"
    return 1
  fi

  if [ ! -d "$REPO" ]; then
    echo "*** No source [ $REPO ] for $NAME"
    return 2
  fi

  echo "=== Package $(basename "$DST") from $REPO"
  helm package -d charts "$REPO" > /dev/null
}

# Generate the set of chart bundles
BUNDLES=(
  $(awk '
    /^\s*#/{next}
    /- name:/   {
      step = 1;
      name = $3;
      next;
    }
    /version:/{
      if (step != 1) { next }
      step = 0;

      version = $2;
      printf("%s-%s.tgz\n", name, version);
    }
  ' Chart.yaml)
)

if [ ! -d "$ROOT/charts" ]; then
  mkdir "$ROOT/charts"
fi

for BUNDLE in ${BUNDLES[@]}; do
  DST="$ROOT/charts/$BUNDLE"
  SRC=$(realpath "$ROOT/../../../repo/$BUNDLE")

  importTarball "$SRC" "$DST"
  rc=$?

  if [ $rc -eq 255 ]; then
    # Attempt
    packageFromSource "$SRC" "$DST"
    rc=$?
  fi

  if [ $rc -ne 0 ]; then
    echo "$rc: ******************************************************************"
  fi

done
exit

#############################################################################
# Package existing helm charts as chart bundles
#FILES=(
#  $(awk '/^\s*#/{next} /file:/{ gsub("file://","",$2); print $2 }' Chart.yaml)
#)
#
#for FILE in ${FILES[@]}; do
#  #echo "$FILE"
#  helm package -d charts "$ROOT/$FILE"
#done
