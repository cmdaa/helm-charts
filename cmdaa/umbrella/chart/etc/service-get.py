#!/usr/bin/env python3
#
# nc s3.test 80 <<EOF
# GET /file/list HTTP/1.1
# User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)
# Host: s3.test
# Accept-Language: en-us
# Authorization: Bearer 6f88c436-f699-47d0-88fd-18ee125634eb
#
# EOF
#
import socket
import sys
import os
import argparse
from urllib.parse import urlparse
import subprocess

g_dir  = os.path.dirname(os.path.realpath(__file__))
g_root = os.path.dirname( g_dir )
g_args = None

def log( msg ): # {
  if g_args.verbose: # {
    print( msg, file=sys.stderr )
# }

def get_user( ): # {
  """
  Retrieve the name of the current user
  """
  user = os.environ.get('USER', None)

  log( '>>> Current user [ %s ]' % (user) )

  return user
# }

def get_token( user ): # {
  """
  Retrieve the analytic access token for the given user
  """
  token = '6f88c436-f699-47d0-88fd-18ee125634eb'

  cmd = [
    '%s/etc/analytics-user-token' % (g_root),
    user
  ]
  #print( cmd )

  tokens = subprocess.run( cmd, stdout=subprocess.PIPE ) \
            .stdout \
            .decode('utf-8') \
            .strip() \
            .split(' ')

  token = tokens.pop()

  log( '>>> Token for [ %s ]: %s' % (user, token) )

  return token
# }

def main(): # {
  global g_args

  parser = argparse.ArgumentParser()
  parser.add_argument( '-v', '--verbose', action='store_true',
                          help='Be verbose' )
  parser.add_argument( '-u', '--user',
                          help='Connect as the given user overriding'
                              +   ' any user specified in the url [ $USER ]' )
  parser.add_argument( 'url', #nargs='1',
                          help='The target url' )
  g_args = parser.parse_args()
  url    = urlparse( g_args.url )
  #print('url[ %s ]:' % (g_args.url))
  #print( dir( url ) )

  host  = url.hostname
  port  = url.port  #getattr( url, 'port', None )
  path  = url.path
  query = url.query
  user  = g_args.user

  if port is None: port = 80
  if user is None: user = url.username
  if user is None: user = get_user()
  if url.query:    path = '%s?%s' % (path, url.query)
  if url.fragment: path = '%s#%s' % (path, url.fragment)

  token = get_token( user )
  if token is None or len(token) < 1: # {
    print('*** Cannot locate token for user [ %s ]' % (user),
            file=sys.stderr)
    sys.exit(-1)
  # }

  # Create a TCP/IP socket
  sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

  # Connect the socket to the port where the server is listening
  server_address = ( host, port )
  log( '>>> Connecting to %s port %s' % server_address )
  sock.connect(server_address)

  try: # {
    log( '>>> Fetch %s ...' % (path))
    # Send data
    message = [
      'GET ', path, ' HTTP/1.1\n',
      'User-Agent: Mozilla/4.0 (compatible; MSIE5.01; Windows NT)\n',
      'Host: ', host, '\n',
      'Accept-Language: en-us\n',
      'Authorization: Bearer ', token, '\n',
      '\n'
    ]

    log( '>>> Sending request ...' )
    sock.sendall( str.encode( ''.join( message ) ) )

    # Look for the response
    amount_received = 0
    amount_expected = len(message) * 100
    content_started = False

    while amount_received < amount_expected: # {
      #data = sock.recv(16)
      data = sock.recv( 512 )
      lines = data.decode().split('\r\n')
      log( '>>> Received %d bytes, %d lines' % (len(data), len(lines)) )

      for idx,line in enumerate(lines): # {
        fields = line.split(': ')
        if len(fields) > 1: # {
          key = fields[0]
          val = fields[1]

          #print( '%3d: key[ %s ]: %s' % (idx, key, val), file=sys.stderr )
          log( '%3d: %s' % (idx, line) )

          if key == 'Content-Length' or key == 'content-length': # {
            amount_expected = int(val)

            log( '>>>  %d bytes expected' % (amount_expected) )
          # }
        else: # }{
          log( '%3d: %s' % (idx, line) )

          if content_started: # {
            if not g_args.verbose: # {
              print( '%s' % (line) )
            # }
            amount_received += len(line)
          # }

          if len(line) < 1: # {
            content_started = True
            log( '>>>  Start content ...' )
          # }
        # }
      # }
    # }

  finally: # }{
    if g_args.verbose: # {
      log( '>>> Closing socket' )
    # }
    sock.close()
  # }
# }

###########################################################################
if __name__ == '__main__': #{
  main()
#}
