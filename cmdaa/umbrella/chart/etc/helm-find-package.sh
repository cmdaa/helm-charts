#!/bin/bash
#
# Search for a helm package and retrieve the full set of information about
# matches
#
curl -s "https://hub.helm.sh/api/chartsvc/v1/charts/search?q=$@" | jq .
