#!/usr/bin/env python3
#
# Identify all anchors/aliases in 'values.yaml'
#
import yaml

class Parser: #{
  """
    A class to process a YAML file to extract anchor/alias
    information.
  """
  #def __init__( self ): #{
  #  self.anchors     = {}
  #  self.path_stack  = []
  #  self.state_stack = []
  #  self.state_cur   = None
  #  self.key         = None
  #  self.val         = None
  ##}

  def parse( self, src_path ): #{
    """
      Parse the given YAML file and return the set of anchors.

      Args:
        src_path {str}  The path to the YAML file

      Returns:
        anchors {dict}  The set of identified anchors when each entry is a
                        `dict` of the form:
                          { 'name'  : anchor-name,
                            'src'   : YAML-path-to-definition,
                            'value' : defined-value,
                            'refs'  : [
                              YAML-path-to-reference,
                              ...
                            ]
                          }
    """
    self.src         = src_path
    self.anchors     = {}
    self.path_stack  = []
    self.state_stack = []
    self.state_cur   = None
    self.key         = None
    self.val         = None

    with open( src_path, 'r') as stream: #{
    
      for event in yaml.parse( stream ): #{
        # MapStart
        #   key : Scalar
        #   val : Scalar | Alias | SeqStart ... SeqEnd | MapStart ... MapEnd
        #   ...
        # MapEnd
        #
        # SeqStart
        #   val : Scalar | Alias | SeqStart ... SeqEnd | MapStart ... MapEnd
        #   ...
        # SeqEnd
        #
        # Any Scalar MAY define a new anchor
        #
        #print( event )
        #continue
    
        if isinstance( event, yaml.MappingStartEvent ): #{
          self._handle_map_start( event )
    
        elif isinstance( event, yaml.MappingEndEvent ): #}{
          # MapEnd
          #print('>>> Map end:', event)
          if self.state_cur != 'map': #{
            print('*** Inconsistent state [ %s != map ]' % (self.state_cur))
          #}
          self.state_cur = stack_pop( self.state_stack )
    
          stack_pop( self.path_stack )
          self.key = self.val = None
    
          #print('>>> Map end:%d: => %s [ path:%s ]' %
          #          (len(self.state_stack), self.state_cur, stack_str(self.path_stack)))
    
        elif isinstance( event, yaml.SequenceStartEvent): #}{
          # SeqStart
          #print('>>> Seq start:%d: %s [ path:%s ] => seq' %
          #          (len(self.state_stack), self.state_cur, stack_str(self.path_stack)))
          if self.state_cur == 'seq': self._seq_next()
    
          stack_push( self.state_stack, self.state_cur )
          self.state_cur = 'seq'
    
          stack_push( self.path_stack, -1 )
    
        elif isinstance( event, yaml.SequenceEndEvent): #}{
          # SeqEnd
          if self.state_cur != 'seq': #{
            print('*** Inconsistent state [ %s != seq ]' % (self.state_cur))
          #}
          self.state_cur = stack_pop( self.state_stack )
          stack_pop( self.path_stack )
    
          #print('>>> Seq end:%d: => %s [ path:%s ]' %
          #          (len(self.state_stack), self.state_cur, stack_str(self.path_stack)))
    
        elif isinstance( event, yaml.ScalarEvent): #}{
          # Scalar
          #print('>>> Scalar:', event)
          if self.state_cur == 'map': #{
            # Scalar : may be a key or value
            self._handle_map_key_val( event.value )
    
          elif self.state_cur == 'seq': #}{
            self._seq_next()
          #}
    
          if event.anchor is not None: #{
            # New anchor : MUST be a value
            self.anchors[ event.anchor ] = {
              'name'  : event.anchor,
              'src'   : stack_str(self.path_stack),
              'value' : event.value,
              'refs'  : [],
            }
    
            #print('&%-30s: %-30s : %s' %
            #        (event.anchor, stack_str(self.path_stack), event.value))
          #}
    
        elif isinstance( event, yaml.AliasEvent): #}{
          # Alias
          anchor = self.anchors.get( event.anchor, None )
          if anchor is None: #{
            print('*** Unknown anchor [ %s ]' % (event.anchor))
          #}
          anchorVal = anchor['value']
    
          if self.state_cur == 'map': #{
            # Alias : may be a key or value
            self._handle_map_key_val( anchorVal, anchor = anchor )
    
          elif self.state_cur == 'seq': #}{
            self._seq_next()
          #}
    
          anchor['refs'].append( stack_str( self.path_stack ) )
    
          #print('>>> Alias: %s = %s [ %s ]' %
          #      ( stack_str( self.path_stack ), anchorVal, event.anchor ))
        #}
      #}
    
      #for token in yaml.scan( stream ): #{
      #  print(token)
      ##}
    
    #}

    return self.anchors
  #}

  #########################################################################
  # Private methods {
  #

  ##
  # Handle a new map key/value
  # @method _handle_map_key_val
  # @param  newVal {str}    The value that should replace key/val;
  # @param  [anchor {dict}] If provided, this key/val is an alias to the given
  #                         anchor;
  #
  # @return self;
  #
  def _handle_map_key_val( self, newVal, anchor = None ): #{
    if self.key is None: #{
      # Replace the current key in the path_stack
      self.key = newVal
  
      stack_pop(  self.path_stack )
  
      if anchor: #{
        #pushKey = '%'+ anchor['name'] +'%='+ self.key
        pushKey = '%'+ anchor['name'] +'%'
  
      else: #}{
        pushKey = self.key
      #}
  
      stack_push( self.path_stack, pushKey )
  
    else: #}{
      self.val = newVal
      self.key = None
    #}
  
    #if anchor: #{
    #  print('*%-30s: %-30s : %s' %
    #            (anchor['name'], stack_str(self.path_stack), newVal))
    ##}
  
    return self
  #}
  
  ##
  # Within a sequence, update the sequence number.
  # @method _next_seq
  #
  # @return The previous index {int};
  #
  def _seq_next( self ): #{
    idex = stack_pop( self.path_stack )
    stack_push( self.path_stack, idex + 1 )
  
    return idex
  #}
  
  ##
  # Handle a map start event
  # @method _handle_map_start
  # @param  event {Event} The triggering event;
  #
  # @return None
  #
  def _handle_map_start( self, event ): #{
    #print('>>> Map start:%d: %s [ path:%s ] => map' %
    #          (len(self.state_stack), self.state_cur, stack_str(self.path_stack)))
    if self.state_cur == 'seq': self._seq_next()
  
    stack_push( self.state_stack, self.state_cur )
    self.state_cur = 'map'
  
    self.key = self.val = None
    stack_push( self.path_stack, '<no-key>' )
  #}

  #
  # Private methods }
  #########################################################################
#}

###########################################################################
# Private helpers {
#

##
# Given a stack, generate a string representation
# @method stack_str
# @param  stack {list}  The target stack;
#
# @return A string representation {str};
#
def stack_str( stack ): #{
  if len(stack) < 1: return 'None'

  return '.'.join( [str(elem) for elem in stack] )
#}

##
# Push a new value onto the given stack.
# @method stack_push
# @param  stack {list}  The target stack;
# @param  val {mixed}   The value to push;
#
# @return None
#
def stack_push( stack, val ): #{
  stack.append( val )
#}

##
# Pop a value from the given stack.
# @method stack_pop
# @param  stack {list}  The target stack;
#
# @return The popped value {mixed};
#
def stack_pop( stack ): #{
  if len(stack) > 0: return stack.pop()
  return None
#}

# Private helpers }
###########################################################################
# Main {
#
if __name__ == '__main__': #{
  import sys
  import os

  dir  = os.path.dirname(os.path.realpath(__file__))
  root = os.path.dirname( dir )

  values_path = os.path.join( root, 'values.yaml' )

  # Ensure values.yaml exists
  if not os.path.isfile( values_path ): # {
    print("*** There is no 'values.yaml'")
    sys.exit( -1 )
   # }

  parser  = Parser()
  anchors = parser.parse( values_path )

  for (name,anchor) in anchors.items(): #{
    #print('src[ %s ], anchor:' % (src), anchor)
    name = anchor['name']
    src  = anchor['src']
    val  = anchor['value']
    refs = anchor['refs']

    print('%s:' % (name))
    print('  Defined   : %s [ %s ]' % (src, val))
    print('  Referenced: ', end='')
    for (idex,ref) in enumerate(refs): #{
      if idex != 0: print('            : ', end='')

      print( ref )
    #}

    if len(refs) == 0: #{
      print()
    #}
    print()
  #}
#}

#
# Main }
###########################################################################
