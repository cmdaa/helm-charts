import os
import sys
import subprocess
import yaml
import base64
import random
import string

dir  = os.path.dirname(os.path.realpath(__file__))
root = os.path.dirname( dir )

import collections

def merge_dict(dst, src): #{
  """
  Recursive dict merge. Inspired by :meth:``dict.update()``, instead of
  updating only top-level keys, merge_dict recurses down into dicts nested to
  an arbitrary depth, updating keys. The ``src`` is merged into ``dst``.

  :param dst: dict onto which the merge is executed
  :param src: dict merged into `dst`

  :return: None

  With minor renaminng and python3 changes:
    src.iteritems() => src.items()

  ###
  Copyright 2016-2022 Paul Durivage

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

     http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
  """
  for k, v in src.items(): #{
    if (k in dst and
        isinstance(dst[k], dict) and
        isinstance(src[k], dict)): #{ noqa

      merge_dict(dst[k], src[k])

    else: #}{
      dst[k] = src[k]
    #}
  #}
#}

def read_values(): # {
  """
  Read `%root%/values.yaml` and merge in any values mixins defined in
  `%root%/config.yaml:mixin`.
  """
  # Open the config file to see if there are any value mixins
  config_path = os.path.join( root, 'config.yaml' )
  with open( config_path, 'r') as stream: # {
    config = yaml.safe_load( stream )
  # }
  mixins = config.get( 'mixin', None )

  # Ensure values.yaml exists
  values_path = os.path.join( root, 'values.yaml' )
  if not os.path.isfile( values_path ): # {
    cmd     = [ os.path.join( dir, 'generate-values' ) ]
    process = subprocess.Popen( cmd, stdout=subprocess.PIPE )
    output, error = process.communicate()
   # }

  with open( values_path, 'r') as stream: # {
    values = yaml.safe_load( stream )
  # }

  if isinstance(mixins, list): # {
    # Read and merge mixins into `values`
    for mixin in mixins: # {
      mixin_path = os.path.join( root, mixin )

      with open( mixin_path, 'r' ) as stream: # {
        mixin_values = yaml.safe_load( stream )
      # }

      merge_dict( values, mixin_values )

      #for key in mixin_values: # {
      #  values[key].update( mixin_values[key] )
      ## }
    # }
  # }

  return values
# }

def get_dotted_path( dct, path ): # {
  """
  Given a target dictionary and dot-separated path, retrieve the nested value
  referenced by the dotted path.
  """
  keys = path.split('.')

  for key in keys: # {
    dct = dct.get( key, None )

    if dct is None: break

    #try:
    #  dct = dct[key]
    #except KeyError:
    #  return None
  # }

  return dct
# }

def base64_encode( val ): #{
  """
  Base64 encode the incoming value
  """
  return str( base64.b64encode( val.encode('ascii') ), 'ascii' )
#}

def gen_random_str( size = 12, chars = string.ascii_uppercase + string.digits): #{
  """
  Generate a random string of the given size.
  """
  return ''.join( random.choice( chars ) for _ in range( size ) )
#}

def get_secret( secret_name ): #{
  """
  Attempt to fetch the value of the namee secret from the current k8s cluster
  """
  cmd     = [
    os.path.join( dir, 'kubectl' ),
    'get',
    ('secret/%s' % (secret_name)),
    '-o',
    'yaml'
  ]
  process = subprocess.Popen( cmd, stdout=subprocess.PIPE,
                                   stderr=subprocess.DEVNULL )
  output, error = process.communicate()

  if error or len(output) < 1: #{
    print('=== No values found for secret %s' % (secret_name), file=sys.stderr)
    return {}
  #}

  #values = yaml.safe_load( process.stdout )
  values = yaml.safe_load( output )

  return values
#}
