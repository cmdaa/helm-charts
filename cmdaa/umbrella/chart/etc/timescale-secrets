#!/bin/bash
#
# Retrieve the timescaledb user/password combinations from the currently
# active/live secret.
#
#   timescale-passwords-live
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")
 REL="$( $DIR/helm-release )"

# Locate the database credentials secret name
KEY='infrastructure.tsMeta.secrets.creds'
SEC=$( $DIR/value-get.py "$KEY" )
if [[ -z "$SEC" ]]; then
  cat <<EOF
***
*** Cannot identify the database credentials secret name.
*** This SHOULD be set in '$KEY'
***
EOF
  exit -1
fi


echo "Timescaledb ($SEC):"
$DIR/kubectl get secret/${SEC} -o yaml 2>&1 | awk '
  /^[a-z]*:/{decode=($1 == "data:"); next;}
  {
    if (! decode) { next }

    KEY = $1;
    VAL = $2;

    cmd="echo "VAL" | base64 --decode";
    cmd | getline DECODED;
    close(cmd);

    printf("  %-15s %s\n", KEY, DECODED);
  }'
