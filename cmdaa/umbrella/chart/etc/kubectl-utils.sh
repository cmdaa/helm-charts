#!/bin/bash
#
# Utilities/helpers
#
if [ "$0" != '-bash' ]; then
   ETC=$(realpath "$(dirname "$0")")
  ROOT=$(dirname "$ETC")
else
  ROOT=$( pwd )
   ETC="$ROOT/etc"
fi

##
# Retrieve the length of generated secrets.
# @method get_secret_len
#
# @return The length of generated secrets;
#
function get_secret_len() {
  SEC_LEN=$( $ETC/value-get.py 'secrets.length' )
  [ -z "$SEC_LEN" ] && SEC_LEN=32

  echo $SEC_LEN
}

##
# Retrieve the key/value pairs from the named secret.
# path.
# @method get_secrets
# @param  secret_name   The name of the target secret;
#
# If the the target secret exists, this helper will generate two parallel arrays:
#     SECRET_KEYS
#     SECRET_VALS
#
function get_secrets() {
  SEC_NAME="$1"

  unset SECRET_KEYS
  unset SECRET_VALS

  YAML="$( $ETC/kubectl get secret/${SEC_NAME} -o yaml 2>/dev/null )"
  if [ ! -z "$YAML" ]; then
    SECRET_KEYS=( $(echo "$YAML" | awk '
      /^[a-z]*:/{inData=($1 == "data:"); next}
      {
        if (! inData) { next }

        SEC_KEY = $1;
        gsub(/:/,"", SEC_KEY);

        printf("%s\n", SEC_KEY);
      }') )

    SECRET_VALS=( $(echo "$YAML" | awk '
      /^[a-z]*:/{inData=($1 == "data:"); next}
      {
        if (! inData) { next }

        SEC_VAL = $2;

        printf("%s\n", SEC_VAL);
      }') )
  fi

  nSECRETS=${#SECRET_KEYS[@]}
  if [ $nSECRETS -gt 0 ]; then
    printf ">>> Retrieved %d values from secret/%s\n" "$nSECRETS" "$SEC_NAME"

    #for (( idex = 0; idex < $nSECRETS; idex++ )); do
    #  printf "%2d: %-10s : %s\n"  "$idex" "${SECRET_KEYS[$idex]}" "${SECRET_VALS[$idex]}"
    #done

    return 0

  elif [ -z "$YAML" ]; then
    printf "=== No secret/%s\n" "$SEC_NAME"

    return 1

  else
    printf "=== No values found for secret/%s\n" "$SEC_NAME"

    return 2
  fi
}

##
# Retrieve the key/value pairs from the secret identified via the template
# path.
# @method get_secrets_by_template_path
# @param  template_path   The helm template path containing the secret name;
#
# If the the target secret exists, this helper will generate two parallel arrays:
#     SECRET_KEYS
#     SECRET_VALS
#
# @return An indication of success (0 == success, !0 == failure);
#
function get_secrets_by_template_path() {
  NAME_KEY="$1"
  SEC_NAME=$( $ETC/value-get.py "$NAME_KEY" )

  if [ -z "$SEC_NAME" ]; then
    cat <<EOF
***
*** Missing secret name [ $NAME_KEY ]
***
EOF
    return -1
  fi

  get_secrets "$SEC_NAME"
  return $?
}

##
# Given SECRET_KEYS and SECRET_VALS generated via get_secrets(), retrieve the
# value of a single secret by key.
# @method get_secret_val
# @param  key     The target key;
#
# @return The value of the target key;
#
function get_secret_val() {
  TARGET_KEY="$1"
  FOUND=0

  for (( idex = 0; idex < ${#SECRET_KEYS[@]}; idex++ )); do
    KEY="${SECRET_KEYS[$idex]}"
    VAL="${SECRET_VALS[$idex]}"

    #echo "KEY[ $KEY ] VAL[ $VAL ]">> log

    if [ "$KEY" == "$TARGET_KEY" ]; then
      echo "$VAL"
      FOUND=1
      break;
    fi
  done

  [ $FOUND ] && return 0
  return 1
}

##
# Given a JSON object with simple key/value pairs, generate a matching set of
# parallel arrays with the keys and values.
# @method json_keys_vals
# @param  json      The JSON string;
#
# Generate two parallel arrays:
#     JSON_KEYS     An array of key names;
#     JSON_VALS     A parallel array of key values;
#
# @return An indication of success (0 == success, !0 == failure);
#
function json_keys_vals() {
  JSON="$@"

  JSON_KEYS=()
  JSON_VALS=()

  eval $( echo "$JSON" | \
            sed "s/[',:{}]//g" | awk '{
    for (idex = 1; idex <= NF; idex += 2) {
      JSON_KEY = $idex;
      JSON_VAL = $(idex+1);

      printf("JSON_KEYS=( ${JSON_KEYS[@]} \"%s\" )\n", JSON_KEY);
      printf("JSON_VALS=( ${JSON_VALS[@]} \"%s\" )\n", JSON_VAL);
    }
  }')

  return $?
}

##
# Retrieve the key/value pairs from the given template path.
# @method get_keys_vals_by_template_path
# @param  template_path   The helm template path containing the secret name;
#
# If the the target exists, this helper will generate two parallel arrays:
#     JSON_KEYS
#     JSON_VALS
#
# @return An indication of success (0 == success, !0 == failure);
#
function get_keys_vals_by_template_path() {
  TEMPLATE_PATH="$1"

  JSON="$( $ETC/value-get.py "$TEMPLATE_PATH" )"

  if [ -z "$JSON" ]; then
    cat <<EOF
***
*** Missing template path [ $TEMPLATE_PATH ]
***
EOF
    return -1
  fi

  json_keys_vals "$JSON"
  return $?
}

##
# Given JSON_KEYS and JSON_VALS generated via json_keys_vals(), retrieve the
# value of a single key.
# @method get_json_val
# @param  key     The target key;
#
# @return The value of the target key;
#
function get_json_val() {
  TARGET_KEY="$1"
  FOUND=0

  for (( idex = 0; idex < ${#JSON_KEYS[@]}; idex++ )); do
    KEY="${JSON_KEYS[$idex]}"
    VAL="${JSON_VALS[$idex]}"

    #echo "KEY[ $KEY ] VAL[ $VAL ]">> log

    if [ "$KEY" == "$TARGET_KEY" ]; then
      echo "$VAL"
      FOUND=1
      break;
    fi
  done

  [ $FOUND ] && return 0
  return 1
}

##
# Generate a random secret of the given length and base64 encode it.
# @method generate_secret
# @param  length      The character length of the new secret;
#
# @return The newly generated secret
#
function generate_secret() {
  SEC_LEN="$1"

  echo "$(< /dev/urandom LC_ALL=c tr -dc A-Za-z0-9 | \
              head -c$SEC_LEN | base64)"
}
