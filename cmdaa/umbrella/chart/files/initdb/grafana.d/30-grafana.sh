{{- if and .Values.grafana.enabled .Values.grafana.config.database.enabled }}
{{-  with .Values.grafana.config.database }}
#!/bin/bash
#
# Expects database secrets to be mounted at:
#   /var/run/secrets/db/creds
#
SECRETS="/var/run/secrets/db/creds"

export GF_DATABASE_USER="{{ .user }}"
export GF_DATABASE_PASSWORD="$(cat "$SECRETS/$GF_DATABASE_USER")"

psql <<EOF
\set ON_ERROR_STOP on

-- grafana.database {
{{-   if not ( eq .user "postgres" ) }}
-- Ensure the grafana database user exists.
DO \$$
  BEGIN
    CREATE ROLE {{ .user }} WITH LOGIN PASSWORD '$GF_DATABASE_PASSWORD';
  EXCEPTION WHEN duplicate_object THEN
    RAISE NOTICE 'role {{ .user }} already exists, skipping create';
  END
\$$;
{{-   end -}}
{{-   if not ( eq .dbName "postgres" ) }}
-- Ensure the grafana configuration database exists.
SELECT 'CREATE DATABASE {{ .dbName }} WITH OWNER="{{ .user }}"'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '{{ .dbName }}')\gexec
{{-   end }}

-- For the case where the database already existed, explicitly grant access to
-- the database user.
GRANT ALL ON DATABASE {{ .dbName }} TO {{ .user }};

-- grafana.database }
EOF

{{-  end }}
{{- end }}
