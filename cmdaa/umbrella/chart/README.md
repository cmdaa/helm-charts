Here you will find a helm chart that can install all CMDAA components in a
coordinated fashion, ensuring that the sub-charts are properly inter-connected
for a fully functional CMDAA>

By default, this chart will install all infrastructure components, the CMDAA
argo-based analysis components, and metric collection components.

This umbrella chart may also be used to install a functional version of
`grafana` with example dashboards, propertly connected to `timescaledb` via
`promscale`.


Infrastructure:
- nginx-ingress (if needed);
- s3/minio;
  (chart 8.0.10);
- mongodb;
- kafka<br />
  *NOTE:* kafka will only be run on nodes with the label `cmdaa/kafka=true`. To
  label nodes, you may use the `kubectl` helper (to ensure the proper kubectl
  context is used):
    ```
    # Retrieve the set of nodes
    ./etc/kubectl get nodes

    # For each node that should be used for kafka:
    ./etc/kubectl label nodes %node% 'cmdaa/kafka=true'
    ```
- TimescaleDb<br />
  *NOTE:* timescaledb will only be run on nodes with the label
  `cmdaa/timescaledb=true`. To label nodes, you may use the `kubectl` helper
  (to ensure the proper kubectl context is used):
    ```
    # Retrieve the set of nodes
    ./etc/kubectl get nodes

    # For each node that should be used for timescaledb:
    ./etc/kubectl label nodes %node% 'cmdaa/timescaledb=true'
    ```
- Promscale;
- argo-workflows;
- argo-events;
- ElasticSearch;


CMDAA argo-based analysis components
(requires minio, mongodb, tobs for Timescaledb/Postgres, and argo):
- [grokstore](//gitlab.com/cmdaa/cmdaa-grokstore) (dao for minio and mongodb);
- [workflow services](//gitlab.com/cmdaa/workflow) (dao for argo);
- [user interface](//gitlab.com/cmdaa/cmdaa-ui);


CMDAA custom metrics collection components
(requires kafka and tobs for Timescaledb and Promscale):
- [KernelBeat](https://gitlab.com/cmdaa/linux-kernel-module/-/blob/master/docs/KernelBeat.md);
- [sg-logs](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/sg-logs/README.md);
- [metrics bridge](//gitlab.com/cmdaa/custom-collectors/-/tree/master/prometheus-bridge)
  (in multiple configurations)
  - [kernelbeat](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kernelbeat)
    =>
    [kafka](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kafka-1)
    : collect KernelBeat metrics and publish them to kafka topics;
  - [kafka](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kafka)
    =>
    [prometheus/PromScale](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#prometheus-1)
    : listen on kafka topics expecting JSON-encoded metrics (from either the
    `kenelbeat => kafka` bridge or the `prometheus-kafka-adapter`). These are
    then written to the Prometheus remote-write endpoint (PromScale) for
    storage in Timescaledb;
- [prometheus](https://github.com/prometheus/prometheus) v2.21.0
  (chart 11.16.7) to collect kubernetes metrics;
- [prometheus-kafka-adapter](https://github.com/Telefonica/prometheus-kafka-adapter)
  v1.0 (chart 0.1.0) to send all Prometheus-collected metrics through kafka.<br>
  **NOTE:** in order for these metrics to be stored in Timescaledb we either
  need to configure Prometheus with two remote writers (this one and Promscale
  as a second) or enable the `kafka => prometheus/PromScale` metrics bridge;

---
**Table of Contents**

[[_TOC_]]


## Getting started
Primary use of this chart and related helpers is expected to be via the command
line. In support of that use-case, it includes a number of helper scripts as
well as a [Makefile](Makefile).

The Makefile serves as the primary entry point, providing insight into how the
other helpers and the full chart may be used. To get started, you can run
`make` to get a list of available rules along with simple descriptions of each:
```bash
$ make

Available rules:
  help                      Show this help message
  charts                    Import charts from the local repository
  identify-images           Identify all the docker images required for the
                            install
  pull-images               Pull all the docker images required for the
                            install and place them in an archive

  cmdaa-pause               Pause the CMDAA application
  cmdaa-resume              Resume the CMDAA application (if paused)
  cmdaa-state               Show the latest stored state of CMDAA

  base                      Generate the kustomized base
  kubectl-contexts          Show the available kubectl contexts
  kubectl-kustomize         Show the kustomized base template
  kubectl-apply             Apply `base` to a k8s cluster via kubectl
  kubectl-delete            Delete `base` from a k8s cluster via kubectl
  helm-dependency-update    Update dependencies via helm, pulling from remote
                            repositories
  helm-template             Perform templating via helm
  helm-dry-run              Perform a dry-run install test via helm
  helm-install              Install via helm
  helm-status               View all active helm charts in the target namespace
  helm-upgrade              Upgrade an installation via helm
  helm-uninstall            Uninstall via helm

  sub-charts                Generate sub helm charts that are individually
                            installable
  sub-charts-install        Install all sub helm charts
  sub-charts-uninstall      Uninstall all sub helm charts

  dist-clean                Remove dependencies for distribution
  clean-resources           Clean out any remaining resources
                             :WARNING: This will destroy persistent volumes/volume claims in addition to
                                       any remaining deployments, daemonsets, endpoints, jobs, services,
                                       pods, and argo-related resources

  show-context              Show the kubectl context
  show-ns                   Show the k8s namespace
  show-rel                  Show the helm release
  show-env                  Show the deployment environment
                            (context, ns, rel, dn)
  show-clean-resources      Show the active, cleanable resources
  show-resources            Show all active resources
  timescale-rollout         Watch the rollout status for the timescale
                            statefulset
  watch-events              Watch kubectl events
  watch-pods                Watch pod status

```

### Dependencies
In order to use this chart and helpers, you will need to have a functional
`kubectl` with defined cluster/context, `helm` (v3), `make`, and `python3` with
the `PyYAML` pip module.

<a name="config.yaml"></a>You will also need to setup
[config.yaml](config.yaml) with the proper values for your k8s cluster and
desired deployment.

<table>
 <thead>
  <tr>
   <th nowrap><a href="config.yaml">config.yaml</a> variable</th>
   <th>purpose</th>
  </tr>
 </thead>
 <tbody>
  <tr>
   <td nowrap><code>kubectl_context</code></td>
   <td>
    the kubectl context that should be used to access your cluster. The
    available contexts may be viewed via <code>make kubectl-contexts</code> or
    <code>kubectl config get-contexts</code>. If you are using rancher to
    manage your cluster, you can retrieve the definition of a context for the
    managed cluster via the <code>Kubeconfig File</code> button in the
    <code>Cluster</code> tab of the target cluster;
   </td>
  </tr>
  <tr>
   <td nowrap><code>k8s_namespace</code></td>
   <td>the kubernetes namespace you wish to use for your deployment;</td>
  </tr>
  <tr>
   <td nowrap><code>helm_release</code></td>
   <td>the name of the helm release you wish to use for your deployment;</td>
  </tr>
  <tr>
   <td nowrap><code>ingress_dn</code></td>
   <td>
    the DNS domain name that should be used for ingress entries;
   </td>
  </tr>
 </tbody>
</table>

### Values
The set of values used by this chart ([values.yaml.tpl](values.yaml.tpl)) is
extensive and makes use of [YAML anchors](#yaml-anchors), but most can be left
at the default settings. The values you will most likely want to change are:
- `global.`
  - `install.nginxIngress` [false] : set to true if you need nginx-ingress to
    be installed;
  - `install.debug_access` [false] : set to true if you would like a pod
    deployed to provide access to the collections context<br>
    (`{{ .Release.Name }}-access`);
  - `install.debug_bridge` [false] : set to true if you would like a pod
    deployed to provide access to the collections bridge context<br>
    (`{{ .Release.Name }}-bridge`);
  - `install.debug_kernelbeat` [false] : set to true if you would like a pod
    deployed to provide access to the collections kernel-beat initialization
    context<br>
    (`{{ .Release.Name }}-kb-init`);

- `db.`
  - `host` [cmdaa-timescale] : if you wish to change the name of the database
    (Timescaledb) service ([`db_host`](#anchor.db_host));
  - `port` [5432] : if you wish to change the port used to connect to the
    database (Timescaledb) service ([`db_port`](#anchor.db_port));
  - `users.app` [cmdaa] : if you wish to use a different application-level user
    name for the database ([`db_user_app`](#anchor.db_user_app));
  - `dbNames.analytics` [analytics] : if you wish to use a different name for
    the analytics database ([`db_name_analytics`](#anchor.db_name_analytics));
  - `dbNames.collections` [collections] : if you wish to use a different name
    for the metrics collection database
    ([`db_name_collections`](#anchor.db_name_collections));

- `minio.`
  - `fullnameOverride` [cmdaa-minio] : if you wish to change the name of the
    minio service ([`s3_fullName`](#anchor.s3_fullName));
  - `service.port` [9000] : if you wish to change the port used to connect to
    the minio service
    ([`s3_port`](#anchor.s3_port));
  - `buckets[0].name` [argo-artifacts] : if you wish to use a different name
    for the bucket used for analytic argo artifacts
    ([`s3_bucket_workflow`](#anchor.s3_bucket_workflow));
  - `buckets[1].name` [cmdaa] : if you wish to use a different name for the
    bucket used for analytic cmdaa artifacts (e.g. log file uploads &mdash;
    [`s3_bucket_service`](#anchor.s3_bucket_service));
  - `buckets[2].name` [cmdaa-db-backups] : if you wish to use a different name
    for the bucket used for database backups
    (not currently enabled nor tested &mdash;
    [`s3_bucket_db`](#anchor.s3_bucket_db));
  - `persistence.`
    - `size` [500Gi] : if you wish to change the size of the minio volume;
    - `storageClass` [] : if you wish to change the storage class used for the
      minio volume;

- `kafka.`
  - `fullnameOverride` [cmdaa-kafka] : if you wish to change the name of the
    kafka service ([`kafka_host`](#anchor.kafka_host));
  - `service.port` [9092] : if you wish to change the port used to connect to
    the kafka service ([`kafka_port`](#anchor.kafka_port));
  - `persistence.`
    - `size` [8Gi] : if you wish to change the size of the kafka volumes;
    - `storageClass` [-] : if you wish to change the storage class used for the
      kafka volumes (`global.storageClass` will over-ride this setting);

- `argo.`
  - `server.name` [argo-ui]: if you wish to *effect* the name of the Argo
    service (the release will be prepended to this name &mdash;
     [`argo_name`](#anchor.argo_name));
  - `server.servicePort` [2746]: if you wish to change the port used to connect
    to the Argo service ([`argo_port`](#anchor.argo_port));

- `tobs.`
  - `timescaledb-single.`
    - `replicaCount` : if you wish a multi-node database
      cluster;
    - `credentials` : if you wish to change the default/initial passwords for
      the various database users
      ([`db_creds`](#anchor.db_creds));
    - `persistenVolumes.`
      - `data.`
        - `size` [60Gi] : if you wish to change the default size of the primary
          data volumes;
        - `storageClass` [-] : if you wish to change the storage class used for
          the primary data volumes;
      - `wal.`
        - `size` [20Gi] : if you wish to change the default size of the
          write-ahead-log (wal) volumes (you may also need to change
          `tobs.timescaledb-single.patroni.postgresql.parameters.max_wal_size`
          to be ~80% of the selected `wal.size`);
        - `storageClass` : if you wish to set the storage class to be used for
          the wal volumes;

  - `promscale.`
    - `fullnameOverride` [cmdaa-promscale] : if you wish to change
      the name of the Promscale service
      ([`promscale_fullName`](#anchor.promscale_fullName));
    - `service.port` [9201] : if you wish to change the port used to
      connect to the Promscale service
      ([`promscale_port`](#anchor.promscale_port));

  - `grafana.`
    - `fullnameOverride` [cmdaa-grafana] : if you wish to change the
      name of the Grafana service
      ([`grafana_fullName`](#anchor.grafana_fullName));
    - `service.port` [3000] : if you wish to change the port used to
      connect to the Grafana service
      ([`grafana_port`](#anchor.grafana_port));

- `prometheus-kafka-adapter.`
  - `fullnameOverride` [cmdaa-kafka-adapter] : if you wish to change the name
    of the adapter service
    ([`kafka_adapter_fullName`](#anchor.kafka_adapter_fullName));
  - `service.port` [80] : if you wish to change the port used to connect to the
    adapter service
    ([`kafka_adapter_port`](#anchor.kafka_adapter_port));
  - `environment.KAFKA_BATCH_NUM_MESSAGES` : if you wish to change the number
    of messages in a single batch;
  - `environment.KAFKA_TOPIC` : if you wish to change the kafka topic used to
    publish Prometheus-generated metrics;

- `prometheus.`
  - `remoteWrite[0].queue.max_shards` : if you wish to change the max shards
    used by Prometheus when communicating with the `prometheus-kafka-adapter`
    remote write endpoint;
  - `remoteWrite[1].queue.max_shards` : if you wish to change the max shards
    used by Prometheus when communicating with the `promscale` remote write
    endpoint;

- `analytics.`
  - `mongo.` : these settings are propagated down from the `mongodb` section
    via [mongo](#mongodb) YAML anchors;
  - `minio.` : these settings, used to specify the S3 service to be used by
    analytics, are propagated down from the `minio` section via
    [s3/Minio](#s3minio) YAML anchors;
  - `kafka.` : these settings, used to specify kafka connection information,
    are propagated down from the `kafka` section via [kafka](#kafka) YAML
    anchors;
  - `postgres.` : these settings, used to specify Postgres/Timescaledb
    connection information, are propagated down from the `tobs` section via
    [db](#timescaledb-tobs) YAML anchors;

- `collections.`
  - `timescale.` : these settings, used to specify Timescaledb connection
    information, are propagated down from the `tobs` section via
    [db](#timescaledb-tobs) YAML anchors;
  - `promscale.` : these settings, used by the kafka/promscale bridge, are
    propagated down from the `tobs` section via
    [promscale](#promscale-tobs) YAML anchors;
    - `enabled` : set to false if you wish to disable the metrics bridge that
      reads from kafka and writes to PromScale &mdash; this bridge is required
      to store collected metrics into Timescaledb;
  - `kernelbeat.`
    - `enabled` : set to false if you wish to disable installation the
      KernelBeat daemonset. This daemonset will only be installed on nodes
      labeled `cmdaa.collect/kernelbeat: true` and will install the kernel_beat
      module and provide a metrics bridge that reads from the installed
      KernelBeat module and writes to kafka
      ([`bridge_kernelbeat_to_kafka`](#anchor.bridge_kernelbeat_to_kafka));
    - `init.repo` : these settings, used by the KernelBeat initialization
      container, are propagated down from the `minio` section via
      [s3/Minio](#s3minio) YAML anchors;
  - `sglogs.enabeld` : set to false if you wish to disable installation the
    sg-logs daemonset. This daemonset will only be installed on nodes labeled
    `cmdaa.collect/sg-logs: true` and will install sg-logs with settings to
    publish metrics to the prometheus-kafka bridge specified via the
    `prometheus` section within the collections values.yaml. This requires that
    the prometheus-kafka bridge be installed 
    ([`bridge_prometheus_to_kafka`](#anchor.bridge_prometheus_to_kafka));
  - `procfs_collector.enabled` : set to false if you wish to disable
    installation the procfs-collector daemonset. This daemonset will only be
    installed on nodes labeled `cmdaa.collect/procfs: true` and will install
    the procfs-based metrics collector. This requires that the prometheus-kafka
    bridge be installed
    ([`bridge_prometheus_to_kafka`](#anchor.bridge_prometheus_to_kafka));
  - `procmon_collector.enabled` : set to false if you wish to disable
    installation the eBPF-based process monitor daemonset. This daemonset will
    only be installed on nodes labeled `cmdaa.collect/procmon: true` and will
    install the eBPF-based process monitor metrics collector. This requires
    that the prometheus-kafka bridge be installed
    ([`bridge_prometheus_to_kafka`](#anchor.bridge_prometheus_to_kafka));
  - `kafka.` : these settings, used to specify the kafka connection
    information, are propagated down from the `kafka` section via
    [kafka](#kafka) YAML anchors;

- ingress
  - `dn` : to change theh DNS domain name used for ingress rules;
  - `entries` : if you wish to change the host name(s) exposed by ingress
    rules;


### Helpers
The [Makekefile](Makefile) relies heavily on the helper scripts in the
[etc/](etc/) sub-directory. Nearly all of the helpers rely on proper settings
within [config.yaml](config.yaml).

<table>
 <thead>
  <tr>
   <th nowrap><a href="etc/">etc/</a> helper</th>
   <th>purpose</th>
  </tr>
 </thead>
 <tbody>
  <tr><th colspan=2>utility wrappers</th></tr>
  <tr>
   <td nowrap><a href="etc/kubectl">kubectl</a></td>
   <td>
    provides contextualized execution of <code>kubectl</code>, making use of
    <a href="config.yaml">config.yaml</a> to determine the proper kubectl
    context and k8s namespace;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/helm">helm</a><br>&nbsp;</td>
   <td>
    provides contextualized execution of <code>helm</code>, making use of
    <a href="config.yaml">config.yaml</a>;
    <br>&nbsp;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/tobs">tobs</a><br>&nbsp;</td>
   <td>
    provides contextualized execution of
    <a href="https://github.com/timescale/tobs/blob/master/cli/README.md"
      >The OBservability Stack (tobs) cli utility</a>,
    making use of <a href="config.yaml">config.yaml</a> to determine the proper
    k8s namespace and helm release;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/timescale-patronictl">timescale-patronictl</a><br>&nbsp;</td>
   <td>
    provides contextualized execution of the `patronictl` utility used by
    `tobs` to provide management of the high-availability installation of
    Timescaledb;
    <br>&nbsp;
   </td>
  </tr>
  <tr><th colspan=2>base helpers</th></tr>
  <tr>
   <td nowrap><a href="etc/base-update">base-update</a></td>
   <td>
    generate a pre-templated version of this helm chart based upon
    <a href="config.yaml">config.yaml</a> into the <code>base/</code>
    sub-directory. This sub-directory may then be used via
    <a href="etc/kubectl">etc/kubectl</a> to directly manage k8s resources;
    <br>
    This may also be invoked via `make base`;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/generate-values">generate-values</a>
   </td>
   <td>
    generate a contextualized <code>values.yaml</code> from
    <a href="config.yaml">config.yaml</a> and
    <a href="values.yaml.tpl">values.yaml.tpl</a> ;
   </td>
  </tr>
  <tr><th colspan=2>configuration helpers</th></tr>
  <tr>
   <td nowrap><a href="etc/kubectl-context">kubectl-context</a></td>
   <td>
    retrieve the configured kubectl context from
    <a href="config.yaml">config.yaml</a>;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/k8s-namespace">k8s-namespace</a></td>
   <td>
    retrieve the configured kubernetes namespace from
    <a href="config.yaml">config.yaml</a>;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/helm-pull">helm-pull</a></td>
   <td>
    manually pull/package the charts referenced in
    <a href="Chart.yaml">Chart.yaml</a>;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/helm-release">helm-release</a></td>
   <td>
    retrieve the configured helm release from
    <a href="config.yaml">config.yaml</a> ;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/ingress-dn">ingress-dn</a></td>
   <td>
    retrieve the configured dns ingress domain from
    <a href="config.yaml">config.yaml</a> ;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/values.py">values.py</a></td>
   <td>
    generate a version of <code>values.yaml</code> with all yaml aliases
    expanded;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/value-get.py">value-get.py</a></td>
   <td>
    retrieve the value of a specific yaml key from a version of
    <code>values.yaml</code> that has all yaml aliases expanded;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/values-anchors.py">values-anchors.py</a></td>
   <td>
    retrieve the anchors/aliases from <code>values.yaml</code>;
   </td>
  </tr>
  <tr><th colspan=2>secret helpers</th></tr>
  <tr>
   <td nowrap><a href="etc/generate-s3-secrets">generate-s3-secrets</a></td>
   <td>
    (re)generate random access/secret keys for s3, updating
    <a href="templates/secret-s3.yaml">templates/secret-s3.yaml</a>;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/grafana-admin">grafana-admin</a></td>
   <td>
    retrieve the grafana admin user name and password;
    <br>
    this password may also be viewed and changed via
    `tobs grafana (get-password | change-password)`;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/minio-secrets-live">minio-secrets-live</a></td>
   <td>
    retrieve the minio secrets from the current/active deployment (if any);
   </td>
  </tr>
  <tr>
   <td nowrap><a
    href="etc/timescale-passwords-live">timescale-passwords-live</a></td>
   <td>
    retrieve the timescale credentials/passwords from the current/active
    deployment (if any);
    <br>
    the password of the super-user (postgres) may also be viewed and changed
    via `tobs timescaledb (get-password | change-password)`;
   </td>
  </tr>
  <tr><th colspan=2>support helpers</th></tr>
  <tr>
   <td nowrap><a href="etc/kubectl-ensure-ns">kubectl-ensure-ns</a></td>
   <td>
    ensures that the target k8s namespace exists in the target cluster. This is
    required because <code>helm</code> does not deal well with a namespace
    resource if that namespace already exists. So, the simple solution of
    including a <code>namespace.yaml</code> file in the top-level
    <a href="templates/">templates/</a> to define the target namespace is not a
    viable option given the current state of <code>helm</code>;
   </td>
  </tr>
  <tr>
   <td nowrap><a href="etc/resources-show">resources-show</a></td>
   <td>
    retrieve the set of resources currently available in the target
    cluster/namespace;
   </td>
  </tr>
 </tbody>
</table>


The remaining utilities are for accessing and managing resources (e.g. access
container, bridge containers).


## Managing via helm
Immediately after updating [config.yaml](config.yaml), you may use helm to
manage deployment.


### Full Install
You may perform an install/deployment either via `make` or directly using
`helm`:
```bash
$ make helm-install

# or ./etc/helm install $(./etc/helm-release) --values=values.yaml .
#    (assuming charts/ is up-to-date  [ make charts ],
#     values.yaml has been generated  [ make values.yaml ],
#     and the namespace exists        [ make ns ] )

# Show the new deployment
$ ./etc/helm list
```

**NOTE:** Normally, all required sub-charts will be automatically pulled from
the pre-packaged tarballs in `../../../repo/`. If however, you are a developer
modifying helm sources for testing, you will need to temporarily remove the
tarball of the repository you are working with (e.g.
`../../../repo/cmdaa-collections-0.0.25-1.tgz`), and ensure that all effected
charts are properly prepared with a `charts` sub-directory. For example:
```bash
$ make -C ../../infrastructure charts
$ make -C ../../collections    charts
$ make -C ../../analytics      charts
```


### Full Uninstall
You may perform an uninstall either via `make` or directly using `helm`:
```bash
$ make helm-uninstall

# or ./etc/helm uninstall $(./etc/helm-release)
```

**NOTE:** Due to what appears to be a bug in either `helm` or the `timescale`
helm charts, a patroni-related service (`%Release.Name%-timescale-config`)
*may not* be properly deleted. If this service persists, a second attempt to
install the chart will fail with complaints about an existing service that
cannot be imported into the current release. You may delete this service via
Rancher or via the command-line:
```bash
$ ./etc/kubectl delete $( ./etc/resources-show services )
```

**NOTE:** There will also be a set of resources that remain which *should not*
interfere with following installations:
- A patroni-related configuration endpoint
  (`endpoints/%Release.Name%-timescale-config`, may be removed via
  `./etc/clean-timescale`);
- A number of persistent volume claims for timescaledb, mongodb, and
  kafka that, if left, will allow follow-on installations to continue with the
  data from a previous installation (may be removed via `./etc/clean-pvc`);
- A number of argo workload-related pods that, if left, will show up in the
  argo ui as past workloads (may be removed via `./etc/clean-argo`);


### Sub-Chart Install
You may also use `helm` to directly install sub-charts thanks to the
`./etc/values.py` helper.

Even with this option, you will likely still need `kubectl` and the
pre-templated `base` in order to install prerequisite secrets.

For example, to install just the minio sub-chart:
```bash
# If charts/ and/or base/ does not yet exist
$ make base

#
# The minio sub-chart requires a pre-existing secret, so we must first use
# kubectl to create that secret
#
# (You may also randomize this secret using ./etc/generate-s3-secrets
#
$ ./etc/kubectl apply -f base/templates/secret-s3.yaml

#
# Now, use values.py to filter the values.yaml for direct use with the minio
# chart as if it were *not* a sub-chart
#
$ ./etc/values.py -c minio -g | \
    ./etc/helm install minio \
              --values=- \
              ./charts/minio-5.0.32.tgz
```

### YAML anchors
The [values.yaml](values.yaml) file makes use of a number of YAML anchors.

Each anchor is used in one or more sections of the values file. To effectively
over-ride the use of a YAML anchor via an external method like `helm --set`,
each use must have a corresponding over-ride.

In this section, we document all YAML anchors, where they are defined and where
they are referenced throughout the values file.

**NOTE:** the full set of anchors/aliases may be viewed using the
<a href="etc/values-anchors.py">values-anchors.py</a> helper.


#### Mongodb
Mongodb will be installed iff `global.install.mongodb` is true.

When installed, these settings will be used for that installation and are also
propagate to helm charts that require mongodb connectivity.

- `install_mongodb`: Specify whether to install mongodb;
  ```
  Defined   : global.install.mongodb [ true ]
  Referenced: mongodb.enabled
  ```

- `mongo_secret`: Specify the name of the secret containing mongodb
  credentials;
  ```
  Defined   : mongodb.secrets.secret [ cmdaa-secret-mongo ]
  Referenced: mongodb.existingSecret
            : analytics.mongo.secrets.secret
  ```

- `mongo_key_password`: Specify the name of the key within `mongo_secret` that
  contains the user password;
  ```
  Defined   : mongodb.secrets.keys.password [ mongodb-password ]
  Referenced: analytics.mongo.secrets.passwordKey
  ```

- `mongo_key_root_password`: Specify the name of the key within `mongo_secret`
  that contains the root password;
  ```
  Defined   : mongodb.secrets.keys.root_password [ mongodb-root-password ]
  Referenced:
  ```

- `mongo_key_replica_set`: Specify the name of the key within `mongo_secret`
  that contains the replica-set password;
  ```
  Defined   : mongodb.secrets.keys.replica_set [ mongodb-replica-set-key ]
  Referenced:
  ```

- `mongo_fullName`: Mongodb service/host full name override;
  ```
  Defined   : mongodb.fullnameOverride [ cmdaa-mongodb ]
  Referenced: analytics.mongo.server
  ```

- `mongo_dbUser`: Specify the name of the mongodb user;
  ```
  Defined   : mongodb.mongodbUsername [ cmdaa ]
  Referenced: analytics.mongo.dbUsername
  ```

- `mongo_db`: Specify the name of the mongodb database;
  ```
  Defined   : mongodb.mongodbDatabase [ cmdaa ]
  Referenced: analytics.mongo.database
  ```

- `mongo_replica_enabled`: Specify whether mongodb replication is enabled;
  ```
  Defined   : mongodb.replicaSet.enabled [ true ]
  Referenced:
  ```

- `mongo_replica_arbiter`: Specify the number of replica arbiters;
  ```
  Defined   : mongodb.replicaSet.replicas.arbiter [ 0 ]
  Referenced:
  ```

- `mongo_replica_secondary`: Specify the number of secondary replicas;
  ```
  Defined   : mongodb.replicaSet.replicas.secondary [ 2 ]
  Referenced:
  ```

- `mongo_replica_pvc`: Specify the name of the mongodb VolumeClaim to use for
  persistence;
  ```
  Defined   : mongodb.persistence.existingClaim [ cmdaa-mongodb ]
  Referenced:
  ```

- `mongo_port`: mongodb service port;
  ```
  Defined   : mongodb.service.port [ 27017 ]
  Referenced: analytics.mongo.port
  ```

#### S3/Minio
Minio will be installed iff `global.install.minio` is true.

When installed, these settings will be used for that installation and are also
propagate to helm charts that require s3/minio connectivity.

- `install_minio`: Specify whether to install Minio
  ```
  Defined   : global.install.minio [ true ]
  Referenced: minio.enabled
            : ingress.entries.0.enabled
  ```

- `s3_secret`: Specify the name of the secret containing s3/minio credentials;
  ```
  Defined   : minio.secrets.secret [ cmdaa-secret-s3 ]
  Referenced: minio.existingSecret
            : argo.artifactRepository.s3.accessKeySecret.name
            : argo.artifactRepository.s3.secretKeySecret.name
            : tobs.timescaledb-single.backup.env.0.valueFrom.secretKeyRef.name
            : tobs.timescaledb-single.backup.env.1.valueFrom.secretKeyRef.name
            : analytics.minio.secret.name
            : collections.kernel_beat.init.repo.secret.name
  ```

- `s3_key_access`: Specify the name of the key within `s3_secret` that 
  contains the access key;
  ```
  Defined   : minio.secrets.keys.access [ accesskey ]
  Referenced: argo.artifactRepository.s3.accessKeySecret.key
            : tobs.timescaledb-single.backup.env.0.valueFrom.secretKeyRef.key
            : analytics.minio.secret.accesskey
            : collections.kernel_beat.init.repo.secret.keys.access
  ```

- `s3_key_secret`: Specify the name of the key within `s3_secret` that 
  contains the secret key;
  ```
  Defined   : minio.secrets.keys.secret [ secretkey ]
  Referenced: argo.artifactRepository.s3.secretKeySecret.key
            : tobs.timescaledb-single.backup.env.1.valueFrom.secretKeyRef.key
            : analytics.minio.secret.secretkey
            : collections.kernel_beat.init.repo.secret.keys.secret
  ```

- <a name="anchor.s3_fullName"></a>`s3_fullName`: s3/minio service/host full name override;
  ```
  Defined   : minio.fullnameOverride [ cmdaa-minio ]
  Referenced: argo.artifactRepository.s3.host
            : tobs.timescaledb-single.backup.pgBackRest.repo1-s3-endpoint
            : analytics.minio.server
            : collections.kernel_beat.init.repo.host
            : ingress.entries.0.service.fullName
  ```

- <a name="anchor.s3_port"></a>`s3_port`: Specify the s3/minio service port;
  ```
  Defined   : minio.service.port [ 9000 ]
  Referenced: argo.artifactRepository.s3.port
            : tobs.timescaledb-single.backup.pgBackRest.repo1-s3-port
            : analytics.minio.port
            : collections.kernel_beat.init.repo.port
            : ingress.entries.0.service.port
  ```

- <a name="anchor.s3_bucket_workflow"></a>`s3_bucket_workflow`: Specify the
  name of the s3/minio bucket that will be used for CMDAA workflow artifacts;
  ```
  Defined   : minio.buckets.0.name [ argo-artifacts ]
  Referenced: argo.artifactRepository.s3.bucket
            : analytics.minio.bucket.workflow
  ```

- <a name="anchor.s3_bucket_service"></a>`s3_bucket_service`: Specify the name
  of the s3/minio bucket that will be used for the CMDAA analytics storage
  service;
  ```
  Defined   : minio.buckets.1.name [ cmdaa ]
  Referenced: analytics.minio.bucket.service
  ```

- <a name="anchor.s3_bucket_db"></a>`s3_bucket_db`: Specify the name of the
  s3/minio bucket that will be used for CMDAA database backups;
  ```
  Defined   : minio.buckets.2.name [ cmdaa-db-backups ]
  Referenced: tobs.timescaledb-single.backup.pgBackRest.repo1-s3-bucket
  ```

- `s3_bucket_kb`: Specify the name of the s3/minio bucket that will be used to
  cache generated, host-specific KernelBeat modules;
  ```
  Defined   : minio.buckets.3.name [ kernel-beat ]
  Referenced: collections.kernel_beat.init.repo.bucket
  ```

#### Kafka

- `install_kafka`: Specify whether to install Kafka
  ```
  Defined   : global.install.kafka [ true ]
  Referenced: kafka.enabled
  ```

- <a name="anchor.kafka_host"></a>`kafka_host`: Kafka service/host full name
  override
  ```
  Defined   : kafka.fullnameOverride [ cmdaa-kafka ]
  Referenced: analytics.kafka.server
            : collections.kafka.host
  ```

- <a name="anchor.kafka_port"></a>`kafka_port`: Kafka service port;
  ```
  Defined   : kafka.service.port [ 9092 ]
  Referenced: analytics.kafka.port
            : collections.kafka.port
  ```

- `kafka_max_bytes`: Specify the maximum number of bytes permitted for a
  message within kafka;
  ```
  Defined   : kafka.maxMessageBytes [ _2097152 ]
  Referenced: collections.kafka.request_max_bytes
  ```

- `kafka_replica_count`: Specify the kafka replication factor;
  ```
  Defined   : kafka.defaultReplicationFactor [ 1 ]
  Referenced: kafka.offsetsTopicReplicationFactor
            : kafka.transactionStateLogReplicationFactor
            : kafka.transactionStateLogMinIsr
  ```

- `kafka_partitions`: Specify the number of kafka partitions;
  ```
  Defined   : kafka.numPartitions [ 3 ]
  Referenced:
  ```

#### Argo
Argo (workflow and events) will be installed iff `global.install.argo` is true.

When installed, these settings will be used for that installation and are also
propagate to helm charts that require Argo connectivity.

- `install_argo`: Specify whether to install Argo;
  ```
  Defined   : global.install.argo [ true ]
  Referenced: argo.enabled
            : ingress.entries.3.enabled
  ```

- <a name="anchor.argo_name"></a>`argo_name`: Argo-UI service/host full name
  override;
  ```
  Defined   : argo.server.name [ argo-ui ]
  Referenced: ingress.entries.3.service.name
  ```

- <a name="anchor.argo_port"></a>`argo_port`: Argo-UI service port;
  ```
  Defined   : argo.server.servicePort [ 2746 ]
  Referenced: ingress.entries.3.service.port
  ```

#### The OBservability Stack (tobs)
The primary components of tobs are Timescaledb and Promscale, though it also
includes Grafana and Promlens.

**NOTE:** The prometheus chart included with tobs is disabled in favor of a
top-level chart to allow template overrides.

- `install_tobs`: Specify whether to install The OBservability Stack (tobs :
  Timescaledb, Promscale, and optionally Grafana and PromLens);
  ```
  Defined   : global.install.tobs [ true ]
  Referenced: tobs.enabled
            : prometheus.server.remoteRead.0.enabled
  ```

##### Timescaledb (tobs)
Installing tobs (`global.install.tobs`) will always include an installation of Timescaledb.

When installed, these settings will be used for that installation and are also
propagate to helm charts that require Timescaledb/Postgresql connectivity.

- <a name="anchor.db_host"></a>`db_host`: timescaledb full name override;
  ```
  Defined   : db.host [ cmdaa-timescale ]
  Referenced: tobs.timescaledb-single.clusterName
            : tobs.promscale.connection.host.nameTemplate
            : tobs.grafana.timescale.database.host
            : tobs.grafana.timescale.datasource.host
            : collections.timescale.host
  ```

- <a name="anchor.db_port"></a>`db_port`: timescaledb port number;
  ```
  Defined   : db.port [ 5432 ]
  Referenced: tobs.timescaledb-single.loadBalancer.port
            : tobs.promscale.connection.port
            : tobs.grafana.timescale.database.port
            : tobs.grafana.timescale.datasource.port
            : collections.timescale.port
  ```

- <a name="anchor.db_creds"></a>`db_creds`: Specify the name of the timescale
  database credentials secret;
  ```
  Defined   : db.secrets.creds [ cmdaa-timescaledb-passwords ]
  Referenced: tobs.promscale.connection.password.secretTemplate
            : tobs.grafana.timescale.adminPassSecret
            : collections.timescale.secret.name
  ```

- <a name="anchor.db_user_app"></a>`db_user_app`: Specify the name of the
  timescale application user;
  ```
  Defined   : db.users.app [ cmdaa ]
  Referenced: tobs.timescaledb-single.credentials.%db_user_app%
            : collections.timescale.user
            : collections.timescale.secret.key
  ```

- `db_user_super`: Specify the name of the timescale super user;
  ```
  Defined   : db.users.super [ postgres ]
  Referenced: tobs.timescaledb-single.credentials.%db_user_super%
            : tobs.promscale.connection.user
            : tobs.grafana.timescale.adminUser
  ```

- `db_user_replication`: Specify the name of the timescale replication user;
  ```
  Defined   : db.users.replication [ standby ]
  Referenced: tobs.timescaledb-single.credentials.%db_user_replication%
  ```

- `db_user_admin`: Specify the name of the timescale admin user;
  ```
  Defined   : db.users.admin [ admin ]
  Referenced: tobs.timescaledb-single.credentials.%db_user_admin%
  ```

- <a name="anchor.db_name_collections"></a>`db_name_collections`: Specify the
  name of the metric collections database;
  ```
  Defined   : db.dbNames.collections [ collections ]
  Referenced: tobs.promscale.connection.dbName
            : tobs.grafana.timescale.datasource.dbName
            : collections.timescale.db
  ```

- `db_name_grafana`: Specify the name of the grafana database;
  ```
  Defined   : db.dbNames.grafana [ grafana ]
  Referenced: tobs.grafana.timescale.database.dbName
  ```

- `telemetry_level`: Specify the level for Timescaledb telemetry;
  ```
  Defined   : db.telemetry_level [ off ]
  Referenced: tobs.timescaledb-single.patroni.postgresql.parameters.timescaledb.telemetry_level
            : tobs.timescaledb-single.patroni.bootstrap.dcs.postgresql.parameters.timescaledb.telemetry_level
  ```

##### Promscale (tobs)
Promscale will be installed iff `global.install.promscale` is true though it
will only be functional if `global.install.tobs` is also true.

When installed, these settings will be used for that installation and are also
propagate to helm charts that require Promscale connectivity.

- `install_promscale`: Specify whether to install the Promscale portion of
  tobs;
  ```
  Defined   : global.install.promscale [ true ]
  Referenced: tobs.promscale.enabled
  ```

- <a name="anchor.promscale_fullName"></a>`promscale_fullName`: Promscale
   service/host full name override;
  ```
  Defined   : tobs.promscale.connector [ cmdaa-promscale-connector ]
  Referenced: prometheus.server.remoteWrite.1.host
            : prometheus.server.remoteRead.0.host
            : collections.promscale.host
  ```

- `promscale_endpoint_write`: The Promscale write endpoint path;
  ```
  Defined   : tobs.promscale.endpoint.write [ write ]
  Referenced: prometheus.server.remoteWrite.1.endpoint
  ```

- `promscale_endpoint_read`: The Promscale read endpoint path;
  ```
  Defined   : tobs.promscale.endpoint.read [ read ]
  Referenced: prometheus.server.remoteRead.0.endpoint
  ```

- <a name="anchor.promscale_port"></a>`promscale_port`: Promscale service port;
  ```
  Defined   : tobs.promscale.service.port [ 9201 ]
  Referenced: prometheus.server.remoteWrite.1.port
            : prometheus.server.remoteRead.0.port
            : collections.promscale.port
  ```

##### Grafana (tobs)
Grafana will be installed iff `global.install.grafana` is true though it will
only be functional if `global.install.tobs` is also true.

When installed, these settings will be used for that installation.

- `install_grafana`: Specify whether to install the Grafana portion of tobs;
  ```
  Defined   : global.install.grafana [ true ]
  Referenced: tobs.grafana.enabled
            : ingress.entries.1.enabled
  ```

- <a name="anchor.grafana_fullName"></a>`grafana_fullName`: Grafana serce/host
  full name override;

  ```
  Defined   : tobs.grafana.fullnameOverride [ cmdaa-grafana ]
  Referenced: ingress.entries.1.service.fullName
  ```

- <a name="anchor.grafana_port"></a>`grafana_port`: Grafana service port;
  ```
  Defined   : tobs.grafana.service.targetPort [ 3000 ]
  Referenced: ingress.entries.1.service.port
  ```

##### Promlens (tobs)
Promlens will be installed iff `global.install.promlens` is true though it
will only be functional if `global.install.tobs` is also true.

When installed, these settings will be used for that installation.

- `install_promlens`: Specify whether to install the Promlens portion of tobs;
  ```
  Defined   : global.install.promlens [ false ]
  Referenced: tobs.promlens.enabled
            : ingress.entries.2.enabled
  ```

- `promlens_fullName`: PromLens service/host full name override;
  ```
  Defined   : tobs.promlens.host [ cmdaa-promlens ]
  Referenced: ingress.entries.2.service.fullName
  ```

- `promlens_port`: PromLens service port;
  ```
  Defined   : tobs.promlens.port [ 80 ]
  Referenced: ingress.entries.2.service.port
  ```

#### Prometheus
While tobs provides an optional Prometheus installation, we use a top-level
install instead to allow template overrides.

- `install_prometheus`: Specify whether to install Prometheus;
  ```
  Defined   : global.install.prometheus [ true ]
  Referenced: prometheus.enabled
  ```

#### Prometheus/Kafka Adapter
The Prometheus/Kafka adapter will be installed iff `global.install.kafkaAdapter`
is true though it will only be functional if `global.install.kafka`,
`global.install.tobs`,  and `global.install.prometheus` are all true.

When installed, these settings will be used for that installation.

- `install_kafkaAdapter`: Specify whether to install the Promethus/Kafka
  adapter;
  ```
  Defined   : global.install.kafkaAdapter [ true ]
  Referenced: prometheus-kafka-adapter.enabled
            : prometheus.server.remoteWrite.0.enabled
  ```

- <a name="anchor.kafka_adapter_fullName"></a>`kafka_adapter_fullName`:
  Prometheus/Kafka adapter service/host full name override;
  ```
  Defined   : prometheus-kafka-adapter.fullnameOverride [ cmdaa-kafka-adapter ]
  Referenced: prometheus.server.remoteWrite.0.host
  ```

- <a name="anchor.kafka_adapter_port"></a>`kafka_adapter_port`:
  Prometheus/Kafka adapter service port;
  ```
  Defined   : prometheus-kafka-adapter.service.port [ 80 ]
  Referenced: prometheus.server.remoteWrite.0.port
  ```

- `kafka_adapter_topic`: The kafka topic for the Prometheus/Kafaka adapter;
  ```
  Defined   : prometheus-kafka-adapter.environment.KAFKA_TOPIC [ metrics_prometheus ]
  Referenced: collections.kafka.topics.7
  ```

- `kafka_adapter_endpoint`: The Prometheus/Kafka adpater receive endpoint path;
  ```
  Defined   : prometheus-kafka-adapter.endpoint [ receive ]
  Referenced: prometheus.server.remoteWrite.0.endpoint
  ```

#### CMDAA analytics
The CMDAA analytics will be installed iff `global.install.analytics` is true.

The analytics chart relies heavily on:
- minio
- mongodb
- Timescaledb (tobs)
- argo


When installed, these settings will be used for that installation.

- `install_analytics`: Specify whether to install CMDAA analytics (requires
  minio, [kafka], mongodb, tobs (timescale/postgres), argo);
  ```
  Defined   : global.install.analytics [ true ]
  Referenced: analytics.enabled
            : ingress.entries.4.enabled
            : ingress.entries.5.enabled
            : ingress.entries.6.enabled
  ```

- `analytics_auth_fullName`: Analytics authentication service/host name;
  ```
  Defined   : analytics.cmdaaAuth.service.name [ cmdaa-auth ]
  Referenced: ingress.entries.4.service.fullName
  ```

- `analytics_auth_port`: Analytics authentication service port;
  ```
  Defined   : analytics.cmdaaAuth.service.port [ 80 ]
  Referenced: ingress.entries.4.service.port
  ```

- `analytics_gs_fullName`: Analytics GrokStore service/host name;
  ```
  Defined   : analytics.grokstore.service.name [ cmdaa-grokstore ]
  Referenced: ingress.entries.5.service.fullName
  ```

- `analytics_gs_port`: Analytics GrokStore service port;
  ```
  Defined   : analytics.grokstore.service.port [ 80 ]
  Referenced: ingress.entries.5.service.port
  ```

- `analytics_ui_fullName`: Analytics UI service/host name;
  ```
  Defined   : analytics.ui.service.name [ cmdaa-ui ]
  Referenced: ingress.entries.6.service.fullName
  ```

- `analytics_ui_port`: Analytics UI service port;
  ```
  Defined   : analytics.ui.service.port [ 80 ]
  Referenced: ingress.entries.6.service.port
  ```

- `analytics_fdgen_fullName`: Analytics Fluentd Generation service/host name;
  ```
  Defined   : analytics.fluentdGen.service.name [ cmdaa-fdgen ]
  Referenced: ingress.entries.7.service.fullName
  ```

- `analytics_fdgen_port`: Analytics Fluentd Generation service port;
  ```
  Defined   : analytics.fluentdGen.service.port [ 8080 ]
  Referenced: ingress.entries.7.service.port
  ```

- `kafka_logs_topic`: The kafka topic to use for Analytics fluentd-based log
  flows;
  ```
  Defined   : analytics.kafka.topics [ fluentd, *kafka_adapter_topic ]
  Referenced:
  ```

#### CMDAA collections
The CMDAA metric collections will be installed iff `global.install.collections`
is true.

The collections chart relies heavily on:
- minio
- kafka
- Timescaledb (tobs)
- Promscale (tobs)
- Prometheus
- Prometheus/Kafka Adapter


When installed, these settings will be used for that installation.

- `install_collections`: Specify whether to install CMDAA metric collections
  (requires kafka and tobs (timescale, promscale);
  ```
  Defined   : global.install.collections [ true ]
  Referenced: collections.enabled
  ```

- <a name="anchor.bridge_kernelbeat_to_kafka"></a>`bridge_kernelbeat_to_kafka`:
  Specify whether the KernelBeat/Kafka bridge should be included for collections;
  ```
  Defined   : global.install.kernelbeat_to_kafka [ true ]
  Referenced: collections.kernel_beat.enabled
            : collections.kernel_beat.init.repo.enabled
  ```

- `bridge_sglogs_to_kafka`: Specify whether the sg-logs/Kafka bridge
  should be included for collections;
  ```
  Defined   : global.install.sglogs_to_kafka [ true ]
  Referenced: collections.sglogs.enabled
  ```

- `bridge_procfs_to_kafka`: Specify whether the procfs-collector should be
  included for collections;
  ```
  Defined   : global.install.procfs_to_kafka [ true ]
  Referenced: collections.procfs_collector.enabled
  ```

- `bridge_procmon_to_kafka`: Specify whether the eBPF-based process monitor
  should be included for collections;
  ```
  Defined   : global.install.procmon_to_kafka [ true ]
  Referenced: collections.procmon_collector.enabled
  ```

- `bridge_kafka_to_promscale`: Specify whether the kafka/Promscale bridge
  should be included for collections;
  ```
  Defined   : global.install.kafka_to_promscale [ true ]
  Referenced: collections.promscale.enabled
  ```

- <a name="anchor.bridge_prometheus_to_kafka"></a>`bridge_prometheus_to_kafka`:
  Specify whether the Prometheus/kafka bridge should be included for collections;
  ```
  Defined   : global.install.prometheus_to_kafka [ true ]
  Referenced: collections.prometheus.enabled
  ```

---

## Managing via kubectl
There is also support for generating pre-rendered templates that can be
installed, updated, and deleted directly using `kubectl`.

To generate the pre-rendered templates:
```bash
$ make base

# or ./etc/base-update
#    (assuming charts/ is up-to-date  [ make charts ],
#     values.yaml has been generated  [ make values.yaml ],
#     and the namespace exists        [ make ns ] )
```

This will generate a `base` sub-directory that may then be used directly by
`kubectl` to manage resources.


### Install
Using the generated `base` sub-directory, you may install the full deployment
or any pieces of the deployment.

You may perform a full install/deployment either via `make` or directly using
`kubectl`:
```bash
$ make kubectl-apply

# or ./etc/kubectl apply -k base
```

To deploy just the minio-related resources:
```bash
$ ./etc/kubectl apply -k base/charts/minio
```

For more examples, look at the various install scripts in [etc/](etc/).


### Uninstall
The generated `base` sub-directory may also be used to uninstall/delete the
full deployment or any pieces of the deployment.

To delete all deployed resources using `kubectl`:
```bash
$ make kubectl-delete

# or ./etc/kubectl delete -k base
```

To delete just minio-related resources:
```bash
$ ./etc/kubectl delete -k base/charts/minio
```

For more examples, look at the various delete scripts in [etc/](etc/).
