# vi: ft=yaml
##############################################################################
# Templatable values are pulled from config.yaml:
#   .Release.Namespace    [ {{ .Release.Namespace }} ]
#   .Release.Name         [ {{ .Release.Name }} ]
#
#   :NOTE: Aliases:
#           .Config.k8s_namespace => .Release.Namespace
#           .Config.helm_release  => .Release.Name
#
##############################################################################
# Global values
#
global: # {
  # Storage class for fast storage (kafka, timescale)
  fastStorageClass: &fast_storage "local-path"

  storageClass:     *fast_storage  # effects: kafka

  # Generated secret length (infrastructure)
  secretLen:        32

  ## :XXX: The default k8s internal cluster domain is 'cluster.local'
  ##       If this is changed, change this value to propagate to the
  ##       sub-charts that allow an override (minio, kafka, mongodb).
  ##       BUT, there are charts that do NOT allow an override
  ##       (prometheus).
  #clusterDomain:  &k8s_clusterDomain  'cluster.local'

  # DNS domain name for ingress rules
  ingress_dn: &ingress_dn cmdaa   # Override via values.mixin

  ###
  # ElasticSearch-related global variables {
  #
  kibanaEnabled:  true  # Used iff `global.install.elasticsearch`

  elasticsearch: # { used iff kibanaEnabled
    # :XXX: If `global.kibanaEnabled` is true, these settings override
    #       `elasticsearch` service information.
    #
    #       The use of `service.fullnameOverride` depends upon a CMDAA-specific
    #       version of the elasticsearch chart.
    service: # {
      fullnameOverride: &elastic_fullName {{ .Release.Name }}-elastic

      ports: #{
        restAPI: &elastic_port     9200
      # } global.elasticsearch.service.ports
    # } global.elasticsearch.service
  # } global.elasticsearch
  #
  # ElasticSearch-related global variables }
  ###

  install: # {
    grafana:        &install_grafana      true

    ########################################################
    # CMDAA Common Infrastructure
    #
    infrastructure: &install_infrastructure true

    # Infrastructure components {
    nginxIngress:   false   # only use if not already in your cluster

    kafka:          &install_kafka        true
    timescaledb:    &install_timescaledb  true
    elasticsearch:  &install_elastic      true

    minio:          &install_minio        true
    argo:           &install_argo         true
    mongodb:        &install_mongodb      true
    # Infrastructure components }

    ########################################################
    # CMDAA Analytics
    #   Requires: timescale/postgres, kafka, argo, mongodb
    analytics:      &install_analytics    true

    ########################################################
    # CMDAA Collections
    #   Requires: kafka, timescale, promscale
    collections:    &install_collections  true

    ########################################################
    # Debug/access deployments
    #
    debug_access:     true         # {{ .Release.Name }}-access
  # } global.install
# } global
##############################################################################
# Accessible values {
configMap: # {
  initDbGrafana: &ts_cm_initDbGrafana {{ .Release.Name }}-init-db-grafana
# } cmNames
# Accessible values }
##############################################################################

infrastructure: # { iff `global.install.infrastructure`
  # Developer access
  dev: # {
    enabled:  false

    #image: # {
    #  name:       node
    #  tag:        18.13.0-alpine
    #  pullPolicy: IfNotPresent
    ## } infrastructure.dev.image

    #source: # { volum definition for the /src mount
    #  hostPath: # {
    #    path: /home/depeele/src/cmdaa/web-ui
    #  # } infrastructure.dev.source.hostPath
    ## } infrastructure.dev.source
  # } infrastructure.dev

  # Shared timescaledb meta-data
  tsMeta: # {
    # :NOTE: This COULD be 'hostTemplate' but then every use would need
    #        to pass it through `tpl`...
    host: &ts_host  {{ .Release.Name}}-timescale
    port: &ts_port  "5432"  # :XXX: MUST be a string for use in
                            #       .infrastructure.promscale.extraEnv

    secrets: # {
      # Secret names
      creds:      &ts_secret_creds  {{ .Release.Name }}-timescaledb-credentials
      cert:       &ts_secret_cert   {{ .Release.Name }}-timescaledb-certificate
      pgbackrest: &ts_secret_backup {{ .Release.Name }}-pgbackrest

      # An empty secret used for timescaledb and promscale to ensure the
      # sub-charts don't override our secret-based environment settings
      empty:      &ts_secret_empty  {{ .Release.Name }}-empty
    # } infrastructure.tsMeta.secrets

    # User names
    users: # {
      super: # {
        # PATRONI_SUPERUSER_[USERNAME, PASSWORD]
        name:     &ts_user_super        postgres
      # } infrastructure.tsMeta.users.super

      replication: # {
        # PATRONI_REPLICATION_[USERNAME, PASSWORD]
        name:     &ts_user_replication  standby
      # } infrastructure.tsMeta.users.replication

      admin: # {
        # PATRONI_admin_[USERNAME, PASSWORD]
        name:     &ts_user_admin        admin
      # } infrastructure.tsMeta.users.amin

      app:  # {
        name:     &ts_user_app          cmdaa
      # } infrastructure.tsMeta.users.app

      grafana: # {
        name:     &ts_user_grafana      grafana
      # } infrastructure.tsMeta.users.grafana
    # } infrastructure.tsMeta.users

    # Database names
    dbNames: # {
      collections:  &ts_name_collections  collections
      grafana:      &ts_name_grafana      grafana
    # } infrastructure.tsMeta.dbNames

    # Extra volumes for the initdb job
    initVolumes: # {
      # {
      - name:       init-db-grafana
        mountPath:  /initdb/01-grafana.d
        readOnly:   true
        volume: # {
          configMap: # {
            name: *ts_cm_initDbGrafana
          # } infrastructure.tsMeta.initVolumes[init-db-gf].volume.configMap
        # } infrastructure.tsMeta.initVolumes[init-db-gf].volume
      # } infrastructure.tsMeta.initVolumes[init-db-gf]
    # } infrastructure.tsMeta.initVolumes
  # } infrastructure.tsMeta

  mongodb: # { iff `global.install.mongodb`
    enabled:  *install_mongodb

    ############################################
    # Accessible values {
    secrets: # {
      #
      # :NOTE: The mongo chart requires specific keys within this secret:
      #         https://github.com/bitnami/charts/tree/master/bitnami/mongodb
      #
      #         mongodb-password
      #         mongodb-root-password
      #         mongodb-replica-set-key
      #
      #         A compliant secrets file may be generated via:
      #           ./etc/generate-mongodb-secrets
      #             which uses the keys below to generate random passwords for
      #             each.
      #
      secret:   &mongo_secret   {{ .Release.Name }}-secret-mongo

      keys: # {
        password:       &mongo_key_password       mongodb-password
        root_password:  &mongo_key_root_password  mongodb-root-password
        replica_set:    &mongo_key_replica_set    mongodb-replica-set-key
      # } infrastructure.mongodb.secrets.keys
    # } infrastructure.mongodb.secrets
    # Accessible host/port and secrets values }
    ############################################

    #clusterDomain:  *k8s_clusterDomain

    #default name is generated via {{ template "mongodb.fullname" }}
    # which checks for:
    #  fullnameOverride:  mongodb           # => mongodb
    #  nameOverride:      mongo             # => cmdaa-mongo
    #  defaulting to:     %Release%-mongodb # => cmdaa-mongodb
    fullnameOverride: &mongo_fullName {{ .Release.Name }}-mongodb

    existingSecret:   *mongo_secret

    mongodbUsername:  &mongo_dbUser   cmdaa
    mongodbDatabase:  &mongo_db       cmdaa

    service: # {
      port: &mongo_port     27017
    # } infrastructure.mongodb.service
  # } infrastructure.mongodb

  minio: # { iff `global.install.minio`
    enabled:  *install_minio

    ############################################
    # Accessible values {
    secrets: # {
      #
      # :NOTE: These are used by the ./etc/generate-s3-secrets script to create
      #        templates/secret-s3.yaml
      #
      secret:   &s3_secret  {{ .Release.Name }}-secret-s3

      keys: # {
        access:   &s3_key_access  accesskey
        secret:   &s3_key_secret  secretkey
      # } infrastructure.minio.secrets.keys
    # } infrastructure.minio.secrets
    # Accessible host/port and secrets values }
    ############################################

    #clusterDomain:  *k8s_clusterDomain

    #default name is generated via {{ template "minio.fullname" }}
    # which checks for:
    #  fullnameOverride:  s3              # => s3
    #  nameOverride:      artifacts       # => cmdaa-artifacts
    #  defaulting to:     %Release%-minio # => cmdaa-minio
    fullnameOverride: &s3_fullName  {{ .Release.Name }}-minio

    service: # {
      #type: LoadBalancer

      port: &s3_port      "9000"  # :XXX: MUST be a string for use in
                                  #       .infrastructure.timescaledb.backup.env
    # } infrastructure.minio.service

    # The set of buckets that should be created upon initialization
    buckets: # {
      # argo workflow
      - name:   &s3_bucket_workflow argo-artifacts
        policy: public
        purge:  false

      # grok-maker
      #
      # :XXX: Due to a bug in cmdaa-ui this bucket MUST be named 'cmdaa'
      #       since there is no way to override a hardcoded bucket name.
      #
      - name:   &s3_bucket_service  cmdaa   #cmdaa-artifacts
        policy: public
        purge:  false

      # pgbackrest (timescale backups via Patroni)
      - name:   &s3_bucket_db {{ .Release.Name }}-db-backups
        policy: public
        purge:  false

      # kernel_beat repository
      - name:   &s3_bucket_kb       kernel-beat
        policy: public
        purge:  false

      # flink working bucket
      - name:   &s3_bucket_flink    flink
        policy: public
        purge:  false
    # } infrastructure.minio.buckets

    existingSecret: *s3_secret
  # } infrastructure.minio

  kafka: # { iff `global.install.kafka`
    enabled:  *install_kafka

    ############################################
    # Accessible values {
    # host: &kafka_host {{ .Release.Name }}-kafka
    host: &kafka_host {{ .Release.Name }}-kafka
    port: &kafka_port 9092
    # Accessible host/port values }
    ############################################

    #clusterDomain:  *k8s_clusterDomain
    #zookeeper: # {
    #  clusterDomain:  *k8s_clusterDomain
    ## } infrastructure.kafka.zookeeper

    #default name is generated via {{ template "kafka.fullname" }}
    # which checks for:
    #  fullnameOverride:  kafka           # => kafka
    #  nameOverride:      bus             # => cmdaa-bus
    #  defaulting to:     %Release%-kafka # => cmdaa-kafka
    fullnameOverride: *kafka_host

    service: # {
      port: *kafka_port
    # } infrastructure.kafka.service

    replicaCount: 3  # 1

    persistence: #{
      storageClass: *fast_storage # over-ridden by global.storageClass
    #} infrastructure.kafka.persistence

    # :NOTE: This REQUIRES that the node label 'cmdaa/kafka' be added to
    #        any node that is permitted to run kafka:
    #           kubectl -n {{ .Release.Namespace }} \
    #             label nodes %node% 'cmdaa/kafka=true'
    nodeSelector: # {
      cmdaa/kafka:  "true"
    # } infrastructure.kafka.nodeSelector
  # } infrastructure.kafka

  argo-workflows: # { iff `global.install.argo`
    enabled:  *install_argo

    # Override from 'argo-workflows' to just 'argo'
    fullnameOverride: {{ .Release.Name }}-argo

    server: # {
      # :XXX: There is no full override for this name.
      #       This is simply a reflection of the name that will be generated
      #       given 'infrastructure.argo-workflows.server.name: server'
      fullnameOverride: &argo_fullName  {{ .Release.Name }}-argo-server
      servicePort:      &argo_port  2746
    # } infrastructure.argo-workflows.server

    artifactRepository: # {
      s3: # {
        host:     *s3_fullName
        port:     *s3_port
        bucket:   *s3_bucket_workflow

        accessKeySecret:
          name: *s3_secret
          key:  *s3_key_access

        secretKeySecret:
          name: *s3_secret
          key:  *s3_key_secret

      # } infrastructure.argo-workflows.artifactRepository.s3
    # } infrastructure.argo-workflows.artifactRepository
  # } infrastructure.argo-workflows

  #argo-events: # {
  #  serviceAccount: &argo_account argo-events-sa
  #
  #  eventsourceController: # {
  #    fullName:     &argo_events_fullName   {{ .Release.Name }}-eventsource-controller
  #    name:         eventsource-controller
  #  # } infrastructure.argo-events.eventsourceController
  ## } infrastructure.argo-events

  timescaledb: # {
    enabled:  *install_timescaledb

    #default name is generated via {{ template "timescaledb.fullname" }}
    # which checks for:
    #  fullnameOverride:  timescale         # => timescale
    #  nameOverride:      timescale         # => cmdaa-timescale
    #  defaulting to:     %Release%-ts      # => cmdaa-ts
    fullnameOverride: *ts_host

    # :XXX: Possible issue:
    #         With multiple replicas, only one will be a writable master while
    #         all others will be read-only replicas. In this scenario, any
    #         connector that needs to write MUST connect to the master.
    #
    replicaCount: 3 # [default: 3]

    # Timescale/Postgres/Patroni cluster name which is also used as the name of
    # the primary service
    clusterName:  *ts_host

    # :NOTE: This REQUIRES that the node label 'cmdaa/timescaledb' be added to
    #        any node that is permitted to run timescaledb:
    #           kubectl -n {{ .Release.Namespace }} \
    #             label nodes %node% 'cmdaa/timescaledb=true'
    nodeSelector: # {
      cmdaa/timescaledb:  "true"
    # } infrastructure.timescaledb.nodeSelector

    service: # {
      primary: # {
        port: *ts_port
      # } infrastructure.timescaledb.service.primary
      replica: # {
        port: *ts_port
      # } infrastructure.timescaledb.service.replica
    # } infrastructure.timescaledb.service

    secrets: # {
      certificateSecretName:  *ts_secret_cert

      # :XXX: Use the empty secret to ensure the secret-based environment
      #       variables below are not overridden.
      credentialsSecretName:  *ts_secret_empty
      pgbackrestSecretName:   *ts_secret_empty
    # } infrastructure.timescaledb.secrets

    persistentVolumes: # {
      data: # {
        storageClass: *fast_storage
        #size:         150Gi
      # } infrastructure.timescaledb.persistenVolumes.data

      wal: # {
        storageClass: *fast_storage
        #size:         20Gi
      # } infrastructure.timescaledb.persistenVolumes.wal
    # } infrastructure.timescaledb.persistenVolumes

    env: # {
      - name:   TSTUNE_PROFILE
        value:  promscale

      ###
      # PATRONI user information (instead of a full secret) {
      - name:   PATRONI_SUPERUSER_USERNAME
        value:  *ts_user_super
      - name:   PATRONI_SUPERUSER_PASSWORD
        valueFrom:
          secretKeyRef:
            name: *ts_secret_creds
            key:  *ts_user_super

      - name:   PATRONI_REPLICATION_USERNAME
        value:  *ts_user_replication
      - name:   PATRONI_REPLICATION_PASSWORD
        valueFrom:
          secretKeyRef:
            name: *ts_secret_creds
            key:  *ts_user_replication

      - name:   PATRONI_admin_USERNAME
        value:  *ts_user_admin
      - name:   PATRONI_admin_PASSWORD
        valueFrom:
          secretKeyRef:
            name: *ts_secret_creds
            key:  *ts_user_admin

      - name:   PATRONI_cmdaa_USERNAME
        value:  *ts_user_app
      - name:   PATRONI_cmdaa_PASSWORD
        valueFrom:
          secretKeyRef:
            name: *ts_secret_creds
            key:  *ts_user_app
      # PATRONI user information }
      ###
    # } infrastructure.timescaledb.env

    backup: # {
      enabled:  false # iff `global.install.minio`  *install_minio

      pgBackRest: # {
        repo1-s3-endpoint:  *s3_fullName
        repo1-s3-port:      *s3_port
        repo1-s3-bucket:    *s3_bucket_db
      # } infrastructure.timescaledb.backup.pgBackRest

      env: # {
        - name: PGBACKREST_REPO_S3_KEY
          valueFrom:
            secretKeyRef:
              name: *s3_secret
              key:  *s3_key_access

        - name: PGBACKREST_REPO_S3_KEY_SECRET
          valueFrom:
            secretKeyRef:
              name: *s3_secret
              key:  *s3_key_secret
      # } infrastructure.timescaledb.backup.env
    # } infrastructure.timescaledb.backup
  # } infrastructure.timescaledb

  promscale: # {
    enabled:  *install_timescaledb

    ##############################################
    # Accessible values {
    host: &promscale_fullName {{ .Release.Name }}-promscale
    port: &promscale_port     9201  # :XXX: MUST be an integer

    # Accessible values }
    ##############################################

    #default name is generated via {{ template "connector.fullname" }}
    # which checks for:
    #  fullnameOverride:  ps                  # => ps
    #  nameOverride:      ps                  # => cmdaa-ps
    #  defaulting to:     %Release%-promscale # => cmdaa-promscale
    fullnameOverride: *promscale_fullName

    # configuration options for the service exposed by promscale
    service: # {
      prometheus: # { port to receive prometheus remote_write data
        port:     *promscale_port
      # } infrastructure.promscale.service.prometheus
    # } infrastructure.promscale.service

    # :XXX: Reflection only
    endpoint: # {
      write:  &promscale_endpoint_write  "write"
      read:   &promscale_endpoint_read   "read"
    # } infrastructure.promscale.endpoint

    ###
    # Use extra environment variables to pass along database connection
    # information
    #
    # We pass the name of an empty secret to the Promscale chart so it won't
    # generate and use a secret that over-rides these environment settings.
    #
    connectionSecretName: *ts_secret_empty

    extraEnv: # {
      - name:   "PROMSCALE_DB_HOST"
        value:  *ts_host
      - name:   "PROMSCALE_DB_PORT"
        value:  *ts_port    # :XXX: MUST be a string
      # user to connect to TimescaleDB with
      #   :NOTE: This will be the user that owns all tables/views created
      #          by Promscale as it receives data via the remote-write
      #          endpoint. In order to access these tables/view you will be
      #          required to authenticate as this same user.
      #
      #          This user must also have permission to create new TimescaleDb
      #          tables/views.
      #
      #          For a database user to access Promscale data, that user must
      #          be a member of the 'prom_reader' and/or 'prom_writer' group:
      #             GRANT prom_reader TO cmdaa;
      #
      - name:   "PROMSCALE_DB_USER"
        value:  *ts_user_super

      - name:   "PROMSCALE_DB_PASSWORD"
        valueFrom:
          secretKeyRef:
            name: *ts_secret_creds
            key:  *ts_user_super

      - name:   "PROMSCALE_DB_NAME"
        value:  *ts_name_collections

      - name: "PROMSCALE_DB_SSL_MODE"
        value:  "require"
    # } infrastructure.promscale.extraEnv
  # } infrastructure.promscale

  kafka_promscale: # {
    async_source: # {
      #
      # Specify the set of kafka topics that should be monitored for metrics to
      # be ingested into Promscale.
      #
      topics: # {
        - &kafka_application_topic  metrics_application # flink
        - metrics_cgroup                                # kernel_beat/procfs
        - metrics_device                                # sg-logs
        - metrics_filesystem                            # kernel_beat/procfs
        - metrics_network                               # kernel_beat/procfs
        - metrics_process                               # kernel_beat/procfs
        - metrics_system                                # kernel_beat/procfs
        - &kafka_adapter_topic      metrics_prometheus  # prometheus-kafka
      # } infrastructure.kafka_promscale.async_source.topics
    # } infrastructure.kafka_promscale.async_source
  # } infrastructure.kafka_promscale

  elasticsearch: # {
    ###
    # :XXX: If `global.kibanaEnabled` is true, these settings are over-ridden
    #       by `global.elasticsearch.service`
    fullnameOverride: *elastic_fullName

    service: # {
      ports: # {
        restAPI:  *elastic_port
      # } infrastructure.elasticsearch.service.ports
    # } infrastructure.elasticsearch.service
    ###

    kibana: # {
      # Enabled/Disabled via `global.kibanaEnabled`
      fullnameOverride: &kibana_fullName  {{ .Release.Name }}-kibana

      service: # {
        port:           &kibana_port      5601
      # } infrastructure.elasticsearch.kibana.service

      ingress: # {
        ensabled: false # Do this in a top-level template
      # } infrastructure.elasticsearch.kibana.ingress
    # } infrastructure.elasticsearch.kibana
  # } infrastructure.elasticsearch
# } infrastructure
##############################################################################
analytics: # { log and metric analytics
  enabled:  *install_analytics

  ingress: # {
    enabled:  false

    ###
    # :XXX: Regardless of `ingress.enabled`, this is used to establish the
    #       proper DN for the efk ingress via
    #         files/argo-workflow/efk-start.yaml => 15_sensor-workflows.yml
    #
    dn:       *ingress_dn
  # } analytics.ingress
  ############################################
  # Infrastructure conntections {
  mongo: # {
    server:     *mongo_fullName
    port:       *mongo_port

    database:   *mongo_db
    dbUsername: *mongo_dbUser

    secrets: # {
      secret:       *mongo_secret
      passwordKey:  *mongo_key_password
    # } analytics.mongo.secrets
  # } analytics.mongo

  minio: # {
    server: *s3_fullName
    port:   *s3_port

    bucket: # {
      service:  *s3_bucket_service
      workflow: *s3_bucket_workflow
      flink:    *s3_bucket_flink
    # } analytics.minio.bucket

    secret: # {
      name:       *s3_secret
      accesskey:  *s3_key_access
      secretkey:  *s3_key_secret
    # } analytics.minio.secret
  # } analytics.minio

  kafka: # { fluentd kafka target
    server: *kafka_host
    port:   *kafka_port

    topics: #{
      grokfailures: &kafka_grokfailures_topic   grokfailures
      fluentd:      &kafka_fluentd_topic        fluentd
      metrics:      *kafka_application_topic
    # } analytics.kafka.topics
  # } analytics.kafka

  kafkaEsSync: # {
    topics: *kafka_fluentd_topic

    elastic: # {
      url:  *elastic_fullName
      port: *elastic_port
    # } analytics.kafkaEsSync.elastic
  # } analytics.kafkaEsSync

  ###
  # :XXX: What are these supposed to reference?
  #argo: # {
  #  serviceaccount: argo-events-sa
  #  service: # {
  #    name: cmdaa-workflows-eventsource-svc
  #    port: 12000
  #  # } argo.service
  ## } analytics.argo

  # Infrastructure conntections }
  ############################################
  flink: #{
    #job: #{
      #deployment: #{
        #grokfailures: #{
          #lowerBoundInterval:       60 # minutes
          #upperBoundInterval:       1  # minutes
          #partFileRolloverInterval: 1 # minutes
          #partFileMaxSize:          1mb
        # } analytics.flink.job.deployment.grokfailures

        #chilogger: #{
          #qvalue: '.95'
        # } analytics.flink.job.deployment.chilogger
      # } analytics.flink.job.deployment
    # } analytics.flink.job

    taskManager: #{
      #deployment: #{
        #replicas: 2
      # } analytics.flink.taskManager.deployment

      service: # {
        name: {{ .Release.Name }}-flink-taskmanager
      # } analytics.flink.taskManager.service
    # } analytics.flink.taskManager

    jobManager: #{
      #deployment: #{
        #replicas: 1
      # } analytics.flink.jobManager.deployment

      service: #{
        name: &analytics_flink_job_fullName {{ .Release.Name }}-flink-jobmanager
      # } analytics.flink.jobManager.service

      web: #{
        #name: web
        port: &analytics_flink_job_web_port  8081
      # } analytics.flink.jobManager.web
    # } analytics.flink.jobManager
  # } analytics.flink

  grokstore: # {
    service: # {
      name: &analytics_gs_fullName {{ .Release.Name }}-grokstore
      port: &analytics_gs_port     80
    # } analytics.grokstore.service
  # } analytics.grokstore

  ui: # {
    service: # {
      name: &analytics_ui_fullName  {{ .Release.Name }}-ui
      port: &analytics_ui_port      80
    # } analytics.ui.service
  # } analytics.ui

  fluentdGen: # {
    service: # {
      name: &analytics_fdgen_fullName  {{ .Release.Name }}-fdgen
      port: &analytics_fdgen_port      8080
    # } analytics.fluentdGen.service

    bareMetal: # {
      # :XXX: I don't think `brokers` here is correct (since there is no port)
      brokers:      *kafka_host

      defaultTopic: *kafka_fluentd_topic
    # } analytics.fluentdGen.bareMetal
  # } analytics.fluentdGen

  #########################################
  #resources: # {
    #loglikeli: # {
      #jvm: # {
        #min: 7g
        #max: 31g
      # } analytics.resources.loglikeli.jvm
      #memoryRequest: # {
        #min: 31000Mi
        #max: 62000Mi
      # } analytics.resources.loglikeli.memoryRequest
      #cpuRequest: # {
        #min: 2000m
        #max: 14000m
      # } analytics.resources.loglikeli.cpuRequest
    # } analytics.resources.loglikeli
    #s3out: # {
      #jvm: # {
        #min: 7g
        #max: 31g
      # } analytics.resources.s3out.jvm
      #memoryRequest: # {
        #min: 31000Mi
        #max: 62000Mi
      # } analytics.resources.s3out.memoryRequest
      #cpuRequest: # {
        #min: 2000m
        #max: 14000m
      # } analytics.resources.s3out.cpuRequest
    # } analytics.resources.s3out
    #mongoout: # {
      #jvm: # {
        #min: 7g
        #max: 31g
      # } analytics.resources.mongoout.jvm
      #memoryRequest: # {
        #min: 31000Mi
        #max: 62000Mi
      # } analytics.resources.mongoout.memoryRequest
      #cpuRequest: # {
        #min: 2000m
        #max: 14000m
      # } analytics.resources.mongoout.cpuRequest
    # } analytics.resources.mongoout
    #elastic: # {
      #memoryRequest: # {
        #min: 2000Mi
        #max: 31000Mi
      # } analytics.resources.elastic.memoryRequest
      #cpuRequest: # {
        #min: 10000m
        #max: 16000m
      # } analytics.resources.elastic.cpuRequest
    # } analytics.resources.elastic
  # } analytics.resources
# } analytics
##############################################################################
collections: # {  iff `global.install.collections`
  enabled:  *install_collections

  bridge: # {
    kafka: # {
      host: *kafka_host
      port: *kafka_port
    # } collections.bridge.kafka

    prometheus: # {
      service: # {
        host: {{ .Release.Name }}-pk-bridge
      # } collections.bridge.prometheus.service
    # } collections.bridge.prometheus
  # } collections.bridge

  prometheus-kafka-adapter: # {
    fullnameOverride: &kafka_adapter_fullName {{ .Release.Name }}-pk-adapter

    service: # {
      port: &kafka_adapter_port 80
    # } collections.prometheus-kafka-adapter.service

    environment: # {
      KAFKA_HOST:   *kafka_host
      KAFKA_PORT:   *kafka_port
      KAFKA_TOPIC:  *kafka_adapter_topic
    # } collections.prometheus-kafka-adapter.environment

    # :XXX: Reflection only, the actual endpoint is hardcoded in the adapter
    #       source.
    endpoint:   &kafka_adapter_endpoint  "receive"
  # } collections.prometheus-kafka-adapter

  prometheus: # {
    #kube-state-metrics: #{
      # :NOTE: If this is set to 0, it will REQUIRE that the deployment be
      #        scaled up to 1 for any collection to be performed. This would
      #        also apply to any helm upgrade.
      #replicas: 1
    # } collections.prometheus.kube-state-metrics

    nodeExporter: # {
      # :NOTE: This REQUIRES that the node label 'cmdaa.collect/node-seporter'
      #        be added to any node on which the prometheus node-exporter
      #        should run:
      #           kubectl -n {{ .Release.Namespace }} \
      #             label nodes %node% 'cmdaa.collect/node-exporter=true'
      nodeSelector: # {
        cmdaa.collect/node-exporter:  "true"
      # } collections.prometheus.nodeExporter.nodeSelector
    # } collections.prometheus.nodeExporter

    server: # {
      remoteWrite: # {
        # kafkaRemote {
        - enabled:  true
          host:     *kafka_adapter_fullName
          port:     *kafka_adapter_port
          endpoint: *kafka_adapter_endpoint
          # complete url = {protocol}://{host}:{port}/{endpoint}

          # write queue config: https://prometheus.io/docs/practices/remote_write/
          queue: # {
            max_shards: 30
          # } prometheus.server.remoteWrite[kafkaRemote].queue
        # prometheus.server.remoteWrite[kafkaRemote] }
      # } collections.prometheus.server.remoteWrite
    # } collections.prometheus.server
  # } collections.prometheus

  ################################################
  # Collectors {
  kernel_beat: # {
    # :NOTE: This REQUIRES that the node label 'cmdaa.collect/kernelbeat'
    #        be added to any node on which the kernelbeat metrics collector
    #        should run:
    #           kubectl -n {{ .Release.Namespace }} \
    #             label nodes %node% 'cmdaa.collect/kernelbeat=true'
    nodeSelector: # {
      cmdaa.collect/kernelbeat: "true"
    # } collections.kernel_beat.nodeSelector

    init: # {
      repo: # {
        host:     *s3_fullName
        port:     *s3_port
        bucket:   *s3_bucket_kb

        secret: # {
          name:     *s3_secret

          keys: # {
            access: *s3_key_access
            secret: *s3_key_secret
          # } collections.kernel_beat.init.repo.secret.keys
        # } collections.kernel_beat.init.repo.secret
      # } collections.kernel_beat.init.repo
    # } collections.kernel_beat.init
  # } collections.kernel_beat

  sglogs: # {
    # :NOTE: This REQUIRES that the node label 'cmdaa.collect/sglogs' be added
    #        to any node on which the sg-logs metrics collector should run:
    #           kubectl -n {{ .Release.Namespace }} \
    #             label nodes %node% 'cmdaa.collect/sglogs=true'
    nodeSelector: # {
      cmdaa.collect/sglogs: "true"
    # } collections.sglogs.nodeSelector

    # The list of extensions that should be loaded when sg-logs is run
    extensions: []
  # } collections.sglogs

  procfs_collector: # {
    # :NOTE: This REQUIRES that the node label 'cmdaa.collect/procfs'
    #        be added to any node on which the procfs metrics collector
    #        should run:
    #           kubectl -n {{ .Release.Namespace }} \
    #             label nodes %node% 'cmdaa.collect/procfs=true'
    nodeSelector: # {
      cmdaa.collect/procfs: "true"
    # } collections.procfs_collector.nodeSelector
  # } collections.procfs_collector

  procmon_collector: # {
    # :NOTE: This REQUIRES that the node label 'cmdaa.collect/procmon'
    #        be added to any node on which the procmon metrics collector
    #        should run:
    #           kubectl -n {{ .Release.Namespace }} \
    #             label nodes %node% 'cmdaa.collect/procmon=true'
    nodeSelector: # {
      cmdaa.collect/procmon: "true"
    # } collections.procmon_collector.nodeSelector
  # } collections.procmon_collector
  # Collectors }
  ################################################
# } collections
##############################################################################
grafana: # { iff `global.install.grafana`
  enabled:  *install_grafana

  # default name is generated via grafana template helper
  # which checks for:
  #  fullnameOverride:  grafana           # => grafana
  #  nameOverride:      graph             # => cmdaa-graph
  #  defaulting to:     %Release%-grafana # => cmdaa-grafana
  fullnameOverride: &grafana_fullName {{ .Release.Name }}-grafana

  service: # {
    port: &grafana_port 3000
  # } grafana.service

  sidecar: # {
    datasources: #{
      enabled:  true

      label:            grafana_datasource
      #searchNamespace:  null
    # } grafana.sidecar.datasources

    dashboards: #{
      enabled:  true

      label:            grafana_dashboard
      #searchNamespace:  null
    # } grafana.sidecar.dashboards
  # } grafana.sidecar

  #extraSecretMounts: # {
  #  - name:         db-creds
  #    # volumeMount {
  #    mountPath:    /var/run/secrets/db/password
  #    readOnly:     true
  #    subPath:      *ts_user_grafana
  #    # volumeMount }
  #
  #    # volumes {
  #    secretName:   *ts_secret_creds
  #    defaultMode:  0444
  #    # volumes }
  ## } grafana.extraSecretMounts

  config: # {  configuration metadata
    # Stored in a secret with this name
    secretName: &grafana_config_secret  {{ .Release.Name }}-grafana-config

    database: # { grafana config database
      enabled:  true

      host:     *ts_host            # -+-- GF_DATABASE_HOST
      port:     *ts_port            # -+

      dbName:   *ts_name_grafana    # GF_DATABASE_NAME

      user:     *ts_user_grafana    # GF_DATABASE_USER
      #pass:     via grafana.envValueFrom.GF_DATABASE_PASSWORD

      sslMode:  require
    # } grafana.config.database

    datasource: # { grafana datasource(s)
      timescale: # {  timescaledb metrics collection database
        enabled:  true

        host:     *ts_host              # -+-- DS_TIMESCALE_HOST
        port:     *ts_port              # -+

        dbName:   *ts_name_collections  # DS_TIMESCALE_NAME

        user:     *ts_user_app          # DS_TIMESCALE_USER
        #pass:     via grafana.envValueFrom.DS_TIMESCALE_PASSWORD

        sslMode:  require
      # } grafana.config.datasource.timescale

      promscale: # { promscale access to metrics collection database
        enabled:  true

        proto:    http                  # -+
        host:     *promscale_fullName   # -+-- DS_PROMSCALE_URL
        port:     *promscale_port       # -+
      # } grafana.config.datasource.promscale
    # } grafana.config.datasource
  # } grafana.config

  ###
  # Establish config.ini overrides:
  #   Override grafana config database settings:
  #     GF_DATABASE_TYPE
  #     GF_DATABASE_HOST      (from config.database.host/port)
  #     GF_DATABASE_NAME
  #     GF_DATABASE_USER
  #     GF_DATABASE_SSL_MODE
  #
  #   Additional variables used in `templates/secret-grafana-datasources.yaml`
  #     DS_TIMESCALE_HOST     (from config.datasource.timescale.host/port)
  #     DS_TIMESCALE_NAME
  #     DS_TIMESCALE_USER
  #     DS_TIMESCALE_SSL_MODE
  #
  #     DS_PROMSCALE_URL      (from config.datasource.promscale.host/port)
  #
  envFromSecret:  *grafana_config_secret

  # From other secrets:
  #   GF_DATABASE_PASSWORD  (for GF_DATABASE_USER)
  #   DS_TIMESCALE_PASSWORD (for DS_TIMESCALE_USER)
  #
  envValueFrom: # {
    #configMapKeyRef:
    #  name: game-demo           # The ConfigMap this value comes from.
    #  key: player_initial_lives # The key to fetch.

    # :XXX: MUST be the password for the user specified in
    #         `grafana.config.database.user`
    #
    GF_DATABASE_PASSWORD: # {
      secretKeyRef: # {
        name: *ts_secret_creds
        key:  *ts_user_grafana
      # } grafana.envValueFrom.secretKeyRef
    # } grafana.envValueFrom.GF_DATABASE_PASSWORD

    # :XXX: MUST be the password for the user specified in
    #         `grafana.config.datasource.timescale.user`
    #
    DS_TIMESCALE_PASSWORD: # {
      secretKeyRef: # {
        name: *ts_secret_creds
        key:  *ts_user_app
      # } grafana.envValueFrom.secretKeyRef
    # } grafana.envValueFrom.DS_TIMESCALE_PASSWORD
  # } grafana.envValueFrom

  grafana.ini: # {
    live: #{
      # Specifying 'allowed_origins' to match the domain used in the ingress
      # rules resolves an issue with grafana filling its logs with
      # "Request Origin is not authorized" warnings.
      #
      # :XXX: Keys are (re)sorted for this/each section and MUST be defined
      #       BEFORE use, hence the use of '_domain' to ensure it is sorted
      #       before use.
      _domain:          *ingress_dn
      allowed_origins:  "https://*.%(_domain)s"
    # } grafana[grafana.ini].live

    analytics: # {
      # Turn off usage statistics and update checks
      reporting_enabled:  false
      check_for_updates:  false
    # } grafana[grafana.ini].analytics

  # } grafana[grafana.ini]

# } grafana
##############################################################################
# Ingress settings
#
ingress: # {
  enabled:  true

  dn:       *ingress_dn   # DNS domain name for ingress

  annotations:  # {
    kubernetes.io/ingress.class:  nginx

    ###
    # Additional nginx ingress adjustments are better handled via
    # `ingress-nginx.controller` config map which would globally apply to any
    # generated nginx ingress.
    #
    #   https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/configmap/
    #
    #nginx.ingress.kubernetes.io/proxy-connect-timeout: 30s
    #nginx.ingress.kubernetes.io/proxy-read-timeout:    20s
    #nginx.ingress.kubernetes.io/proxy-body-size:       50m

    ###
    # :XXX: To enable tls and automatic certificates, there must be a cert
    #       issuer that responds to an annotation that is applied to an
    #       ingress.
    #
    #       Examples:
    #         certmanager.k8s.io/issuer:      cmdaa-ca-issuer
    #
    #         cert-manager.io/cluster-issuer: letsencrypt-staging
    #
  # } ingress.annotations

  # Define ingress hosts along with their related service names/ports
  entries: # {
    ###############################################################
    # Umbrella {
    #
    - host:     grafana   # {
      enabled:  *install_grafana  # ingress rule
      tls:      true
      path:     /

      service: # {
        fullName: *grafana_fullName
        port:     *grafana_port
      # } ingress.entries[grafana].service
    # } ingress.entries[grafana]

    #
    # Umbrella }
    ###############################################################
    # Infrastructure {
    #
    - host:     minio   # {
      enabled:  *install_minio  # ingress rule
      tls:      true
      path:     /

      service: # {
        fullName: *s3_fullName
        port:     *s3_port
      # } ingress.entries[minio].service
    # } ingress.entries[minio].minio

    - host:     argo-ui   # {
      enabled:  *install_argo   # ingress rule
      tls:      true
      path:     /

      service: # {
        fullName: *argo_fullName
        port:     *argo_port
      # } ingress.entries[argo-ui].service
    # } ingress.entries[argo-ui]

    - host:     kibana  # {
      enabled:  *install_elastic    # ingress rule
      tls:      true
      path:     /

      service: # {
        fullName: *kibana_fullName
        port:     *kibana_port
      # } ingress.entries[kibana].service
    # } ingress.entries[kibana]

    #
    # Infrastructure }
    ###############################################################
    # CMDAA Analytics {
    #
    - host:     grokstore  # {
      enabled:  *install_analytics  # ingress rule
      tls:      true
      path:     /

      service: # {
        fullName: *analytics_gs_fullName
        port:     *analytics_gs_port
      # } ingress.entries[grokstore].service
    # } ingress.entries[grokstore]

    - host:     cmdaa-ui  # {
      enabled:  *install_analytics  # ingress rule
      tls:      true
      path:     /

      service: # {
        fullName: *analytics_ui_fullName
        port:     *analytics_ui_port
      # } ingress.entries[cmdaa-ui].service
    # } ingress.entries[cmdaa-ui]

    - host:     flink  # {
      enabled:  *install_analytics  # ingress rule
      tls:      true
      path:     /

      service: # {
        fullName: *analytics_flink_job_fullName
        port:     *analytics_flink_job_web_port
      # } ingress.entries[flink].service
    # } ingress.entries[flink]

    # CMDAA Analytics }
    ###############################################################
  # } ingress.entries

# } ingress
