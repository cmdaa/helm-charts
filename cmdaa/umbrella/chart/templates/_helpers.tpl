##############################################################################
# Common labels
#
#   Usage:
#     labels: \{\{- include "cmdaa.labels" . | nindent 4 \}\}
#
{{- define "cmdaa.labels" -}}
app:                          {{ .Release.Name }}
release:                      {{ .Release.Name }}
heritage:                     {{ .Release.Service }}
chart:                        {{ .Chart.Name }}
app.kubernetes.io/instance:   {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart:                {{ .Chart.Name }}
{{ end -}}

##############################################################################
# Return the Promscale service (name:port)
#
#   Usage:
#     service: \{\{- include "cmdaa.promscale.service" . \}\}
#
#   NOTE:
#     We use `tobs.promscale.fullnameOverride` instead of
#            `tobs.promscale.connector`
#     since connector is a reflection-only value.
#
#     We COULD use or mirror the tobs.promscale connector.fullname template...
#
{{- define "cmdaa.promscale.service" -}}
{{-  $name := .Values.tobs.promscale.fullnameOverride -}}
{{-  $port := .Values.tobs.promscale.service.port -}}
{{-  $host := printf "%s-connector" $name | trunc 63 | trimSuffix "-" -}}
{{ $host }}:{{ $port }}
{{- end -}}

##############################################################################
# Return the URL of the prometheus remote_write endpoint service
#
#   Usage:
#     url: \{\{- include "cmdaa.prometheus.remote_write.url" . \}\}
#
{{- define "cmdaa.prometheus.remote_write.url" -}}
{{-  $service := (include "cmdaa.promscale.service" .) -}}
prometheus://{{ $service }}/write
{{- end -}}

##############################################################################
# Return the URL for kafka
#
#   Usage:
#     url: \{\{- include "cmdaa.kafka.url" . \}\}
#
{{- define "cmdaa.kafka.url" -}}
{{-  $host := .Values.kafka.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{-  $port := .Values.kafka.service.port -}}
kafka://{{ $host }}:{{ $port }}/
{{- end -}}

##############################################################################
# Return the full name of a secret from an `ingress.entries` item
#
#   Usage:
#     \{\{- $root := . \}\}
#     \{\{- range .Values.ingress.entries \}\}
#     ...
#       name: \{\{- include "cmdaa.ingress.secretName"  (list $root .) \}\}
#     \{\{- end -\}\}
#
{{- define "cmdaa.ingress.secretName" -}}
{{-  $root  := index . 0 -}}
{{-  $entry := index . 1 -}}
{{-  printf "cert-%s" $entry.host -}}
{{- end -}}

##############################################################################
# Return the fully qualified name of an ingress
#
#   Usage:
#     \{\{- $root := . \}\}
#     \{\{- range .Values.ingress.entries \}\}
#     ...
#       name: \{\{- include "cmdaa.ingress.serviceName"  (list $root .) \}\}
#     \{\{- end -\}\}
#
{{- define "cmdaa.ingress.serviceName" -}}
{{-  $root  := index . 0 -}}
{{-  $entry := index . 1 -}}
{{-  $rel   := $root.Release.Name -}}
{{-  if $entry.service.fullName -}}
{{    $entry.service.fullName | trunc 63 | trimSuffix "-" -}}
{{-  else -}}
{{    printf "%s-%s" $rel $entry.service.name | trunc 63 | trimSuffix "-" -}}
{{-  end -}}
{{- end -}}

##############################################################################
# Ingress annotations
#
#   Usage:
#     annotations: \{\{- include "cmdaa.ingress.annotations" . | nindent 4 \}\}
#
{{- define "cmdaa.ingress.annotations" -}}
{{-  range $key, $value := .Values.ingress.annotations -}}
{{    $key }}: {{ $value | quote }}
{{   end -}}
{{- end -}}

##############################################################################
# Return the apiVersion for ingress
#
# :XXX: For some reason helm is unhappy with what SHOULD BE a standard
#       interface: .Capabilities ...
#
#   Usage:
#     apiVersion: \{\{- include "cmdaa.ingress.apiVersion" . \}\}
#
#   if semverCompare ">=1.14-0" .Capabilities.KubeVersion.GitVersion
#   if semverCompare ">=1.14-0" .Capabilities.KubeVersion.Version
#   if .Capabilities.APIVersions.Has "networking.k8s.io/v1beta1"
#
{{- define "cmdaa.ingress.apiVersion" -}}
{{-  if true -}}
{{-   print "networking.k8s.io/v1beta1" -}}
{{-  else -}}
{{-   print "apps/v1beta1" -}}
{{-  end -}}
{{- end -}}
