#!/bin/bash
SELF="$(realpath "$0")"
SELF_DIR="$( dirname  "$SELF")"

TARGET_DIR="${1:-${SELF_DIR}}"

function log() {
  echo "$(date '+%Y-%m-%d %H:%M:%S') - initdb - $1"
}

function terminate() {
  RC=${1:-1}

  ## DEBUG -- keep alive {
  #log "terminate( $RC ): keep alive ..."
  #while [ 1 ]; do
  #  sleep 60 &
  #  wait $!
  #done
  ## DEBUG -- keep alive }

  log "Stopping [ $RC ]"
  exit $RC
}

function apply_scripts() {
  DIR="$@"

  if [ ! -d "$DIR" ]; then
    log "ERROR: Not a directory [ $DIR ]"
    terminate
  fi

  log "Apply scripts from $DIR"

  # Iterate over all files in the directory holding this script
  for file in $(find "$DIR" -type f | sort); do
    [[ "$file" == "$SELF" || ! -s "$file" ]] && continue
    case "$file" in
      *.sh)
        if [ -x "$file" ]; then
          log "Call script [ $file ]"
          "$file" $@
          RC=$?
        else
          log "Source script [ $file ]"
          . "$file"
          RC=$?
        fi
        ;;
      *.sql)
        log "Apply sql [ $file ]"
        # If initializing we would need to disable synchronous_commit
        #PGOPTIONS="-c synchronous_commit=local" psql -f "$file"
        psql -f "$file"
        RC=$?
        ;;
      *.sql.gz)
        log "Decompress and apply sql [ $file ]"
        # If initializing we would need to disable synchronous_commit
        #gunzip -c "$file" | PGOPTIONS="-c synchronous_commit=local" psql
        gunzip -c "$file" | psql
        RC=$?
        ;;
      *)
        log "Ignore unknown file type [ $file ]"
        RC=0
        ;;
    esac

    if [ "$RC" != "0" ]; then
      log "ERROR: $file exited with $RC"

      terminate $RC
    fi
  done
}

# If we don't catch these signals, and we're still waiting for PostgreSQL to be
# ready, we will not respond at all to a regular shutdown request, therefore,
# we explicitly terminate if we receive these signals.
trap terminate SIGTERM SIGQUIT

log "TARGET_DIR[ $TARGET_DIR ]"

###################################################
# Wait for the database to become ready
#
while ! pg_isready -q; do
  log "Waiting for Timescale to become available"
  # Interruptable sleep
  sleep 3 &
  wait $!
done
log "Timescale ready"

###
# Iterate over the target directory and for each sub-directory that is not
# "promscale.d", apply all contained scripts
#
for entry in "$TARGET_DIR/"*; do
  base_entry="$(basename "$entry")"
  if [ ! -d "$entry" -o $base_entry = 'promscale.d' ]; then
    continue
  fi

  # Apply timescale init scripts
  apply_scripts "$entry"
done

if [ -d "$TARGET_DIR/promscale.d" ]; then
  ###################################################
  # Wait for Promscale to become ready
  #
  while ! nc -w 3 -z $PROMSCALE; do
    log "Waiting for Promscale to become available"
    # Interruptable sleep
    sleep 2 &
    wait $!
  done
  log "Promscale ready"

  apply_scripts "$TARGET_DIR/promscale.d/"
fi

terminate 0
