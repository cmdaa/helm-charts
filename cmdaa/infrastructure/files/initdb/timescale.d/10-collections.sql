{{- $db    := .Values.tsMeta.dbNames.collections -}}
{{- $user  := .Values.tsMeta.users.app.name -}}

{{- if not ( eq $user "postgres" ) }}
DO $$
  BEGIN
    CREATE ROLE {{ $user }};
  EXCEPTION WHEN duplicate_object THEN
    RAISE NOTICE 'role {{ $user }} already exists, skipping create';
  END
$$;
{{- end }}

{{- if not ( eq $db "postgres" ) }}
-- =========================================================================
-- Create the target collections database owned by the specified collections
-- user iff it doesn't already exist.
SELECT 'CREATE DATABASE {{ $db }} WITH OWNER={{ $user }}'
WHERE NOT EXISTS (SELECT FROM pg_database WHERE datname = '{{ $db }}')\gexec

{{- end }}

-- For the case where the database already existed,
-- explicitly grant access to the database user.
GRANT ALL ON DATABASE {{ $db }} TO {{ $user }};

--
-- :NOTE: For this user to access the Prometheus metrics, it will need
--        to be given the 'prom_reader' role which is created by
--
--        To update the search_path for a role:
--          ALTER ROLE {{ $user }}
--            SET search_path = "$user",public,...;
--
--        It will also need to have its search_path updated to include:
--          - prom_metric     access to metric views
--          - [prom_api]      API-releated helpers
--          - [_prom_catalog] Catalog meta-data to identify label, series,
--                            and metric names
--
-- Create the 'prom_reader' role here and update the search_path to
-- allow access to metrics.
--
DO $$
  BEGIN
    CREATE ROLE prom_reader;
  EXCEPTION WHEN duplicate_object THEN
    RAISE NOTICE 'role prom_reader already exists, skipping create';
  END
$$;

GRANT prom_reader TO {{ $user }};

{{- if not ( eq $user "postgres" ) }}
ALTER ROLE {{ $user }}
 SET search_path = "$user",public,prom_api,prom_metric;
{{- end }}
