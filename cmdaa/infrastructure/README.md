Here you will find a helm chart that can deploy within an existing kubernetes
cluster the infrastructure and CMDAA components required for a fully functional
CMDAA.

Infrastructure:
- [nginx-ingress](https://github.com/kubernetes/ingress-nginx) v1.3.0
  (chart 4.2.0, if needed);
- [minio](https://github.com/minio/minio) RELEASE.2021-02-14T04-01-33Z
  (chart 8.0.10);
- [mongodb](https://github.com/mongodb/mongo) v4.2.4 (chart 7.8.10);
- [kafka](https://github.com/apache/kafka) v3.3.1 (chart 19.1.4)<br />
  *NOTE:* kafka will only be run on nodes with the label `cmdaa/kafka=true`
  (specified via `kafka.nodeSelector` in `values.yaml`). To label nodes, you
  may use the `kubectl` helper (to ensure the proper kubectl context is used):
    ```
    # Retrieve the set of nodes
    ./etc/kubectl get nodes

    # For each node that should be used for kafka:
    ./etc/kubectl label nodes %node% 'cmdaa/kafka=true'
    ```
- [Timescaledb](https://docs.timescale.com/) (chart 0.22.0-cmdaa)<br />
  *NOTE:* timescaledb will only be run on nodes with the label
  `cmdaa/timescaledb=true`.  (specified via `timescaledb.nodeSelector` in
  `values.yaml`). To label nodes, you may use the `kubectl` helper (to ensure
  the proper kubectl context is used):
    ```
    # Retrieve the set of nodes
    ./etc/kubectl get nodes

    # For each node that should be used for timescaledb:
    ./etc/kubectl label nodes %node% 'cmdaa/timescaledb=true'
    ```
- [Promscale](https://github.com/timescale/tobs) v0.16.0 (chart 14.4.2-cmdaa)
  &mdash; provides a Prometheus-complient read/write endpoint to Timescaledb;
- [argo-workflows](https://github.com/argoproj/argo-workflows) v3.3.8
  (chart 0.16.7-cmdaa);
- [argo-events](https://github.com/argoproj/argo-events) v1.3.1 (chart 1.6.1);
- [ElasticSearch](https://www.elastic.co/products/elasticsearch) v8.5.2
  (chart 19.5.4-cmdaa);
  *NOTE:* ElasticSearch will only be run on nodes with the label
  `cmdaa/elastic=true`.  (specified via
  `elasticsearch.master.nodeSelector` in `values.yaml`). To label nodes, you
  may use the `kubectl` helper (to ensure the proper kubectl context is used):
    ```
    # Retrieve the set of nodes
    ./etc/kubectl get nodes

    # For each node that should be used for ElasticSearch:
    ./etc/kubectl label nodes %node% 'cmdaa/elastic=true'
    ```
