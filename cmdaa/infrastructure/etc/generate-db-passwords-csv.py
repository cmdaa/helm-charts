#!/usr/bin/env python3
import sys
import argparse

from values_helper import read_values
from values_helper import get_dotted_path
from values_helper import base64_encode
from values_helper import gen_random_str
from values_helper import get_secret

def process_user( key, info, pass_len, secret ): #{
  name    = info.get('name',    key)
  patroni = info.get('patroni', '')

  # Password precedence:
  #   secret
  #     -> values.yaml
  #       -> generated
  b64_passwd = get_dotted_path( secret, ('data.%s' % name) )
  if b64_passwd is not None: #{
    print('>>>>> %-10s: existing secret-based password: %s' %
            (name, b64_passwd), file=sys.stderr)

  else: #}{
    # Look for a password in 'info'
    passwd = info.get('pass', None)
    if passwd is not None: #{
      # Base64 encode the password
      #b64_passwd = base64.b64encode( passwd.encode('ascii') ).encode('ascii')
      b64_passwd = base64_encode( passwd )

      print('>>>>> %-10s: base64   values-based password: %s' %
            (name, b64_passwd), file=sys.stderr)
    #}
  #}

  if b64_passwd is None: #{
    # Generate a random password
    passwd     = gen_random_str( pass_len )
    b64_passwd = base64_encode( passwd )

    print('>>>>> %-10s: random %4d character password: %s' %
            (name, pass_len, b64_passwd), file=sys.stderr)
  #}

  print('%s , %s , %s' %
          (patroni, name, b64_passwd))
#}

def main(): #{
  values = read_values()

  ##############################################################
  # Retrieve data from values.yaml {
  #
  # Determine the password length
  pass_len = get_dotted_path( values, 'secrets.length' )
  if pass_len is None: pass_len = 32

  # Secret name
  secret_name = get_dotted_path( values, 'db.secrets.creds' )
  if secret_name is None: #{
    print('*** Missing db.secrets.creds', file=sys.stderr)
    process.exit(-1)
  #}

  # Locate user records
  users = get_dotted_path( values, 'db.users' )
  if users is None: #{
    print('*** Missing db.users', file=sys.stderr)
    process.exit(-1)
  #}

  # Retrieve data from values.yaml }
  ##############################################################
  # Retrieve the content of any existing secret {
  #
  #secret = get_secret( 'missing.secret' )
  secret = get_secret( secret_name )

  # Retrieve the content of any existing secret }
  ##############################################################

  # Generate a single CSV line for each user
  #   patroni_name , user_name , base64-encoded-password
  print( '# patroni , user , b64_password' )
  for key, info in users.items(): #{
    process_user( key, info, pass_len, secret )
  #}
#}

###########################################################################
if __name__ == '__main__': #{
  main()
#}
