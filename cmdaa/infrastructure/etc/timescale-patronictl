#!/bin/bash
#
# Connect to a timescale pod an run a patronictl command
#
#   timescale-patronictl [-m]
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")
 REL="$( $DIR/helm-release )"

CLUSTER=$( $DIR/value-get.py timescaledb.clusterName )
if [[ ! -z "$CLUSTER" ]]; then
  LABELS="cluster-name=$CLUSTER"
else
  LABELS="app=${REL}-timescaledb"
fi

 PODS=( $($DIR/kubectl get pods --no-headers -l "${LABELS}" |
        awk '{print $1}') )
NPODS=${#PODS[@]}

if [[ $NPODS < 1 ]]; then
  echo "*** No POD found for '$LABELS'"
  exit
fi

# Auto or manual selection?
AUTO_SELECT=1
if [[ "$1" == "-m" ]]; then
  shift
  AUTO_SELECT=0
fi

if [[ $AUTO_SELECT == 0 && $NPODS > 1 ]]; then
  echo ">>> There are $NPODS matching Pods:"

  for (( num=1; num <= $NPODS; num++ )); do
    idex=$(( $num - 1 ))
    printf "%2d: %s\n" "$num" "${PODS[$idex]}"
  done

  echo -n ">>> Which would you like [1..$NPODS]? "
  read num
  idex=$(( $num - 1 ))

  POD=${PODS[$idex]}
else
  POD=${PODS[0]}
fi

date +'%Y-%m-%d %H:%M:%S-%Z'
echo ">>> @$POD : patronictl -c /etc/timescaledb/patroni.yaml $@"
$DIR/kubectl exec -it "$POD" -- patronictl -c /etc/timescaledb/patroni.yaml "$@"
