#!/bin/bash
#
# Cleanout argo-related resources that should NOT persist but aren't
# properly removed when the helm chart is uninstalled.
#
#   clean-argo [-f]
#
#     -f  Full clean, including argo-related CRDs
#
 ETC=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$ETC")

# sensor.argoproj.io      : sn
# eventsource.argoproj.io : es
# eventbus.argoproj.io    : eb
# workflows.argoproj.io   : wf
RES=( $($ETC/kubectl get -o=name sn,es,eb,wf) )
if [ ${#RES[@]} -gt 0 ]; then
  echo ">>> Clean out ${#RES[@]} argo resource(s) ..."
  for res in ${RES[@]}; do
    #
    # argo-related CRD-based resources will NOT actually be deleted when the
    # containing chart is uninstalled due to an issue with resource finalizers.
    #
    # Manually resetting the finalizer releases the resource for final/actual
    # deletion.
    #
    # Refs:
    #   https://github.com/argoproj/argo-events/issues/1056
    #   https://github.com/kubernetes/kubernetes/issues/60538#issuecomment-369099998
    #
    $ETC/kubectl patch $res -p '{"metadata":{"finalizers":[]}}' --type=merge
  done

  sleep 1
fi

RES=(
  $($ETC/kubectl get pods -l 'workflows.argoproj.io/workflow'  -o name)
  $($ETC/kubectl get wf   -l 'workflows.argoproj.io/completed' -o name)
)
if [ ${#RES[@]} -gt 0 ]; then
  echo ">>> Clean out ${#RES[@]} workflow-related pod(s) ..."
  $ETC/kubectl delete ${RES[@]}
fi

if [ "$1" == '-f' ]; then
  CRDS=( $($ETC/kubectl get -o=name crds | awk '/argo/') )
  if [ ${#CRDS[@]} -gt 0 ]; then
    echo ">>> Clean out ${#CRDS[@]} argo-related CRD(s) ..."
    $ETC/kubectl delete ${CRDS[@]}
  fi
fi
