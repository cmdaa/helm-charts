#!/bin/bash
#
# Add the node label to allow the timescaledb deployment to run on the given
# set of nodes.
#
#   infrastructure-labels-timescaledb-add [-s sleep] %node1% [ %node2% ... ]
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")
 REL="$( $DIR/helm-release )"

if [ "$1" == '-s' ]; then
  SLEEP="$2"
  shift; shift;
else
  SLEEP=0
fi

if [ $# -lt 1 ]; then
  cat <<EOF
***
*** Usage: $(basename "$0") [-s sleep] %node1% [ %node2% ... ]
***
EOF
  exit -1
fi

# Retrieve the label that controls the target daemonset
label="$($DIR/value-get.py timescaledb.nodeSelector | \
          sed "s/{'//;s/': '/=/;s/'}//")"
if [ -z "$label" ]; then
  echo "*** No timescaledb.nodeSelector found in values.yaml"
  exit
fi

nodes=$@
if [ ${#nodes[@]} -eq 1 -a ${nodes[0]} == 'all' ]; then
  nodes=( $($DIR/kubectl get nodes --no-headers | awk '{print $1}') )
fi

numNodes=${#nodes[@]}
numDone=0
for node in ${nodes[@]}; do
  echo ">>> Add '$label' to $node ..."
  $DIR/kubectl label nodes "$node" "$label"

  if [ $SLEEP -gt 0 ]; then
    numDone=$(( numDone + 1 ))
    if [ $numDone -lt $numNodes ]; then
      echo ">>> sleep $SLEEP seconds ..."
      sleep $SLEEP &
      wait $!
    fi
  fi
  echo "================================"
done
