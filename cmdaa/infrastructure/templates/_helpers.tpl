##############################################################################
# Common labels
#
#   Usage:
#     labels: \{\{- include "cmdaa.infrastructure.labels" . | nindent 4 \}\}
#
{{- define "cmdaa.infrastructure.labels" -}}
app:                          {{ .Release.Name }}
release:                      {{ .Release.Name }}
heritage:                     {{ .Release.Service }}
chart:                        {{ .Chart.Name }}
app.kubernetes.io/instance:   {{ .Release.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart:                {{ .Chart.Name }}
{{ end -}}

##############################################################################
# Return the Promscale service (name:port)
#
#   Usage:
#     service: \{\{- include "cmdaa.infrastructure.promscale.service" . \}\}
#
#   NOTE:
#     We use `promscale.fullnameOverride` instead of
#            `promscale.connector`
#     since connector is a reflection-only value.
#
#     We COULD use or mirror the promscale.fullname template...
#
{{- define "cmdaa.infrastructure.promscale.service" -}}
{{-  $name := .Values.promscale.fullnameOverride -}}
{{-  $port := .Values.promscale.service.prometheus.port -}}
{{-  $host := ( $name | trunc 63 | trimSuffix "-" ) -}}
{{ $host }}:{{ $port }}
{{- end -}}

##############################################################################
# Return the URL of the prometheus remote_write endpoint service
#
#   Usage:
#     url: \{\{- include "cmdaa.infrastructure.prometheus.remote_write.url" . \}\}
#
{{- define "cmdaa.infrastructure.prometheus.remote_write.url" -}}
{{-  $service := (include "cmdaa.infrastructure.promscale.service" .) -}}
prometheus://{{ $service }}/write
{{- end -}}

##############################################################################
# Return the URL for kafka
#
#   Usage:
#     url: \{\{- include "cmdaa.infrastructure.kafka.url" . \}\}
#
{{- define "cmdaa.infrastructure.kafka.url" -}}
{{-  $host := .Values.kafka.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{-  $port := .Values.kafka.service.port -}}
kafka://{{ $host }}:{{ $port }}/
{{- end -}}
