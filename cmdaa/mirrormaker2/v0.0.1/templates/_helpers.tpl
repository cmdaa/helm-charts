##############################################################################
# Generate the 'host:port' for the mirrormaker2 'source' or 'destination'
#
# Due to DNS naming limitations, we truncate the host to 63 characters and will
# trim any trailing '-'.
#
#   Usage:
#     url: \{\{- include "mirrormaker2.hostPort" target \}\}
#
#     Where 'target' is a dictionary with 'host' and 'port'
#
{{- define "mirrormaker2.hostPort" -}}
{{-  $host := .host | trunc 63 | trimSuffix "-" -}}
{{   $host }}:{{ .port }}
{{- end -}}

##############################################################################
# Expand the name of the chart.
#
#   Usage:
#     name: \{\{- include "mirrormaker2.name" . \}\}
#
{{- define "mirrormaker2.name" -}}
{{-  default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

##############################################################################
# Create a default fully qualified app name.
#
# We truncate at 63 chars because some Kubernetes name fields are limited to
# this (by the DNS naming spec).
#
# If release name contains chart name it will be used as a full name.
#
#   Usage:
#     name: \{\{- include "mirrormaker2.fullname" . \}\}
#
{{- define "mirrormaker2.fullname" -}}
{{-  if .Values.fullnameOverride }}
{{-   .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{-  else }}
{{-   $name := default .Chart.Name .Values.nameOverride }}
{{-   if contains $name .Release.Name }}
{{-    .Release.Name | trunc 63 | trimSuffix "-" }}
{{-   else }}
{{-    printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{-   end }}
{{-  end }}
{{- end }}

##############################################################################
# Create chart name and version as used by the chart label.
#
#   Usage:
#     name: \{\{- include "mirrormaker2.chart" . \}\}
#
{{- define "mirrormaker2.chart" -}}
{{-  printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

##############################################################################
# Common labels
#
#   Usage:
#     labels: \{\{- include "mirrormaker2.labels" . | nindent 2\}\}
#
#   Depends:
#     mirrormaker2.selectorLabels
#     mirrormaker2.name
#
{{- define "mirrormaker2.labels" -}}
helm.sh/chart:                {{ include "mirrormaker2.chart" . }}
{{   include "mirrormaker2.selectorLabels" . }}
{{-  if .Chart.AppVersion }}
app.kubernetes.io/version:    {{ .Chart.AppVersion | quote }}
{{-  end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

##############################################################################
# Selector labels
#
#   Usage:
#     labels: \{\{- include "mirrormaker2.selectorLabels" . | nindent 2\}\}
#
#   Depends:
#     mirrormaker2.name
#
{{- define "mirrormaker2.selectorLabels" -}}
app.kubernetes.io/name:       {{ include "mirrormaker2.name" . }}
app.kubernetes.io/instance:   {{ .Release.Name }}
{{- end }}
