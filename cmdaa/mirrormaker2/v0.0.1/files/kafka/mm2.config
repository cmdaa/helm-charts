# Define the clusters
clusters = SRC, DST

# Establish unidirectional mirroring of all topics from SRC/src to DST/dst
{{ with .Values.mirror -}}
SRC.bootstrap.servers = {{ include "mirrormaker2.hostPort" .source }}
DST.bootstrap.servers = {{ include "mirrormaker2.hostPort" .destination }}

SRC->DST.enabled = true
SRC->DST.topics  = {{ .topics }}

groupId = {{ .groupId }}

# :NOTE: Without these explicitly empty values, mirroring will not work {
replication.policy.separator  = 
source.cluster.alias          = 
target.cluster.alias          = 
# }
{{- end }}

################## Internal Topic Settings  #############################
# The replication factor for mm2 internal topics "heartbeats",
# "DST.checkpoints.internal" and "mm2-offset-syncs.DST.internal"
#
# For anything other than development testing, a value greater than 1 is
# recommended to ensure availability such as 3.
#
{{- with .Values.internalTopicReplication }}
checkpoints.topic.replication.factor  = {{ .checkpoints }}
heartbeats.topic.replication.factor   = {{ .heartbeats }}
offset-syncs.topic.replication.factor = {{ .offsetSyncs }}
{{- end }}

# The replication factor for connect internal topics
# "mm2-configs.DST.internal", "mm2-offsets.DST.internal" and
# "mm2-status.DST.internal"
#
# For anything other than development testing, a value greater than 1 is
# recommended to ensure availability such as 3.
#
{{- with .Values.internalTopicReplication }}
offset.storage.replication.factor = {{ .offset }}
status.storage.replication.factor = {{ .status }}
config.storage.replication.factor = {{ .config }}
{{- end }}

################## Additional Settings ##################################
{{- range $key,$val := .Values.additionalSettings }}
{{   $key }} = {{ $val }}
{{- end }}
