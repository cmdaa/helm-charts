This is a helm chart that can deploy kafka mirrormaker2 within an existing
kubernetes cluster. This allows kafka topics to be forwarded from on k8s/kafka
cluster to a second k8s/kafka cluster, currently in a one-way,
source/destination scenario.

### Helm chart values

<table>
 <thead>
  <tr><th>value</th><th>default</th><th>purpose</th></tr>
 </thead>
 <tbody>
  <tr>
   <td><code>replicaCount</code></td>
   <td>1</td>
   <td>
    the number of mirrormaker2 instances to deploy;
   </td>
  </tr>
  <tr>
   <td><code>groupId</code></td>
   <td>cmdaa</td>
   <td>
    the unique id used to synchronize topic offsets within the two kafka
    instances to ensure that a restart of mirrormaker2 won't repeat messages
    that have already been mirrored;
   </td>
  </tr>
  <tr>
   <td><code>image.repository</code></td>
   <td>bitnami/kafka</td>
   <td>the container image repository;</td>
  </tr>
  <tr>
   <td><code>image.tag</code></td>
   <td>2.8.0-debian-10-r3</td>
   <td>the container image tag;</td>
  </tr>
  <tr>
   <td><code>image.pullPolicy</code></td>
   <td>IfNotPresent</td>
   <td>the container image pull policy;</td>
  </tr>
  <tr>
   <td><code>nameOverride</code></td>
   <td>""</td>
   <td>
    if provided, a partial over-ride of the deployment name. This will be
    combined with the helm release to form the full deployment name.<br/>
    For example, if the helm release is <code>my-release</code> and this value
    is <code>mm2</code>, the deployment name will be
    <code>my-release-mm2</code>.<br/>
    <b>NOTE:</b> if this value <i>contains</i> the helm release, it will be
    used as the deployment name without repeating the helm release.<br/>
    For example, if the helm release is <code>my-release</code> and this value
    is <code>my-release-mm2</code>, the deployment name will <i>also</i> be
    <code>my-release-mm2</code>.
   </td>
  </tr>
  <tr>
   <td><code>fullnameOverride</code></td>
   <td>mm2</td>
   <td>
    if provided, a full over-ride of the deployment name which also over-rides
    any <code>nameOverride</code> value.<br />
    <b>NOTE:</b> if neither <code>nameOverride</code> nor
    <code>fullnameOverride</code> are provided, the deployment will follow the
    ad hoc standard for naming: <code>%helm-release%-%chart-name%</code>.
   </td>
  </tr>
  <tr>
   <td><code>mirror.source.host</code></td>
   <td>kafka.kafka-instance-2.svc.cluster.local</td>
   <td>
    the FQDN or IP address of the source kafka (service) from which topics will
    be mirrored.<br/>
    <b>NOTE:</b> This must be directly accessible from the mirrormaker2
    container(s);
   </td>
  </tr>
  <tr>
   <td><code>mirror.source.port</code></td>
   <td>9092</td>
   <td>
    the IP port number of the source kafka (service);
   </td>
  </tr>
  <tr>
   <td><code>mirror.destination.host</code></td>
   <td>kafka.kafka-instance-1.svc.cluster.local</td>
   <td>
    the FQDN or IP address of the destination kafka (service) to which topics
    will be mirrored.<br/>
    <b>NOTE:</b> This must be directly accessible from the mirrormaker2
    container(s);
   </td>
  </tr>
  <tr>
   <td><code>mirror.destination.port</code></td>
   <td>9092</td>
   <td>
    the IP port number of the destination kafka (service);
   </td>
  </tr>
  <tr>
   <td><code>mirror.topics</code></td>
   <td>.*</td>
   <td>
    the regular expression identifying the kafka topic(s) to mirror from the
    source to the destination kafka;
   </td>
  </tr>
  <tr>
   <td><code>internalTopicReplication</code></td>
   <td>&nbsp;</td>
   <td>
    these settings establish the replication factor for mirrormaker2 internal
    topics (checkpoints, heartbeats, offset synchronization, status, and
    configuration).<br/>
    For anything other than development testing, a value greater than 1 is
    recommended to ensure availability.
   </td>
  </tr>
  <tr>
   <td>&nbsp;&nbsp;<code>.checkpoints</code></td>
   <td>1</td>
   <td>
    Replication factor used for internal checkpoints topics (establishes config
    value for <code>checkpoints.topic.replication.factor</code>).
    This is related to the replica count for the source/destination kafka
    clusters;
   </td>
  </tr>
  <tr>
   <td>&nbsp;&nbsp;<code>.heartbeats</code></td>
   <td>1</td>
   <td>
    Replication factor used for internal heartbeats topics (establishes config
    value for <code>heartbeats.topic.replication.factor</code>).
    This is related to the replica count for the source/destination kafka
    clusters;
   </td>
  </tr>
  <tr>
   <td>&nbsp;&nbsp;<code>.offsetSyncs</code></td>
   <td>1</td>
   <td>
    Replication factor used for internal offset-syncs topics (establishes config
    value for <code>offset-syncs.topic.replication.factor</code>).
    This is related to the replica count for the source/destination kafka
    clusters;
   </td>
  </tr>
  <tr>
   <td>&nbsp;&nbsp;<code>.offset</code></td>
   <td>1</td>
   <td>
    Replication factor used for mirrormaker2 offset storage (establishes config
    value for <code>offset.storage.replication.factor</code>).
    This is related to the replica count for <b>this</b> mirrormaker2
    deployment;
   </td>
  </tr>
  <tr>
   <td>&nbsp;&nbsp;<code>.status</code></td>
   <td>1</td>
   <td>
    Replication factor used for mirrormaker2 status storage (establishes config
    value for <code>status.storage.replication.factor</code>).
    This is related to the replica count for <b>this</b> mirrormaker2
    deployment;
   </td>
  </tr>
  <tr>
   <td>&nbsp;&nbsp;<code>.config</code></td>
   <td>1</td>
   <td>
    Replication factor used for mirrormaker2 config storage (establishes config
    value for <code>config.storage.replication.factor</code>).
    This is related to the replica count for <b>this</b> mirrormaker2
    deployment;
   </td>
  </tr>
  <tr>
   <td><code>additionalSettings</code></td>
   <td>{}</td>
   <td>
    If provided, a set of key/value pairs that will be directly included in the
    mirrormaker2 configuration.<br/>
    Example:
    <pre>
    additionalSettings:
      "sync.topic.acls.enabled"         : false
      "emit.heartbeats.interval.seconds": 5
    <pre>
   </td>
  </tr>
 </tbody>
</table>
