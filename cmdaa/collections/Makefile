#
#
# This makefile makes use of the following utilities:
#   - awk
#   - helm
#
NS      := cmdaa	#$(shell ./etc/k8s-namespace)
CTX     := local	#$(shell ./etc/kubectl-context)
REL     := colle	#$(shell ./etc/helm-release)
KUBECTL := kubectl --context=$(CTX) --namespace=$(NS)
HELM    := helm --kube-context=$(CTX) --namespace=$(NS)

VALUES	:= --values=values.yaml

#? Show this help message
help:
	@echo
	@echo "Available rules:"
	@awk 'BEGIN{ split("", help) } \
	      /^#\?/{ \
	        help[ length(help) ] = substr($$0,4); \
	      } \
	      /^[a-z]/{ \
	        if ( length(help) < 1) { next } \
	        printf("  %-25s ",substr($$1,1,length($$1)-1)); \
	        nHelp = length(help); \
	        for ( idex = 0; idex < nHelp; idex++) { \
	          if (idex > 0) { printf("%27s ", " ") } \
	          printf("%s\n", help[idex]); \
	        } \
	        split("", help); \
	      }' Makefile
	@echo

#? Import charts from the local repository
charts: Chart.yaml
	@echo ">>> Import charts from the local repository"
	@./etc/update-local-charts

##############################################################################
# Helm {
#
#? Update dependencies via helm, pulling from remote
#? repositories
helm-dependency-update: Chart.yaml
	@helm dependency update

#? Perform templating via helm
helm-template: charts values.yaml
	@$(HELM) template $(REL) $(VALUES) .

#? Perform a dry-run install test via helm
helm-dry-run: charts values.yaml
	@$(HELM) install $(REL) $(VALUES) --dry-run .

#? Install via helm
helm-install: charts values.yaml
	@$(HELM) install $(REL) --create-namespace $(VALUES) .

#? View all active helm charts in the target namespace
helm-status:
	@$(HELM) list

#? Upgrade an installation via helm
helm-upgrade: charts values.yaml
	@$(HELM) upgrade $(REL) $(VALUES) .

#? Uninstall via helm
#?
helm-uninstall:
	@$(HELM) uninstall $(REL)

# Helm }
##############################################################################
# Kustomize {
#
#? Generate the kustomized base
base: charts
	@echo ">>> Generate kustomize base"
	@./etc/base-update

# Kustomize }
##############################################################################

#? Show the kubectl context
show-context:
	@echo "kubectl context: $(CTX)"

#? Show the k8s namespace
show-ns:
	@echo "k8s namespace  : $(NS)"

#? Show the helm release
show-rel:
	@echo "helm release   : $(REL)"

#? Show the deployment environment
#? (context, ns, rel)
#?
show-env: show-context show-ns show-rel

#? Watch kubectl events
watch-events:
	@echo ">>> Watching k8s events (ctl-c to quit)"
	@$(KUBECTL) get events --watch-only

#? Watch pod status
#?
watch-pods:
	@echo ">>> Watching k8s pods (ctl-c to quit)"
	@./etc/watch $(KUBECTL) get pods

#? Remove dependencies for distribution
dist-clean:
	@echo ">>> Remove artifacts & dependencies ..."
	@rm -f  Chart.yaml.bak Chart.lock
	@rm -rf charts base

##############################################################################
# No help / private
#
