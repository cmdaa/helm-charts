Here you will find a helm chart that may be used to install the
CMDAA components required for custom metric collections and data
flow.

This may be deployed within a kubernetes cluster with a functional
infrastructure comprised of:
- [s3-compatible storage service](#s3-storage-service);
- [Kafka event streaming platform](#kafka-event-streaming-platform);
- [Timescaledb and Promscale](#timescaledb-and-promscale), typically deployed
  via [The OBservatility Stack (tobs)](https://github.com/timescale/tobs);

**Table Of Contents**

[[_TOC_]]

---
# Required Infrastructure
## S3 storage service
The s3-compatible storage service (e.g. [minio](https://min.io/)) provides
storage for a repository/cache of compiled kernel-beat modules. These modules
are compiled specifically for the target node when kernel-beat is installed on
that node, and the generated module is uploaded to the repository as a cache
for the next startup.

This service must expose the following information which will be used to set
[kernel_beat values](#kernelbeat):
- connection information: service host / port
  (`.Values.kernel_beat.init.repo.host`, `.Values.kernel_beat.init.repo.port`);
- authentication information:
  - the name of a secret containing two keys (access, secret) that may be used
    to authentiate to the given host/port with permission to create buckets,
    upload new objects, download existing objects
    (`.Values.kernel_beat.init.repo.secret.name`);
  - the name of the access key (`.Values.kernel_beat.init.repo.secret.keys.access`);
  - the name of the secret key (`.Values.kernel_beat.init.repo.secret.keys.secret`);


## Kafka event streaming
The [kafka](https://kafka.apache.org/) event streaming platform provides a
reliable connection between the disparate CMDAA services from endpoint
collection through streaming analytics to database/index storage.

This service must expose the following information which will be used to set
[kafka values](#kafka):
- connection information: service host / port;
  (`.Values.kafka.host`, `.Values.kafka.port`);


## Timescaledb and Promscale
[PromScale](https://github.com/timescale/promscale) provides
Prometheus-complient read/write access to
[Timescaledb](https://github.com/timescale/timescaledb), which serves as the
persistent store for all metrics collected by CMDAA.

Within the collection flow, Promscale is used to write metrics to Timescaledb
but may also be used by additional components (e.g.  grafana) to access those
metrics.

The Promscale service must expose the following information which wiill be used
to set [Promscale values](#promscale):
- connection information: service host / port;
  (`.Values.promscale.host`, `.Values.promscale.port`);


**NOTE:** the tobs v0.1.2 helm chart hardcodes the Promscale write endpoint to
'write' and so it is currently not exposed. Any change to the write endpoint
would need to be manually reflected in the
`cmdaa.collections.prometheus.remote_write.url` [helper
template](templates/_helpers.tpl)


---
# The CMDAA metric flow
```mermaid
graph TD
  A("sg-logs / kernel-beat") --> R1("metrics-bridge<br> custom metrics")
  B("logs / journald") --> R2("fluentd/grok<br> enables grok matching and<br> local/edge streaming analytics")
  C("prometheus exporter") --> R3("prometheus<br> enables access to<br> existing metric sources") --> R4("prometheus<br>-kafka<br>-adapter")

  R1 & R2 ---> K{"kafka<br> enables cluster-wide<br> streaming analytics"}
  R4 --> K

  K --> M(metrics-bridge)
  M --> P("PromScale<br> enables prometheus-like<br> read/write access to<br> metric data")
  P --> T[("Timescaledb<br> persistent metric storage enabling<br> retrospective/static analytics")]
```

## Custom metrics

This chart provides metrics collection from a number of metric sources
including those custom to CMDAA.

- [kernel_beat](https://gitlab.com/cmdaa/linux-kernel-module/-/blob/master/docs/KernelBeat.md)
  &mdash; provides linux kernel-based collection of system metrics;
- [sg-logs](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/sg-logs/README.md)
  &mdash; provides collection of SCSI disk metrics;


In addition, if a typical Prometheus server is installed in the cluster,
metrics collected by the server may be included in the metrics flow using the
[prometheus-kafka-adapter](https://github.com/Telefonica/prometheus-kafka-adapter).
The Prometheus server would need to be configured to use this adapter as a
remote write endpoint, sending all Prometheus-collected metrics into the kafka
event streaming platform along with all other CMDAA metrics.


## Metrics bridge

In support of metrics collection, the
[metrics bridge](//gitlab.com/cmdaa/custom-collectors/-/tree/master/prometheus-bridge)
enables the flow of metrics through the CMDAA system between core components.

This bridge can accept and process asynchronous metric data (sources) and route
it to one or more remote writers (sinks). The configuration and activation of
sources and sinks is run-time configurable.

Asynchronous sources accept metric data from a metrics collector, convert it
into a normalized metric form, and publish the normalized metrics to an
internal event bus.

The remote writers listen on the internal event bus for normalized metrics,
convert them into a form suitable for the configured destination, and push them
out to that destination.


The available asynchronous sources and remote writers are:
- [asynchronous sources](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#asynchronous-source):
  - [kafka](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kafka)
    : receive JSON formatted metrics via one or more kafka topics;
  - [kernelbeat](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kernelbeat)
    : read and process data from the
    [KernelBeat](https://gitlab.com/cmdaa/linux-kernel-module/-/blob/master/docs/KernelBeat.md)
    kernel module;
  - [prometheus](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#prometheus)
    : receive prometheus formatted data via TCP socket (Note that this does
    **not** implement the prometheus remote write API but expects metrics as
    strings in the
    [Prometheus exposition format](https://prometheus.io/docs/instrumenting/exposition_formats/));
  - [prometheus scrape](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#scraper)
    : simple, time-based scraping of Prometheus metric endpoints;
- [remote writers](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#remote-writer):
  - [kafka](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kafka-1)
    : write JSON formatted metrics to one or more kafka topics;
  - [prometheus](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#prometheus-1)
    : use the Prometheus remote write protocol to send metrics to a
    Prometheus-like endpoint (e.g. PromScale);
  - [timescale](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#timescale)
    : use the retired `pg_prometheus` timescale extension to write data
    directly to Timescaledb;


# Helm configuration

The contained [values.yaml](values.yaml) is used to configure the helm
installation (e.g. `helm install your-release --values=values.yaml .`).

This section documents the available sections and values.


## Bridge

The `bridge` values affect all templates that make use of the
[metrics-bridge](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/).

 Key                      | Default               | Description
 ------------------------ | --------------------- | ----------------------------------------------------
 bridge.image.name        | cmdaa/metrics-bridge  | Container image name
 bridge.image.tag         | 0.0.22                | Container image tag
 bridge.image.pullPolicy  | IfNotPresent          | Container image pull policy


## Promscale

The `promscale` values affect the
[kafka](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kafka)-[promscale](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#prometheus-1).
bridge/deployment which receives metrics data from kafka and writes it to
PromScale.

**NOTE:** The Promscale write endpoint is currently not configurable via
`values.yaml` beyond simple `host` and `port`. Any change to the write endpoint
beyond that would need to be manually reflected in the
`cmdaa.collections.prometheus.remote_write.url` [helper
template](templates/_helpers.tpl). This is due to the fact that the tobs v0.1.2
helm chart hardcodes the Promscale write endpoint to 'write'.


 Key                         | Default                   | Description
 --------------------------- | ------------------------- | ----------------------------------------------------
 promscale.enabled           | True                      | Is the kafka-promscale bridge enabled?
 promscale.host              | cmdaa-promscale-connector | Promscale service/host name
 promscale.port              | 9201                      | Promscale service TCP port number
 promscale.replicas          | 1                         | kafka-promscale bridge replica count
 promscale.verbosity         | 0                         | Remote write verbosity
 promscale.idle_timeout      | 5s                        | Remote write idle timeout
 promscale.request_timeout   | 15s                       | Remote write request timeout
 promscale.max_sockets       | 0                         | Maximum sockets (0 == no max)
 promscale.max_free_sockets  | 0                         | Maximum free sockets (0 == no max)
 promscale.batch_num_samples | 10000                     | Maximum metric samples to batch together
 promscale.batch_timeout     | 5s                        | Timeout for batching samples (after which they are sent regardless of count)
 promscale.max_in_flight     | 128                       | Maximum number of in-flight/incomplete writes
 promscale.max_resends       | 10                        | Maximum resend attempts for failed sends


## Prometheus

The `prometheus` values are used to configure the
[prometheus](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#prometheus)-[kafka](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kafka-1)
bridge/deployment, configuration, and service.

This bridge instance receives metrics in [Prometheus exposition
format](https://prometheus.io/docs/instrumenting/exposition_formats/) over a
TCP sockets and writes them to kafka.


 Key                      | Default                 | Description
 ------------------------ | ----------------------- | ----------------------------------------------------
 prometheus.enabled       | True                    | Is the prometheus-kafka bridge enabled?
 prometheus.replicas      | 1                       | prometheus-kafka bridge replica count
 prometheus.service.host  | cmdaa-prometheus-kafka  | prometheus-kafka bridge service/host name
 prometheus.service.port  | 3337                    | prometheus-kafka bridge service TCP port number
 prometheus.service.loadBalancer.enabled | False    | Is the load balancer enabled?
 prometheus.service.loadBalancer.annotations | {}   | Load balancer annotations


## KernelBeat

These values are used by the KernelBeat repository, daemonset and
configuration.

The KernelBeat repository is a simple nginx-based data store containing
pre-compiled versions of the KernelBeat kernel module. It is used by the
daemonset's initialization container to retrieve the appropriate kernel module
for installation.

The KernelBeat daemonset is comprised of an initialization container, which
fetches|compiles and installs the
[kernel_beat](https://gitlab.com/cmdaa/linux-kernel-module/-/blob/master/docs/KernelBeat.md)
kernel module, along with a
[kernelbeat](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kernelbeat)-
[kafka](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/prometheus-bridge/README.md#kafka-1)
bridge container.

The bridge container reads metrics from the `/dev/kernel_beat` character device
and writes them to kafka.

**NOTE:** This daemonset will only be installed on k8s nodes with the label
`cmdaa.collect/kernelbeat=true`.


 Key                            | Default                 | Description
 ------------------------------ | ----------------------- | ----------------------------------------------------
 kernel_beat.enabled            | True                    | Is the KernelBeat component enabled?
 kernel_beat.version            | 0.15                    | KernelBeat version to use
 kernel_beat.beat_interval      | 30s                     | Collection interval (beat, differential reporting on each beat)
 kernel_beat.beat_sync          | 10                      | The beat at which all metrics are collected regardless of difference (5 minutes with a 30s period)
 kernel_beat.buf_size           | 4096                    | Metrics-bridge async source buffer size
 kernel_beat.poll_interval      | 1s                      | Metrics-bridge async source poll interval
 kernel_beat.init.image.name    | cmdaa/kernel_beat-build | Container image name
 kernel_beat.init.image.tag     | 0.15-1                  | Container image tag
 kernel_beat.init.image.pullPolicy  | IfNotPresent        | Container image pull policy
 kernel_beat.init.repo.enabled  | False                   | The name of the image to use to build kernel_beat for the target system
 kernel_beat.init.repo.host     | cmdaa-minio             | S3 Repository service/host name
 kernel_beat.init.repo.port     | 9000                    | S3 Repository service TCP port number
 kernel_beat.init.repo.bucket   | kernel-beat             | S3 Repository bucket name
 kernel_beat.init.repo.secret.name  | cmdaa-secret-s3     | The name of the secret containing S3 access credentials
 kernel_beat.init.repo.secret.keys.access | accesskey     | The name of the key containing the access-key credential
 kernel_beat.init.repo.secret.keys.secret | secretkey     | The name of the key containing the secret-key credential
 kernel_beat.init.repo.init_tries   | 12                  | Maximum number of connection attempts
 kernel_beat.init.repo.init_timeout | 5                   | Inter-try timeout (seconds)


## sg-logs

The `sglogs` values are used to configure the sg-logs daemonset.

This daemonset runs the
[sg-logs](https://gitlab.com/cmdaa/custom-collectors/-/blob/master/sg-logs/README.md)
SCSI disk metric collector configured to generate metrics in the
[Prometheus exposition format](https://prometheus.io/docs/instrumenting/exposition_formats/)
and write them to the [prometheus-kafka bridge](#prometheus).


**NOTE:** This daemonset will only be installed on k8s nodes with the label
`cmdaa.collect/sglogs=true`.


 Key                      | Default       | Description
 ------------------------ | ------------- | ----------------------------------------------------
 sglogs.enabled           | True          | Is the sg-logs component enabled?
 sglogs.image.name        | cmdaa/sg-logs | Container image name
 sglogs.image.tag         | 0.2.5         | Container image tag
 sglogs.image.pullPolicy  | IfNotPresent  | Container image pull policy
 sglogs.publish.timestamp | True          | Should the timestamp be included in published metrics?
 sglogs.log_page.parse_unknowns | True    | Should unknown pages/parameters be parsed?
 sglogs.log_page.report_unknowns | True   | Should unknown pages/parameters be reported?
 sglogs.log_page.report_level | 1         | The report level (0..100)
 sglogs.scheduler.period  | 5m            | The scheduling period/interval (beat, differential reporting on each beat)
 sglogs.scheduler.sync_beat | 6           | The beat at which all metrics are collected regardless of difference (30 minutes with a 5m period)
 sglogs.scheduler.thread_pool | 10        | The number of scheduling threads to use
 sglogs.scheduler.reconnect_delay | 5s    | Scheduling reconnect delay (ATScheduler)


## kafka

The `kafka` values are used by any template that requires kafka connectivity.

 Key                      | Default     | Description
 ------------------------ | ----------- | ----------------------------------------------------
 kafka.host               | cmdaa-kafka | Kafka service/host name
 kafka.port               | 9092        | Kafka service TCP port number
 kafka.verbosity          | 0           | Async Source/Remote write verbosity
 kafka.connect_timeout    | 10s         | TCP connect timeout
 kafka.idle_timeout       | 5m          | Idle request timeout
 kafka.request_timeout    | 30s         | Request timeout
 kafka.request_max_bytes  | `_1048576`  | Maximum kafka request size (the kafka chart requires that this inclue a `_` prefix to avoid YAML conversion to exponential notation or float)
 kafka.topics             | [metrics_application, metrics_cgroup, metrics_device, metrics_filesystem, metrics_network, metrics_process, metrics_system] | The set of kafka topics that should be used: `metrics_cgroup,filesystem,network,process,system` are used by kernel_beat, `metrics_deivce` is used by sg-logs, and `metrics_application` is the fallback
 kafka.topic_prefix       | metrics     | Remote write metric prefix to use when selecting the target topic
 kafka.batch_num_samples  | 10000       | Maximum metric samples to batch together
 kafka.batch_timeout      | 500ms       | Timeout for batching samples (after which they are sent regardless of count)


# Daemonset controls

As mentioned above, the daemonsets provided by this helm chart will only be
deployed on properly labeled k8s nodes.

- The KernelBeat daemonset requires a node label of
  `cmdaa.collect/kernelbeat=true`;
- The sg-logs daemonset requires a node label of
  `cmdaa.collect/sglogs=true`;
- The procfs collector daemonset requires a node label of
  `cmdaa.collect/procfs=true`;
- The eBPF-based process monitor daemonset requires a node label of
  `cmdaa.collect/procmon=true`;
