#
# Start a bridge to read data from the local kernel_beat character device
# and forward it to kafka
#
function log {
  echo "$(date '+%Y-%m-%d %H:%M:%S') - kernelbeat-kafka - $1"
}

function terminate() {
  log "Stopping -- kill SIGTERM $BRIDGE_PID"

  kill $BRIDGE_PID
  if [ ! -z "$(lsmod | grep kernel_beat)" ]; then
    # Given the bridge a bit of time to stop before attempting to unload the
    # module
    sleep 5 &
    wait $!

    log "Unload kernel_beat module"
    rmmod -f kernel_beat

    # Give rmmod a second to complete
    sleep 1 &
    wait $!
  fi

  log "Stopped"
  exit 0
}

# If we don't catch these signals, this script will not respond at all to a
# regular shutdown request, therefore, we explicitly terminate if we
# receive these signals.
trap terminate SIGTERM SIGQUIT

# Wait for kafka to become available
log "Wait for kafka"
while [ 1 ]; do
  nc -zw 1 {{ .Values.bridge.kafka.host }} {{ .Values.bridge.kafka.port }}
  [ $? -eq 0 ] && break

  sleep 2 &
  wait $!
done

# Start the bridge
/app/bin/bridge -c /app/etc/bridge.yaml &
BRIDGE_PID=$!

log "Bridge started [ $BRIDGE_PID ]"
wait $BRIDGE_PID
