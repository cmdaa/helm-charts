#
# Start a bridge that is simply configured via /app/etc/bridge.yaml with no
# pre-requisites,
#
function log {
  echo "$(date '+%Y-%m-%d %H:%M:%S') - kafka-promscale - $1"
}

function terminate() {
  log "Stopping -- kill SIGTERM $BRIDGE_PID"

  kill $BRIDGE_PID
  log "Stopped"
  exit 0
}

# If we don't catch these signals, we will not respond at all to a regular
# shutdown request, therefore, we explicitly terminate if we receive these
# signals.
trap terminate SIGTERM SIGQUIT

/app/bin/bridge -c /app/etc/bridge.yaml &
BRIDGE_PID=$!

log "Bridge started [ $BRIDGE_PID ]"
wait $BRIDGE_PID
