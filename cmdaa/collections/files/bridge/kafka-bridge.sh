#
# Start a bridge to read data from the kafka and foward it on to the
# configured remote writer.
#
function log {
  echo "$(date '+%Y-%m-%d %H:%M:%S') - prometheus-kafka - $1"
}

function terminate() {
  log "Stopping -- kill SIGTERM $BRIDGE_PID"

  kill $BRIDGE_PID
  log "Stopped"
  exit 0
}

# If we don't catch these signals, we will not respond at all to a regular
# shutdown request, therefore, we explicitly terminate if we receive these
# signals.
trap terminate SIGTERM SIGQUIT

# Wait for kafka to become available
log "Wait for kafka"
while [ 1 ]; do
  nc -zw 1 {{ .Values.bridge.kafka.host }} {{ .Values.bridge.kafka.port }}
  [ $? -eq 0 ] && break

  sleep 2 &
  wait $!
done

# Start the bridge
/app/bin/bridge -c /app/etc/bridge.yaml &
BRIDGE_PID=$!

log "Bridge started [ $BRIDGE_PID ]"
wait $BRIDGE_PID
