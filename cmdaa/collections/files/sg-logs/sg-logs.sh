#
# Start a bridge to read data from sglogs as an asynchronous source, and
# forward it to kafka.
#
# This script expects sg-logs to be installed with a configuration file at:
#   /etc/sg-logs/config.ini
#
#   Within thie config file, we expect to find a 'publish' that identifies
#   the expected TCP endpoint that our bridge will be started to serve:
#     publish tcp://%host%:%port%
#
# This script also expects the /etc and /proc filesystems of the underlying
# system to be available via /root/etc and /root/proc. This is to allow
# resolution of the /etc/mtab file for the underlying system.
#
DIR="$(dirname "$(realpath "$0")")"

SGLOGS_CONFIG="/etc/sg-logs/config.ini"

function log {
  echo "$(date '+%Y-%m-%d %H:%M:%S') - sg-logs - $@"
}

function terminate() {
  log "Stopping -- kill SIGTERM $SGLOGS_PID"

  kill $SGLOGS_PID

  log "Stopped"
  exit 0
}

# If we don't catch these signals, we will not respond at all to a regular
# shutdown request, therefore, we explicitly terminate if we receive these
# signals.
trap terminate SIGTERM SIGQUIT

###########################################################################
# Establish /etc/mtab
#
MTAB_LOCAL='/etc/mtab'
MTABS_ROOT=( '/root/etc/mtab' '/root/proc/self/mounts' )
for MTAB_ROOT in ${MTABS_ROOT[@]}; do
  if [ ! -f $MTAB_ROOT ]; then
    continue
  fi

  MD5_LOCL="$(md5sum $MTAB_LOCAL | awk '{print $1}')"
  MD5_ROOT="$(md5sum $MTAB_ROOT | awk '{print $1}')"
  if [ $MD5_LOCL = $MD5_ROOT ]; then
    log "$MTAB_LOCAL == $MTAB_ROOT"

  else
    log "Soft-link $MTAB_LOCAL to $MTAB_ROOT"

    ln -sf $MTAB_ROOT $MTAB_LOCAL
  fi

  break
done

if [ ! -e $MTAB_LOCAL ]; then
  log "Cannot locate $MTAB_LOCAL"
  terminate
else
  log "$MTAB_LOCAL : $(ls -l $MTAB_LOCAL)"
fi

###########################################################################
# Assemble sg-logs arguments
#
# :XXX: Previously, extensions were included here via:
#       {{- range $ext := .Values.sglogs.extensions }}
#       -e "{{ $ext }}"
#       {{- end }}
#
#       This functionality has been moved into the config.ini file now that
#       sg-logs supports this.
#
SGLOGS_ARGS=(
  -c "${SGLOGS_CONFIG}"
)

###########################################################################
# Retrieve any publish URL and split it into HOST/PORT
#
# We're expecting the URL to have the form:
#     tcp://(host):(port)
#
PUB_URL=$(awk -F= '/^publish /{
  # Trim leading/trailing white-space
  sub(/(^[ \t\r\n]+|[ \t\r\n]+$])/, "", $2);
  print $2
}' ${SGLOGS_CONFIG})

PUB_HOST=$(echo "$PUB_URL"| sed -E 's#^tcp://([^:]+):(.+)$#\1#')
PUB_PORT=$(echo "$PUB_URL"| sed -E 's#^tcp://([^:]+):(.+)$#\2#')

if [[ ! -z "$PUB_HOST" && "$PUB_HOST" != "$PUB_PORT" ]]; then
  # Wait for the publish endpoint (bridge) to become available
  while [ 1 ]; do
    log "Waiting for bridge ($PUB_HOST:$PUB_PORT)"
    $DIR/check-conn.py 1 $PUB_HOST $PUB_PORT
    [ $? -eq 0 ] && break

    sleep 2 &
    wait $!
  done
  log "Bridge ($PUB_HOST:$PUB_PORT) available"
fi

# Start sg-logs
log "Start with: ${SGLOGS_ARGS[@]}"
sg-logs ${SGLOGS_ARGS[@]} &
SGLOGS_PID=$!

log "sg-logs started [ $SGLOGS_PID ]"
wait $SGLOGS_PID
