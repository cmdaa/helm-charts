#!/bin/bash
#
# Start eBPF-based process monitor collections on the given set of nodes
#
#   labels-add-procmon %node1% [ %node2% ... ]
#
 DIR=$(realpath "$(dirname "$0")")
ROOT=$(dirname "$DIR")
 REL="$( $DIR/helm-release )"

if [ $# -lt 1 ]; then
  cat <<EOF
***
*** Usage: $(basename "$0") %node1% [ %node2% ... ]
***
EOF
  exit -1
fi

nodes=$@
if [ ${#nodes[@]} -eq 1 -a ${nodes[0]} == 'all' ]; then
  nodes=( $($DIR/kubectl get nodes --no-headers | awk '{print $1}') )
fi

# Retrieve the label that controls the target daemonset
label="$($DIR/kubectl get ds/$REL-procmon-kafka --no-headers | \
          awk '{print $7}')"
if [ -z "$label" ]; then
  echo "*** No $REL-procmon-kafka daemonset found"
  exit
fi

for node in ${nodes[@]}; do
  echo ">>> Add '$label' to $node ..."
  $DIR/kubectl label nodes "$node" "$label"
done
