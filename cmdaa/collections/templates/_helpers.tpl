##############################################################################
# Common labels
#
#   Usage:
#     labels: \{\{- include "cmdaa.collections.labels" . | nindent 4 \}\}
#
{{- define "cmdaa.collections.labels" -}}
app:                          {{ .Release.Name }}-collections
release:                      {{ .Release.Name }}
heritage:                     {{ .Release.Service }}
chart:                        {{ .Chart.Name }}

app.kubernetes.io/instance:   {{ .Release.Name }}-collections
app.kubernetes.io/managed-by: {{ .Release.Service }}

helm.sh/chart:                {{ .Chart.Name }}
{{ end -}}

##############################################################################
# Return the URL for kafka
#
#   Usage:
#     url: \{\{- include "cmdaa.collections.kafka.url" . \}\}
#
{{- define "cmdaa.collections.kafka.url" -}}
{{-  $host := .Values.bridge.kafka.host | trunc 63 | trimSuffix "-" -}}
{{-  $port := .Values.bridge.kafka.port -}}
kafka://{{ $host }}:{{ $port }}/
{{- end -}}

##############################################################################
# Return the host:port to be used for the prometheus TCP endpoint
#
#   Usage:
#     url: \{\{ include "cmdaa.collections.prometheus.endpoint" . \}\}
#
{{- define "cmdaa.collections.prometheus.endpoint" -}}
{{-  $host := .Values.bridge.prometheus.service.host | trunc 63 | trimSuffix "-" -}}
{{-  $port := .Values.bridge.prometheus.service.port -}}
{{ $host }}:{{ $port }}/
{{- end -}}

##############################################################################
# Return the URL to be used as a TCP async_source for a bridge
#
#   Usage:
#     url: \{\{ include "cmdaa.collections.prometheus.url" . \}\}
#
{{- define "cmdaa.collections.prometheus.url" -}}
{{-  $host := .Values.bridge.prometheus.service.host | trunc 63 | trimSuffix "-" -}}
{{-  $port := .Values.bridge.prometheus.service.port -}}
prometheus://{{ $host }}:{{ $port }}/
{{- end -}}

##############################################################################
# Return the URL to be used as a TCP endpoint for a bridge
#
#   Usage:
#     url: \{\{ include "cmdaa.collections.prometheus.tcp.url" . \}\}
#
{{- define "cmdaa.collections.prometheus.tcp.url" -}}
{{-  $host := .Values.bridge.prometheus.service.host | trunc 63 | trimSuffix "-" -}}
{{-  $port := .Values.bridge.prometheus.service.port -}}
tcp://{{ $host }}:{{ $port }}/
{{- end -}}

