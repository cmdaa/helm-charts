The CMDAA helm charts are divided into:
- [infrastrucure](infrastructure) : manages the helm-based installation of the
  CMDAA infrastructure required by all other components (e.g. kafka,
  timescaledb, promscale, minio/s3, mongodb, argo);
- [collections](collections) : manages the helm-based installation of the
  metric collections portion of CMDAA. This portion of CMDAA provides access to
  various metric sources;
- [analytics](analytics) : manages the helm-based installation of the analytics
  portion of CMDAA. This portion of CMDAA provides an analytic framework for
  use on the various metrics collected by CMDAA;
- [umbrella](umbrella) : provides a helm-based installation of all CMDAA
  components in a coordinated fashion, ensuring that the sub-charts are
  properly inter-connected;
- [mirrormaker2](mirrormaker2) : may be used to mirror topics from one kafka
  cluster to another. This enables metric collection in one k8s/kafka cluster
  to be forwarded to a second k8s/kafka cluster for analysis and/or storage;

The CMDAA data-flow (see
[Data-Flow](https://gitlab.com/cmdaa/documentation/-/wikis/Data-Flow) for more
details):
![CMDAA-data-flow](CMDAA-data-flow.png)
